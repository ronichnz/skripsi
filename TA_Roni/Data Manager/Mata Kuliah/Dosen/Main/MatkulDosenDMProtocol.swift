//
//  MatkulDosenDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol MatkulDosenRequestProtocol: class {
    var dataManager: MatkulDosenResponseProtocol? { get set }
    var api: API<MatKulModel> { get set }
    
    func getMatkulDosen(_ dosenId: String)
}

protocol MatkulDosenResponseProtocol: class {
    func successGetMatkulDosen(_ response: ApiResponse<MatKulModel>)
    func failedGetMatkulDosen(_ error: ApiError)
}

protocol MatkulDosenDMRequestProtocol: class {
    var interactor: MatkulDosenResponseProtocol? { get set }
    var remote: MatkulDosenRequestProtocol { get set }
    
    init(_ interactor: MatkulDosenResponseProtocol)
    
    func getMatkulDosen(_ dosenId: String)
}
