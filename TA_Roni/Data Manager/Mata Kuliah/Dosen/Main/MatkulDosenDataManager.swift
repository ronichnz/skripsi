//
//  MatkulDosenDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class MatkulDosenDataManager: MatkulDosenDMRequestProtocol {
    weak var interactor: MatkulDosenResponseProtocol? = nil
    var remote: MatkulDosenRequestProtocol
    
    required init(_ interactor: MatkulDosenResponseProtocol) {
        self.interactor = interactor
        remote = MatkulDosenRemote()
        remote.dataManager = self
    }
    
    func getMatkulDosen(_ dosenId: String) {
        remote.getMatkulDosen(dosenId)
    }
}

extension MatkulDosenDataManager: MatkulDosenResponseProtocol {
    func successGetMatkulDosen(_ response: ApiResponse<MatKulModel>) {
        interactor?.successGetMatkulDosen(response)
    }
    
    func failedGetMatkulDosen(_ error: ApiError) {
        interactor?.failedGetMatkulDosen(error)
    }
}
