//
//  MatkulDosenRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class MatkulDosenRemote: MatkulDosenRequestProtocol {
    weak var dataManager: MatkulDosenResponseProtocol?
    var api: API<MatKulModel> = API<MatKulModel>()
    
    func getMatkulDosen(_ dosenId: String) {
        let data = ApiRequest(path: "matkul_dosen.php?id_dosen=\(dosenId)", method: .get, headers: [
            "Authorization": Account.getClientCredentials() ?? ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successGetMatkulDosen, onError: dataManager.failedGetMatkulDosen)
    }
}
