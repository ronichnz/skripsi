//
//  MatKulDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 05/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import Alamofire

class MatKulDataManager: MatKulDMRequestProtocol {
    weak var interactor: MatKulResponseProtocol? = nil
    var remote: MatKulRequestProtocol
    
    required init(_ interactor: MatKulResponseProtocol) {
        self.interactor = interactor
        remote = MatKulRemote()
        remote.dataManager = self
    }
    
    func getMatKul(_ semester: String) {
        remote.getMatKul(semester) 
    }
}

extension MatKulDataManager: MatKulResponseProtocol {
    func successGetMatKul(_ response: ApiResponse<MatKulModel>) {
        interactor?.successGetMatKul(response)
    }
    
    func failedGetMatKul(_ error: ApiError) {
        interactor?.failedGetMatKul(error)
    }
}
