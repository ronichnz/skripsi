//
//  MatKulDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 05/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import Alamofire

protocol MatKulRequestProtocol: class {
    var dataManager: MatKulResponseProtocol? { get set }
    var api: API<MatKulModel> { get set }
    
    func getMatKul(_ semester: String)
}

protocol MatKulResponseProtocol: class {
    func successGetMatKul(_ response: ApiResponse<MatKulModel>)
    func failedGetMatKul(_ error: ApiError)
}

protocol MatKulDMRequestProtocol: class {
    var interactor: MatKulResponseProtocol? { get set }
    var remote: MatKulRequestProtocol { get set }
    
    init(_ interactor: MatKulResponseProtocol)
    
    func getMatKul(_ semester: String)
}
