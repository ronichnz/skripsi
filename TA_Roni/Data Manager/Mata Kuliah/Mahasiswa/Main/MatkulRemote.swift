//
//  MatkulRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 05/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class MatKulRemote: MatKulRequestProtocol {
    weak var dataManager: MatKulResponseProtocol?
    var api: API<MatKulModel> = API<MatKulModel>()
    
    func getMatKul(_ semester: String) {
        let data = ApiRequest(path: "mata_kuliah.php", method: .get, headers: [
            "Authorization": Account.getClientCredentials() ?? ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successGetMatKul, onError: dataManager.failedGetMatKul)
    }
}
