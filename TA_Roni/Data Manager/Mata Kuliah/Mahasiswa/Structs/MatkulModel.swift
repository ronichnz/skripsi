//
//  MatkulModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 05/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import SwiftyJSON

struct MatKulModel: ApiModel {
    
    var id: String
    var namaMatkul: String
    var semester: String
    var sks: String
    
    init(json: JSON) {
        id = json["id_matkul"].stringValue
        namaMatkul = json["nama_matkul"].stringValue
        semester = json["semester"].stringValue
        sks = json["sks"].stringValue
    }
}
