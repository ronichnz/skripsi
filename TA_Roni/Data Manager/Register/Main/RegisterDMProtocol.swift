//
//  RegisterDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 09/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

protocol RegisterRequestProtocol: class {
    var dataManager: RegisterResponseProtocol? { get set }
    var api: API<EmptyModel> { get set }
    
    func postRegisterDosen(params: Parameters)
    func postRegisterMahasiswa(params: Parameters)
}

protocol RegisterResponseProtocol: class {
    func successPostRegisterDosen(_ response: ApiResponse<EmptyModel>)
    func failedPostRegisterDosen(_ error: ApiError)
    
    func successPostRegisterMahasiswa(_ response: ApiResponse<EmptyModel>)
    func failedPostRegisterMahasiswa(_ error: ApiError)
}

protocol RegisterDMRequestProtocol: class {
    var interactor: RegisterResponseProtocol? { get set }
    var remote: RegisterRequestProtocol { get set }
    
    init(_ interactor: RegisterResponseProtocol)
    
    func postRegisterDosen(_ data: DosenSt)
    func postRegisterMahasiswa(_ data: MahasiswaSt)
}
