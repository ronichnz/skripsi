//
//  RegisterDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 09/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

class RegisterDataManager: RegisterDMRequestProtocol {
    weak var interactor: RegisterResponseProtocol? = nil
    var remote: RegisterRequestProtocol
    
    required init(_ interactor: RegisterResponseProtocol) {
        self.interactor = interactor
        remote = RegisterRemote()
        remote.dataManager = self
    }
    
    func postRegisterDosen(_ data: DosenSt) {
        var params: Parameters = [:]
        params["name"] = data.name
        params["phone"] = data.phone
        params["email"] = data.email
        params["password"] = data.password
        remote.postRegisterDosen(params: params)
    }
    
    func postRegisterMahasiswa(_ data: MahasiswaSt) {
        var params: Parameters = [:]
        params["nim"] = data.nim
        params["semester"] = data.semester
        params["jurusan"] = data.jurusan
        params["name"] = data.nama
        params["phone"] = data.noTelpon
        params["email"] = data.email
        params["password"] = data.password
        remote.postRegisterMahasiswa(params: params)
    }
    
}

extension RegisterDataManager: RegisterResponseProtocol {
    func successPostRegisterDosen(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPostRegisterDosen(response)
    }
    
    func failedPostRegisterDosen(_ error: ApiError) {
        interactor?.failedPostRegisterDosen(error)
    }
    
    func successPostRegisterMahasiswa(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPostRegisterMahasiswa(response)
    }
    
    func failedPostRegisterMahasiswa(_ error: ApiError) {
        interactor?.failedPostRegisterMahasiswa(error)
    }
}
