//
//  RegisterRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 09/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

class RegisterRemote: RegisterRequestProtocol {
    weak var dataManager: RegisterResponseProtocol?
    var api: API<EmptyModel> = API<EmptyModel>()
    
    func postRegisterDosen(params: Parameters) {
        let data = ApiRequest(path: "register_dosen.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return } 
        _ = api.request(data, onSuccess: dataManager.successPostRegisterDosen, onError: dataManager.failedPostRegisterDosen)
    }
    
    func postRegisterMahasiswa(params: Parameters) {
        let data = ApiRequest(path: "register_mahasiswa.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPostRegisterDosen, onError: dataManager.failedPostRegisterDosen)
    }
}
