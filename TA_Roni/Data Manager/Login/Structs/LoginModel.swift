//
//  LoginModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 04/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import SwiftyJSON

struct LoginModel: ApiModel {
    
    var id: Int
    var name: String
    var nim: String
    var phone: String
    var email: String
    
    init(json: JSON) {
         _ = Account(json: json)
        id = json["id"].intValue
        name = json["name"].stringValue
        nim = json["nim"].stringValue
        phone = json["phone"].stringValue
        email = json["email"].stringValue
    }
}
