//
//  LoginDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 15/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

protocol LoginRequestProtocol: class {
    var dataManager: LoginResponseProtocol? { get set }
    var api: API<LoginModel> { get set }
    
    func postLoginDosen(params: Parameters)
    func postLoginMahasiswa(params: Parameters)
}

protocol LoginResponseProtocol: class {
    func successPostLoginDosen(_ response: ApiResponse<LoginModel>)
    func failedPostLoginDosen(_ error: ApiError)
    
    func successPostLoginMahasiswa(_ response: ApiResponse<LoginModel>)
    func failedPostLoginMahasiswa(_ error: ApiError)
}

protocol LoginDMRequestProtocol: class {
    var interactor: LoginResponseProtocol? { get set }
    var remote: LoginRequestProtocol { get set }
    
    init(_ interactor: LoginResponseProtocol)
    
    func postLoginDosen(_ email: String, _ password: String)
    func postLoginMahasiswa(_ nim: String, _ password: String)
}
