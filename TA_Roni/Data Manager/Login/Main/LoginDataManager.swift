//
//  LoginDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 15/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

class LoginDataManager: LoginDMRequestProtocol {
    weak var interactor: LoginResponseProtocol? = nil
    var remote: LoginRequestProtocol
    
    required init(_ interactor: LoginResponseProtocol) {
        self.interactor = interactor
        remote = LoginRemote()
        remote.dataManager = self
    }
    
    func postLoginDosen(_ email: String, _ password: String) {
        var params: Parameters = [:]
        params["email"] = email
        params["password"] = password
        remote.postLoginDosen(params: params)
    }
    
    func postLoginMahasiswa(_ nim: String, _ password: String) {
        var params: Parameters = [:]
        params["nim"] = nim
        params["password"] = password
        remote.postLoginMahasiswa(params: params)
    }
    
}

extension LoginDataManager: LoginResponseProtocol {
    func successPostLoginDosen(_ response: ApiResponse<LoginModel>) {
        interactor?.successPostLoginDosen(response)
    }
    
    func failedPostLoginDosen(_ error: ApiError) {
        interactor?.failedPostLoginDosen(error)
    }
    
    func successPostLoginMahasiswa(_ response: ApiResponse<LoginModel>) {
        interactor?.successPostLoginMahasiswa(response)
    }
    
    func failedPostLoginMahasiswa(_ error: ApiError) {
        interactor?.failedPostLoginMahasiswa(error)
    }
}
