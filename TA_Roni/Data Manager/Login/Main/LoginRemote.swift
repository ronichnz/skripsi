//
//  LoginRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 15/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

class LoginRemote: LoginRequestProtocol {
    weak var dataManager: LoginResponseProtocol?
    var api: API<LoginModel> = API<LoginModel>()
    
    func postLoginDosen(params: Parameters) {
        let data = ApiRequest(path: "login_dosen.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPostLoginDosen, onError: dataManager.failedPostLoginDosen)
    }
    
    func postLoginMahasiswa(params: Parameters) {
        let data = ApiRequest(path: "login_mahasiswa.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPostLoginMahasiswa, onError: dataManager.failedPostLoginMahasiswa)
    }
}
