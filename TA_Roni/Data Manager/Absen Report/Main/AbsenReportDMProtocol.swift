//
//  AbsenReportDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 12/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol AbsenReportRequestProtocol: class {
    var dataManager: AbsenReportResponseProtocol? { get set }
    var api: API<AbsenReportModel> { get set }
    
    func getAbsenReport(_ mahasiswaID: String, _ semester: String)
}

protocol AbsenReportResponseProtocol: class {
    func successGetAbsenReport(_ response: ApiResponse<AbsenReportModel>)
    func failedGetAbsenReport(_ error: ApiError)
}

protocol AbsenReportDMRequestProtocol: class {
    var interactor: AbsenReportResponseProtocol? { get set }
    var remote: AbsenReportRequestProtocol { get set }
    
    init(_ interactor: AbsenReportResponseProtocol)
    
    func getAbsenReport(_ mahasiswaID: String, _ semester: String)
}

