//
//  AbsenReportDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 12/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class AbsenReportDataManager: AbsenReportDMRequestProtocol {
    weak var interactor: AbsenReportResponseProtocol? = nil
    var remote: AbsenReportRequestProtocol
    
    required init(_ interactor: AbsenReportResponseProtocol) {
        self.interactor = interactor
        remote = AbsenReportRemote()
        remote.dataManager = self
    }
    
    func getAbsenReport(_ mahasiswaID: String, _ semester: String) {
        remote.getAbsenReport(mahasiswaID, semester)
    }
}

extension AbsenReportDataManager: AbsenReportResponseProtocol {
    func successGetAbsenReport(_ response: ApiResponse<AbsenReportModel>) {
        interactor?.successGetAbsenReport(response)
    }
    
    func failedGetAbsenReport(_ error: ApiError) {
        interactor?.failedGetAbsenReport(error)
    }
}
