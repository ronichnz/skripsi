//
//  AbsenReportRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 12/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class AbsenReportRemote: AbsenReportRequestProtocol {
    weak var dataManager: AbsenReportResponseProtocol?
    var api: API<AbsenReportModel> = API<AbsenReportModel>()
    
    func getAbsenReport(_ mahasiswaID: String, _ semester: String) {
        let data = ApiRequest(path: "absensi_report_mahasiswa.php?id_mahasiswa=\(mahasiswaID)&semester=\(semester)", method: .get, headers: [
            "Authorization": Account.getClientCredentials() ?? ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successGetAbsenReport, onError: dataManager.failedGetAbsenReport)
    }
}
