//
//  AbsenReportDetailModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 12/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import SwiftyJSON

struct AbsenReportDetailModel: ApiModel {
    
    var pertemuanAt: String
    var scannedAt: String
    var atTime: String
    
    init(json: JSON) {
        pertemuanAt = json["pertemuan_at"].stringValue
        scannedAt = json["scanned_at"].stringValue
        atTime = json["at_time"].stringValue
    }
}
