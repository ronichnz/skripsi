//
//  AbsenReportModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 12/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import SwiftyJSON

struct AbsenReportModel: ApiModel {
    
    var atSemester: String
    var namaMahasiswa: String
    var namaMatkul: String
    var idMatkul: String
    var absen: [AbsenReportDetailModel] = []
    
    init(json: JSON) {
        atSemester = json["at_semester"].stringValue
        namaMahasiswa = json["nama_mahasiswa"].stringValue
        namaMatkul = json["nama_matkul"].stringValue
        idMatkul = json["id_matkul"].stringValue
        absen = json["absen"].arrayValue.map({ element in
            return AbsenReportDetailModel(json: element)
        })
    }
}
