//
//  GenerateQRDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 06/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class GenerateQRDataManager: GenerateQRDMRequestProtocol {
    weak var interactor: GenerateQRResponseProtocol? = nil
    var remote: GenerateQRRequestProtocol
    
    required init(_ interactor: GenerateQRResponseProtocol) {
        self.interactor = interactor
        remote = GenerateQRRemote()
        remote.dataManager = self
    }
    
    func postGenerateQR(_ data: CreateQRSt) {
        var params: Parameters = [:]
        params["matkul_id"] = data.matkulId
        params["dosen_id"] = data.dosenId
        params["pertemuan_at"] = data.pertemuanAt
        params["about"] = data.about
        params["date"] = data.date
        remote.postGenerateQR(params: params)
    }
    
}

extension GenerateQRDataManager: GenerateQRResponseProtocol {
    
    func successPostGenerateQR(_ response: ApiResponse<QRModel>) {
        interactor?.successPostGenerateQR(response)
    }
    
    func failedPostGenerateQR(_ error: ApiError) {
        interactor?.failedPostGenerateQR(error)
    }
}
