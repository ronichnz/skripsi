//
//  GenerateQRDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 06/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol GenerateQRRequestProtocol: class {
    var dataManager: GenerateQRResponseProtocol? { get set }
    var api: API<QRModel> { get set }
    
    func postGenerateQR(params: Parameters)
}

protocol GenerateQRResponseProtocol: class {
    func successPostGenerateQR(_ response: ApiResponse<QRModel>)
    func failedPostGenerateQR(_ error: ApiError)
}

protocol GenerateQRDMRequestProtocol: class {
    var interactor: GenerateQRResponseProtocol? { get set }
    var remote: GenerateQRRequestProtocol { get set }
    
    init(_ interactor: GenerateQRResponseProtocol)
    
    func postGenerateQR(_ data: CreateQRSt)
}
