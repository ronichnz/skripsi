//
//  GenerateQRRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 06/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class GenerateQRRemote: GenerateQRRequestProtocol {
    weak var dataManager: GenerateQRResponseProtocol?
    var api: API<QRModel> = API<QRModel>()
    
    func postGenerateQR(params: Parameters) {
        let data = ApiRequest(path: "generate_qr.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPostGenerateQR, onError: dataManager.failedPostGenerateQR)
    }
}
