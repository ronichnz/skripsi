//
//  QRModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 06/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import SwiftyJSON

struct QRModel: ApiModel {
    
    var qrID: Int
    var matkulID: String
    var dosenID: String
    var date: String
    
    init(json: JSON) { 
        qrID = json["id_qr"].intValue
        matkulID = json["id_matkul"].stringValue
        dosenID = json["id_dosen"].stringValue
        date = json["date"].stringValue
    }
}
