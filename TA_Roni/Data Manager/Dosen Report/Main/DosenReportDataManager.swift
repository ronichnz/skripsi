//
//  DosenReportDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class DosenReportDataManager: DosenReportDMRequestProtocol {
    weak var interactor: DosenReportResponseProtocol? = nil
    var remote: DosenReportRequestProtocol
    
    required init(_ interactor: DosenReportResponseProtocol) {
        self.interactor = interactor
        remote = DosenReportRemote()
        remote.dataManager = self
    }
    
    func getDosenReport(_ dosenId: String) {
        remote.getDosenReport(dosenId)
    }
}

extension DosenReportDataManager: DosenReportResponseProtocol {
    func successGetDosenReport(_ response: ApiResponse<DosenReportModel>) {
        interactor?.successGetDosenReport(response)
    }
    
    func failedGetDosenReport(_ error: ApiError) {
        interactor?.failedGetDosenReport(error)
    }
}
