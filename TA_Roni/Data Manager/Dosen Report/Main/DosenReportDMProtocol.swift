//
//  DosenReportDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol DosenReportRequestProtocol: class {
    var dataManager: DosenReportResponseProtocol? { get set }
    var api: API<DosenReportModel> { get set }
    
    func getDosenReport(_ dosenId: String)
}

protocol DosenReportResponseProtocol: class {
    func successGetDosenReport(_ response: ApiResponse<DosenReportModel>)
    func failedGetDosenReport(_ error: ApiError)
}

protocol DosenReportDMRequestProtocol: class {
    var interactor: DosenReportResponseProtocol? { get set }
    var remote: DosenReportRequestProtocol { get set }
    
    init(_ interactor: DosenReportResponseProtocol)
    
    func getDosenReport(_ dosenId: String)
}
