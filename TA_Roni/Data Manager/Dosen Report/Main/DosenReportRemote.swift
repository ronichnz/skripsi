//
//  DosenReportRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class DosenReportRemote: DosenReportRequestProtocol {
    weak var dataManager: DosenReportResponseProtocol?
    var api: API<DosenReportModel> = API<DosenReportModel>()
    
    func getDosenReport(_ dosenId: String) {
        let data = ApiRequest(path: "absensi_report_dosen.php?id_dosen=\(dosenId)", method: .get, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successGetDosenReport, onError: dataManager.failedGetDosenReport)
    }
}
