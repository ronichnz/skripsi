//
//  DosenReportModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import SwiftyJSON

struct DosenReportModel: ApiModel {
     
    var namaMatkul: String
    var pertemuan: [DosenPertemuanModel] = []
    
    init(json: JSON) {
        namaMatkul = json["nama_matkul"].stringValue
        pertemuan = json["pertemuan"].arrayValue.map({ element in
            return DosenPertemuanModel(json: element)
        })
    }
}
