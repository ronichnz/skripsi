//
//  DosenPertemuanModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import SwiftyJSON

struct DosenPertemuanModel: ApiModel {
      
    var mahasiswa: [AllAbsenceModel] = []
    var about: String
    var pertemuanAt: String
    var createdAt: String
    
    init(json: JSON) { 
        mahasiswa = json["mahasiswa"].arrayValue.map({ element in
            return AllAbsenceModel(json: element)
        })
        about = json["about"].stringValue
        pertemuanAt = json["pertemuan_at"].stringValue
        createdAt = json["created_at"].stringValue
    }
}
