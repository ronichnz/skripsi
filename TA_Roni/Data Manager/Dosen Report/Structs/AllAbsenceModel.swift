//
//  AllAbsenceModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import SwiftyJSON

struct AllAbsenceModel: ApiModel {
    
    var namaMahasiswa: String
    var scannedAt: String
    var atTime: String
    
    init(json: JSON) {
        namaMahasiswa = json["nama_mahasiswa"].stringValue
        scannedAt = json["scanned_at"].stringValue
        atTime = json["at_time"].stringValue
    }
}
