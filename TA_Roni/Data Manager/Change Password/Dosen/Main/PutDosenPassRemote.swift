//
//  PutDosenPassRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutDosenPasswordRemote: PutDosenPasswordRequestProtocol {
    weak var dataManager: PutDosenPasswordResponseProtocol?
    var api: API<EmptyModel> = API<EmptyModel>()
    
    func putDosenPassword(params: Parameters) {
        let data = ApiRequest(path: "change_password_dosen.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPutDosenPassword, onError: dataManager.failedPutDosenPassword)
    }
}
