//
//  PutDosenPassDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import Alamofire

protocol PutDosenPasswordRequestProtocol: class {
    var dataManager: PutDosenPasswordResponseProtocol? { get set }
    var api: API<EmptyModel> { get set }
    
    func putDosenPassword(params: Parameters)
}

protocol PutDosenPasswordResponseProtocol: class {
    func successPutDosenPassword(_ response: ApiResponse<EmptyModel>)
    func failedPutDosenPassword(_ error: ApiError)
}

protocol PutDosenPasswordDMRequestProtocol: class {
    var interactor: PutDosenPasswordResponseProtocol? { get set }
    var remote: PutDosenPasswordRequestProtocol { get set }
    
    init(_ interactor: PutDosenPasswordResponseProtocol)
    
    func putDosenPassword(_ oldPass: String, _ newPass: String, _ idDosen: String)
}
