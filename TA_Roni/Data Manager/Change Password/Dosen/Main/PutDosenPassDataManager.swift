//
//  PutDosenPassDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutDosenPasswordDataManager: PutDosenPasswordDMRequestProtocol {
    weak var interactor: PutDosenPasswordResponseProtocol? = nil
    var remote: PutDosenPasswordRequestProtocol
    
    required init(_ interactor: PutDosenPasswordResponseProtocol) {
        self.interactor = interactor
        remote = PutDosenPasswordRemote()
        remote.dataManager = self
    }
    
    func putDosenPassword(_ oldPass: String, _ newPass: String, _ idDosen: String) {
        var params: Parameters = [:]
        params["old_pass"] = oldPass
        params["new_pass"] = newPass
        params["id_dosen"] = idDosen
        remote.putDosenPassword(params: params)
    }
    
}

extension PutDosenPasswordDataManager: PutDosenPasswordResponseProtocol {
    
    func successPutDosenPassword(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPutDosenPassword(response)
    }
    
    func failedPutDosenPassword(_ error: ApiError) {
        interactor?.failedPutDosenPassword(error)
    }
}
