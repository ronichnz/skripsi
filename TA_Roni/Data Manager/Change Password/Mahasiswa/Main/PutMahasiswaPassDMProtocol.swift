//
//  PutMahasiswaPassDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol PutMahasiswaPasswordRequestProtocol: class {
    var dataManager: PutMahasiswaPasswordResponseProtocol? { get set }
    var api: API<EmptyModel> { get set }
    
    func putMahasiswaPassword(params: Parameters)
}

protocol PutMahasiswaPasswordResponseProtocol: class {
    func successPutMahasiswaPassword(_ response: ApiResponse<EmptyModel>)
    func failedPutMahasiswaPassword(_ error: ApiError)
}

protocol PutMahasiswaPasswordDMRequestProtocol: class {
    var interactor: PutMahasiswaPasswordResponseProtocol? { get set }
    var remote: PutMahasiswaPasswordRequestProtocol { get set }
    
    init(_ interactor: PutMahasiswaPasswordResponseProtocol)
    
    func putMahasiswaPassword(_ oldPass: String, _ newPass: String, _ idMahasiswa: String)
}
