//
//  PutMahasiswaPassDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutMahasiswaPasswordDataManager: PutMahasiswaPasswordDMRequestProtocol {
    weak var interactor: PutMahasiswaPasswordResponseProtocol? = nil
    var remote: PutMahasiswaPasswordRequestProtocol
    
    required init(_ interactor: PutMahasiswaPasswordResponseProtocol) {
        self.interactor = interactor
        remote = PutMahasiswaPasswordRemote()
        remote.dataManager = self
    }
    
    func putMahasiswaPassword(_ oldPass: String, _ newPass: String, _ idMahasiswa: String) {
        var params: Parameters = [:]
        params["old_pass"] = oldPass
        params["new_pass"] = newPass
        params["id_mahasiswa"] = idMahasiswa
        remote.putMahasiswaPassword(params: params)
    }
    
}

extension PutMahasiswaPasswordDataManager: PutMahasiswaPasswordResponseProtocol {
    
    func successPutMahasiswaPassword(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPutMahasiswaPassword(response)
    }
    
    func failedPutMahasiswaPassword(_ error: ApiError) {
        interactor?.failedPutMahasiswaPassword(error)
    }
}
