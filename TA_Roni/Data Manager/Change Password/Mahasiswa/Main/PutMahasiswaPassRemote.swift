//
//  PutMahasiswaPassRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutMahasiswaPasswordRemote: PutMahasiswaPasswordRequestProtocol {
    weak var dataManager: PutMahasiswaPasswordResponseProtocol?
    var api: API<EmptyModel> = API<EmptyModel>()
    
    func putMahasiswaPassword(params: Parameters) {
        let data = ApiRequest(path: "change_password_mahasiswa.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPutMahasiswaPassword, onError: dataManager.failedPutMahasiswaPassword)
    }
}
