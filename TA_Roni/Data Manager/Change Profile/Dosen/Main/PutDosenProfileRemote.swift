//
//  PutDosenProfileRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutDosenProfileRemote: PutDosenProfileRequestProtocol {
    weak var dataManager: PutDosenProfileResponseProtocol?
    var api: API<EmptyModel> = API<EmptyModel>()
    
    func putDosenProfile(params: Parameters) {
        let data = ApiRequest(path: "change_profile_dosen.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPutDosenProfile, onError: dataManager.failedPutDosenProfile)
    }
}
