//
//  PutDosenProfileDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol PutDosenProfileRequestProtocol: class {
    var dataManager: PutDosenProfileResponseProtocol? { get set }
    var api: API<EmptyModel> { get set }
    
    func putDosenProfile(params: Parameters)
}

protocol PutDosenProfileResponseProtocol: class {
    func successPutDosenProfile(_ response: ApiResponse<EmptyModel>)
    func failedPutDosenProfile(_ error: ApiError)
}

protocol PutDosenProfileDMRequestProtocol: class {
    var interactor: PutDosenProfileResponseProtocol? { get set }
    var remote: PutDosenProfileRequestProtocol { get set }
    
    init(_ interactor: PutDosenProfileResponseProtocol)
    
    func putDosenProfile(_ phone: String, _ email: String, _ idDosen: String)
}
