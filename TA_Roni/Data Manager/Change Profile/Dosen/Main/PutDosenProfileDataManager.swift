//
//  PutDosenProfileDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutDosenProfileDataManager: PutDosenProfileDMRequestProtocol {
    weak var interactor: PutDosenProfileResponseProtocol? = nil
    var remote: PutDosenProfileRequestProtocol
    
    required init(_ interactor: PutDosenProfileResponseProtocol) {
        self.interactor = interactor
        remote = PutDosenProfileRemote()
        remote.dataManager = self
    }
    
    func putDosenProfile(_ dosen: String, _ email: String, _ idDosen: String) {
        var params: Parameters = [:]
        params["phone"] = dosen
        params["email"] = email
        params["id_dosen"] = idDosen
        remote.putDosenProfile(params: params)
    }
    
}

extension PutDosenProfileDataManager: PutDosenProfileResponseProtocol {
    
    func successPutDosenProfile(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPutDosenProfile(response)
    }
    
    func failedPutDosenProfile(_ error: ApiError) {
        interactor?.failedPutDosenProfile(error)
    }
}
