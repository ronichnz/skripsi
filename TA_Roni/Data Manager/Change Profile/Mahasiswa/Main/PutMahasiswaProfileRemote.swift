//
//  PutMahasiswaProfileRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutMahasiswaProfileRemote: PutMahasiswaProfileRequestProtocol {
    weak var dataManager: PutMahasiswaProfileResponseProtocol?
    var api: API<EmptyModel> = API<EmptyModel>()
    
    func putMahasiswaProfile(params: Parameters) {
        let data = ApiRequest(path: "change_profile_mahasiswa.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPutMahasiswaProfile, onError: dataManager.failedPutMahasiswaProfile)
    }
}
