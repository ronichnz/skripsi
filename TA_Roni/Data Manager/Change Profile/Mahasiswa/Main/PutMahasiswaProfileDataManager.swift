//
//  PutMahasiswaProfileDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class PutMahasiswaProfileDataManager: PutMahasiswaProfileDMRequestProtocol {
    weak var interactor: PutMahasiswaProfileResponseProtocol? = nil
    var remote: PutMahasiswaProfileRequestProtocol
    
    required init(_ interactor: PutMahasiswaProfileResponseProtocol) {
        self.interactor = interactor
        remote = PutMahasiswaProfileRemote()
        remote.dataManager = self
    }
    
    func putMahasiswaProfile(_ phone: String, _ email: String, _ idMahasiswa: String) {
        var params: Parameters = [:]
        params["phone"] = phone
        params["email"] = email
        params["id_mahasiswa"] = idMahasiswa
        remote.putMahasiswaProfile(params: params)
    }
    
}

extension PutMahasiswaProfileDataManager: PutMahasiswaProfileResponseProtocol {
    
    func successPutMahasiswaProfile(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPutMahasiswaProfile(response)
    }
    
    func failedPutMahasiswaProfile(_ error: ApiError) {
        interactor?.failedPutMahasiswaProfile(error)
    }
}
