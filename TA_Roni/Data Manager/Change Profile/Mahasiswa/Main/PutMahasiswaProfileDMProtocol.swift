//
//  PutMahasiswaProfileDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 01/07/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol PutMahasiswaProfileRequestProtocol: class {
    var dataManager: PutMahasiswaProfileResponseProtocol? { get set }
    var api: API<EmptyModel> { get set }
    
    func putMahasiswaProfile(params: Parameters)
}

protocol PutMahasiswaProfileResponseProtocol: class {
    func successPutMahasiswaProfile(_ response: ApiResponse<EmptyModel>)
    func failedPutMahasiswaProfile(_ error: ApiError)
}

protocol PutMahasiswaProfileDMRequestProtocol: class {
    var interactor: PutMahasiswaProfileResponseProtocol? { get set }
    var remote: PutMahasiswaProfileRequestProtocol { get set }
    
    init(_ interactor: PutMahasiswaProfileResponseProtocol)
    
    func putMahasiswaProfile(_ phone: String, _ email: String, _ idMahasiswa: String)
}
