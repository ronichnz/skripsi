//
//  ScanQRRemote.swift
//  TA_Roni
//
//  Created by Cumaroni on 08/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

class ScanQRRemote: ScanQRRequestProtocol {
    weak var dataManager: ScanQRResponseProtocol?
    var api: API<EmptyModel> = API<EmptyModel>()
    
    func postScanQR(params: Parameters) {
        let data = ApiRequest(path: "scan_qr.php", method: .post, params: params, headers: [
            "Authorization": ""
        ])
        guard let dataManager = dataManager else { return }
        _ = api.request(data, onSuccess: dataManager.successPostScanQR, onError: dataManager.failedPostScanQR)
    }
}

