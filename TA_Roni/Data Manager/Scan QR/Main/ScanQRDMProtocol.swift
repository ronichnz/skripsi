//
//  ScanQRDMProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 08/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Alamofire

protocol ScanQRRequestProtocol: class {
    var dataManager: ScanQRResponseProtocol? { get set }
    var api: API<EmptyModel> { get set }
    
    func postScanQR(params: Parameters)
}

protocol ScanQRResponseProtocol: class {
    func successPostScanQR(_ response: ApiResponse<EmptyModel>)
    func failedPostScanQR(_ error: ApiError)
}

protocol ScanQRDMRequestProtocol: class {
    var interactor: ScanQRResponseProtocol? { get set }
    var remote: ScanQRRequestProtocol { get set }
    
    init(_ interactor: ScanQRResponseProtocol)
    
    func postScanQR(_ data: ScanQRSt)
}
