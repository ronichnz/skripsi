//
//  ScanQRDataManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 08/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import Alamofire

class ScanQRDataManager: ScanQRDMRequestProtocol {
    weak var interactor: ScanQRResponseProtocol? = nil
    var remote: ScanQRRequestProtocol
    
    required init(_ interactor: ScanQRResponseProtocol) {
        self.interactor = interactor
        remote = ScanQRRemote()
        remote.dataManager = self
    }
    
    func postScanQR(_ data: ScanQRSt) {
        var params: Parameters = [:]
        params["mahasiswa_id"] = data.mahasiswaId
        params["qr_id"] = data.qrId
        params["matkul_id"] = data.matkulId
        params["pertemuan_at"] = data.pertemuanAt
        params["date"] = data.date
        params["time"] = data.time
        remote.postScanQR(params: params)
    }
    
}

extension ScanQRDataManager: ScanQRResponseProtocol {
    
    func successPostScanQR(_ response: ApiResponse<EmptyModel>) {
        interactor?.successPostScanQR(response)
    }
    
    func failedPostScanQR(_ error: ApiError) {
        interactor?.failedPostScanQR(error)
    }
}
