//
//  UIButton+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 22/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UIButton {
    
    public func setUnderlineTitle(_ title: String, _ fontSize: CGFloat, _ weight: UIFont.Weight, _ color: UIColor = .black) {
        let attrib: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: fontSize, weight: weight),
            .foregroundColor: color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: title, attributes: attrib)
        self.setAttributedTitle(attributeString, for: .normal)
    }
    
}
