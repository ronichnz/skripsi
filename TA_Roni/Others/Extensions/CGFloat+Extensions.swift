//
//  CGFloat+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 20/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension CGFloat {
    /**
     //Usage
      label.font = UIFont.boldSystemFont(ofSize: Margin.f20.font)
     */
    var autoSize: CGFloat {
        switch UIDevice.current.deviceTypeHeight {
        case .iPhone4_4S, .iPhones_5_5s_5c_SE, .iPhones_6_6s_7_8, .iPhoneX_XS:
            return (self / 375) * mainScreen.width
        case .iPhones_6Plus_6sPlus_7Plus_8Plus, .iPhoneXR, .iPhoneXSMax:
            return (self / 414) * mainScreen.width
        default:
            return (self / 414) * mainScreen.width
        }
    }
}
