//
//  UIViewController+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentTextFieldAlert(
        title: String? = nil,
        message: String? = nil,
        actionTitle: String? = "Tambah",
        cancelTitle: String? = "Batal",
        inputPlaceholder: String? = nil,
        inputKeyboardType: UIKeyboardType = .default,
        inputReturnKeyType: UIReturnKeyType = .done,
        cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
        actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            textField.returnKeyType = inputReturnKeyType
        }
        
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentAlert(title: String, message: String, alertAction items: [UIAlertAction] = [], _ completion: (()->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for item in items {
            alert.addAction(item)
        }
        if items.count == 0 {
            alert.addAction(
                UIAlertAction(title: "Close", style: .cancel, handler: nil)
            )
        }
        present(alert, animated: true, completion: completion)
    }
    
    func presentActionSheet(title: String? = nil, message: String? = nil, alertAction items: [UIAlertAction] = [], _ completion: (()->Void)? = nil) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            presentAlert(title: title!, message: message!, alertAction: items, completion)
            return
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for item in items {
            alert.addAction(item)
        }
        if items.count == 0 {
            alert.addAction(
                UIAlertAction(title: "Close", style: .cancel, handler: nil)
            )
        }
        present(alert, animated: true, completion: completion)
    }
}

