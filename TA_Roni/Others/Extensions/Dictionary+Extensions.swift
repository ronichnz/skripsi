//
//  Dictionary+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 25/06/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func value(for key: Key, orAdd valueClosure: @autoclosure () -> Value) -> Value {
        if let value = self[key] {
            return value
        }
        
        let value = valueClosure()
        self[key] = value
        return value
    }
}
