//
//  UIImageView+Extensions.swift
//  MetaPropertyMember
//
//  Created by Ripin Li on 27/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit


extension UIImageView {
    
    public func setImage(_ image: UIImage, size: CGFloat = 0, isTransparent: Bool = false) {
        if isTransparent {
            self.image = image.resize(padding: size).transparent
        } else {
            self.image = image.resize(padding: size)
        }
    }
    
}
