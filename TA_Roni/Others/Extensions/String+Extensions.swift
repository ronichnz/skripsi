//
//  String+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension String {
    
    public var words: [String] {
        return components(separatedBy: .punctuationCharacters)
            .joined()
            .components(separatedBy: .whitespaces)
            .filter{!$0.isEmpty}
    }
    
    public static func empty(_ value: String?) -> Bool {
        return (value == nil || value!.isEmpty) || (value!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)).count == 0
    }
    
    public func contain(_ str: String) -> Bool {
        return (lowercased().range(of: str) != nil) ? true : false
    }
    
    public func between(min: Int = 0, max: Int) -> Bool {
        return (count >= min && count <= max)
    }
    
    public func replace(_ string: String, _ with: String) -> String {
        return replacingOccurrences(of: string, with: with)
    }
    
    public func replace(_ strings: [String], _ with: String) -> String {
        var text = self
        for str in strings {
            text = text.replace(str, with)
        }
        return text
    }
    
    public func index(of: String, startFrom: Int = 0) -> Int {
        guard let idx = range(of: of)?.lowerBound, startFrom != -1 else {
            return -1
        }
        return distance(from: self.index(startIndex, offsetBy: startFrom), to: idx)
    }
    
    public func indexes(of: Character) -> [Int] {
        return self.enumerated().filter {
            $1 == of
            }.map {
                $0.0
        }
    }
    
    public var isURL: Bool {
        let urlRegEx: String = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        return urlTest.evaluate(with: self)
    }
    
    public var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]{3,}@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public func html2Attr(_ size: CGFloat = 16) -> NSAttributedString {
        do {
            let html = NSString(string: "<span style=\"font-family: system-light; font-size: \(size)\">" + self + "</span>")
            
            let attr = try NSMutableAttributedString(
                data: (html.replacingOccurrences(of: "\r\n", with: "")).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!,
                options: [
                    .documentType: NSAttributedString.DocumentType.html,
                    .characterEncoding: String.Encoding.utf8.rawValue
                ],
                documentAttributes: nil
            )
            
            return attr
        }
        catch {
            return NSAttributedString(string: "")
        }
    }
    
    public func subStr(_ startPosition: Int, _ total: Int) -> String {
        let first = self.index(
            startIndex,
            offsetBy: startPosition
        )
        
        let last = self.index(
            endIndex,
            offsetBy: -1 * (count - total)
        )
        
        return String(self[first..<last])
    }
    
    public func convertToCurrency(symbol: String? = "") -> String {
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = symbol
        formatter.currencyGroupingSeparator = ","
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        number = NSNumber(value: Int(amountWithPrefix) ?? 0)
        
        // if first number is 0 return ""
        guard number != 0 else {
            return ""
        }
        
        return formatter.string(from: number)!.replace(".00", "")
    }
    
    public func convertToString() -> String {
        return self.replacingOccurrences(of: ",", with: "")
    }
    
    public func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    public func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
    
    public func isValidDouble(maxDecimalPlaces: Int) -> Bool {
        // Use NumberFormatter to check if we can turn the string into a number
        // and to get the locale specific decimal separator.
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        let decimalSeparator = formatter.decimalSeparator ?? ","
        
        if formatter.number(from: self) != nil {
            // Split our string at the decimal separator
            let split = self.components(separatedBy: decimalSeparator)
            
            let digits = split.count == 2 ? split.last ?? "" : ""
            
            return digits.count <= maxDecimalPlaces
        }
        
        return false // couldn't turn string into a valid number
    }
    
    func boldString(boldRange: NSRange?, fontSize: CGFloat = Margin.f16) -> NSAttributedString {
        let attrs = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)
        ]
        let boldAttribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.heavy)//boldSystemFont(ofSize: fontSize)
        ]
        let attrStr = NSMutableAttributedString(string: self, attributes: attrs)
        if let range = boldRange {
            attrStr.setAttributes(boldAttribute, range: range)
        }
        return attrStr
    }
    
    public func convertDateFormat(currentFormat: String = "yyyy-MM-dd HH:mm:ss", destinationFormat: String = "EEEE, dd MMM yyyy HH:mm", locale: String = "id_ID") -> String {
        // for more format https://nsdateformatter.com
        // EE -> day (EEEE for full character)
        // dd -> date
        // MM -> Month (MMM for convert to character and MMMM for full character)
        // yy -> Year (YYYY for 4 digit year)
        // HH -> Hour
        // mm -> Minute
        // ss -> Second
        // locale "id_ID" for Indonesia format
        let formatterGet = DateFormatter()
        formatterGet.dateFormat = currentFormat
        let formatterSet = DateFormatter()
        formatterSet.dateFormat = destinationFormat
        formatterSet.locale = Locale(identifier: locale)
        let date: Date? = formatterGet.date(from: self)
        return "\(formatterSet.string(from: date!))"
    }
    
    func generateQRCode() -> CIImage {
        let data = self.data(using: String.Encoding.ascii)
        let image = CIImage(image: UIImage(named: "ic_replay")!)
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return image! }
        qrFilter.setValue(data, forKey: "inputMessage")
        guard let qrImage = qrFilter.outputImage else { return image! }
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        guard let colorInvertFilter = CIFilter(name: "CIColorInvert") else { return image! }
        colorInvertFilter.setValue(scaledQrImage, forKey: "inputImage")
        guard let outputInvertedImage = colorInvertFilter.outputImage?.tinted(using: .primary) else { return image! }
        
        return outputInvertedImage
    } 
    
}
