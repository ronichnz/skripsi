//
//  UITableView+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UITableView {
     
    
    public func flexibleHeight() {
        rowHeight = UITableView.automaticDimension
        estimatedRowHeight = 44
    }
    
    func register(cells: [String: AnyClass?]) {
        for (key, value) in cells {
            register(value, forCellReuseIdentifier: key)
        }
    } 
    
}
