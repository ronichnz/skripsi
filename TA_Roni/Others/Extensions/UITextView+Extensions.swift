//
//  UITextView+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UITextView {
    
    public convenience init(placeholder: String) {
        self.init()
        self.accessibilityLabel = placeholder
        self.text = placeholder
        self.textColor = .lightGray
        self.autocorrectionType = .no
    } 
    
}
