//
//  UIAppearance+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 15/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UIAppearance {
    
    @discardableResult
    public func style(_ styleClosure: (Self) -> Void) -> Self {
        styleClosure(self)
        return self
    }
}


/** Applies a styling block on an element.
 
 Example Usage:
 
 button.style { b in
    b.A = X
    b.B = Y
    b.C = Z
 }
 
 */
