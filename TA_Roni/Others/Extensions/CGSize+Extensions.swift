//
//  CGSize+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 21/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension CGSize {
    
    public init(_ size: CGFloat) {
        self.init()
        width = size
        height = size
    }
    
}
