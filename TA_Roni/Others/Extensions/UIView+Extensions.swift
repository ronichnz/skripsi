//
//  UIView+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {
    
    convenience init(color: UIColor) {
        self.init()
        self.backgroundColor = color
    }
    
    public func setAsLine(height: Float = 1) {
        self.snp.makeConstraints { (make) in
            make.height.equalTo(height)
        }
        backgroundColor = .lightGray
    }
      
    
    public func setLayer(cornerRadius: CGFloat? = nil, borderWidth width: CGFloat? = nil, borderColor color: UIColor? = nil) {
        setNeedsLayout()
        layoutIfNeeded()
        if let radius = cornerRadius {
            let size = (frame.width == 0 ? frame.height : frame.width) / 2
            layer.cornerRadius = (radius == 0 ? size : radius)
        } else {
            layer.cornerRadius = 0
        }
        
        if let width = width {
            layer.borderWidth = width
        }
        if let color = color {
            layer.borderColor = color.cgColor
        }
        layer.masksToBounds = true
    }
    
    public func setShadow(offset: CGSize = CGSize(0), radius: CGFloat = 4, opacity: Float = 0.5, color: UIColor = .black) {
        // set shadow must be implement after set layer
        // set layer first and then set shadow
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = radius
//        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    } 
    
    @discardableResult
    public func sv(_ subViews: UIView...) -> UIView {
        return sv(subViews)
    }
    
    @objc @discardableResult
    public func sv(_ subViews: [UIView]) -> UIView {
        for sv in subViews {
            addSubview(sv)
            sv.translatesAutoresizingMaskIntoConstraints = false
        }
        return self
    }
    
    func moveUpViewInY(duration: Float = 0.1, delay: Float = 0.0 , distance: CGFloat, options: UIView.AnimationOptions = [], isHidden: Bool = false, direct: Bool = false, completion: ((Bool) -> Void)? = nil ) {
        UIView.animate(withDuration: TimeInterval(duration), delay: TimeInterval(delay), options: options, animations: {
        self.center.y -= distance
        if direct {
            Timer.scheduledTimer(withTimeInterval: TimeInterval(duration), repeats: false) { (time) in
                self.alpha = isHidden ? 0 : 1
            }
         } else {
            self.alpha = isHidden ? 0 : 1
        }}, completion: completion)
        
    }
    
    func hide(in duration: TimeInterval = 0.2, delay: TimeInterval = 0.0, withTransform: CGAffineTransform = CGAffineTransform(scaleX: 1.0, y: 1.0), options: UIView.AnimationOptions = [], withHiddenProperty: Bool = false , completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, delay: delay, options: options, animations: {
            if withHiddenProperty == true {
                self.isHidden = true
            }
            self.alpha = 0.0
            self.transform = withTransform
        },
            completion: completion
        )
    }
    
}



