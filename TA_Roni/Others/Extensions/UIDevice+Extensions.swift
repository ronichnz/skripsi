//
//  UIDevice+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 20/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UIDevice {
    var deviceTypeHeight: DeviceTypeEn {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX_XS
        case 1792:
            return .iPhoneXR
        case 2688:
            return .iPhoneXSMax
        default:
            return .unknown
        }
    } 
} 
