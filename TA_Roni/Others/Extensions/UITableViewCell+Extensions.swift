//
//  UITableViewCell+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 15/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    @discardableResult
    public override func sv(_ subViews: [UIView]) -> UIView {
        contentView.snp.makeConstraints { (make) in
            make.edges.width.equalTo(self)
        }
        return contentView.sv(subViews)
    } 
}
