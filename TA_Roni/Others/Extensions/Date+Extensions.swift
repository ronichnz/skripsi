//
//  Date+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import SwiftDate

extension Date {
    
    public static func now() -> DateInRegion {
        return Date().dateByAdding(7, Calendar.Component.hour)
    }
    
    public func now() -> DateInRegion {
        return self.dateByAdding(7, Calendar.Component.hour)
    }
    
    public static func currentTimeInterval() -> TimeInterval {
        return Date.timeIntervalBetween1970AndReferenceDate
    }
    
    public var minGMT7: DateInRegion {
        return self.dateByAdding(7, Calendar.Component.hour)
    }
    
    var chatRoomString: (key: String, value: String) {
        let time = self.toString(DateToStringStyles.custom("HH:mm"))
        let current = Date()
        if (current.year - self.year) != 0 {
            // beda tahun
            return (self.toString(DateToStringStyles.custom("EEE, d MMMM YY")), time)
        }
        if current.month - self.month != 0 {
            // beda bulan
            return (self.toString(DateToStringStyles.custom("EEE, d MMM")), time)
        }
        let range = current.day - self.day
        if range == 0 {
            // hari yang sama
            return ("Hari ini", time)
        } else if range == 1 || range == -1 {
            // semalam
            return ("Kemarin", time)
        }
        return (self.toString(DateToStringStyles.custom("EEE, d MMM YYYY")), time)
    }
    
    var chatListString: String {
        let current = Date()
        if (current.year - self.year) != 0 {
            return self.toString(DateToStringStyles.custom("dd/MM/yy"))
        }
        if current.month - self.month != 0 {
            return self.toString(DateToStringStyles.custom("d MMM"))
        }
        let range = current.day - self.day
        if range == 0 {
            // hari ini
            return self.toString(DateToStringStyles.custom("HH:mm"))
        } else if range == 1 || range == -1 {
            // semalam
            return "Kemarin"
        }
        return self.toString(DateToStringStyles.custom("d EEE"))
    }
    
    var differenceDate: TimeInterval {
        let currentTime = Date().timeIntervalSince1970
        let time = currentTime - self.timeIntervalSince1970
        return time
    }
    
    func convertToLastActiveFrom() -> String {
        var active: String = ""
        let currentTime = Date().now().date
        let agentTime = date + 7.hours
        
        if agentTime.month < currentTime.month {
            active = "Terakhir online \(currentTime.month - agentTime.month) bulan yang lalu"
        } else if agentTime.day < currentTime.day {
            active = "Terakhir online \(currentTime.day - agentTime.day) hari yang lalu"
        } else if agentTime.hour < currentTime.hour {
            active = "Terakhir online \(currentTime.hour - agentTime.hour) jam yang lalu"
        } else if agentTime.minute < currentTime.minute {
            active = "Terakhir online \(currentTime.minute - agentTime.minute) menit yang lalu"
        } else if agentTime.second < currentTime.second {
            active = "Terakhir online \(currentTime.second - agentTime.second) detik yang lalu"
        } else {
            active = "Baru saja"
        }
        
        return active
    }
    
}
