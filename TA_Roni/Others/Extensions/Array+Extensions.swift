//
//  Array+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension Array {
    
    public func get(_ index: Int) -> Element? {
        guard index >= 0 && index < self.count else {
            return nil
        }
        return self[index]
    }
    
    public func findIndex(value searchValue: String, in array: [String]) -> Int? {
        for (index, value) in array.enumerated() {
            if value == searchValue {
                return index
            }
        }
        return nil
    }
    
}

extension Array where Element: UIView {
    
    func style(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
}


extension Array where Element: UIBarItem {
    
    func style(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
    
}

extension Array where Element: UIGestureRecognizer {
    
    func style(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
    
}

