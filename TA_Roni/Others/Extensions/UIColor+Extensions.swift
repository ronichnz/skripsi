//
//  UIColor+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(hexa: Int) {
        let mask = 0xFF
        let limit: CGFloat = 255.0
        let red = CGFloat((hexa >> 16) & mask) / limit
        let green = CGFloat((hexa >> 8) & mask) / limit
        let blue = CGFloat(hexa & mask) / limit
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
    public func alpha(_ value: CGFloat) -> UIColor {
        return self.withAlphaComponent(value)
    }
    
    open class var primary: UIColor {
        return #colorLiteral(red: 0.09019607843, green: 0.3568627451, blue: 0.9843137255, alpha: 1)
    }
    
    open class var secondary: UIColor {
        return #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
    }
    
    open class var background: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    open class var alpha: UIColor {
        return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).alpha(0)
    }
    
    open class var backgroundAlpha: UIColor {
        return UIColor.grayAlpha.alpha(0.4)
    }
    
    open class var silverGray: UIColor {
        return #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    }
    
    open class var grayAlpha: UIColor {
        return #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5)
    }
    
    open class var blackAlpha: UIColor {
        return UIColor.black.withAlphaComponent(0.75)
    }
    
    var coreImageColor: CIColor {
        return CIColor(color: self)
    }
    
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let coreImageColor = self.coreImageColor
        return (coreImageColor.red, coreImageColor.green, coreImageColor.blue, coreImageColor.alpha)
    }
    
    func getSuitableTextColor() -> UIColor {
        let redColor = self.components.red
        let greenColor = self.components.green
        let blueColor = self.components.blue
        
        let colorBrightness = ((redColor * 299) + (greenColor * 587) + (blueColor * 114)) / 1000
        
        if colorBrightness > 0.5 {
            return UIColor.black
        } else {
            return UIColor.white
        }
    }
}
