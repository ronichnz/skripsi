//
//  UIScrollView+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit
import SnapKit

extension UIScrollView {
    
    public var content: UIView! {
        guard subviews.count != 0 else {
            addContentView(view: UIView())
            return subviews[0]
        }
        return subviews[0]
    }
    
    public func addContentView(view: UIView) {
        if subviews.count == 0 {
            addSubview(view)
            subviews[0].snp.makeConstraints({ (make) in
                make.edges.width.equalTo(self)
            })
        } else {
            debug(key: "this scrollview already have contentView", subviews)
        }
    } 
}
