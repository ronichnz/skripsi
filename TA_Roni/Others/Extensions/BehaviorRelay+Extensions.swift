//
//  BehaviorRelay+Extensions.swift
//  MetaPropertyMember
//
//  Created by Ripin Li on 18/06/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation
import RxRelay

extension BehaviorRelay where Element: RangeReplaceableCollection {
    
    func append(_ subElement: Element.Element) {
        var newValue = value
        newValue.append(subElement)
        accept(newValue)
    }
    
}
