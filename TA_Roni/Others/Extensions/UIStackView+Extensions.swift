//
//  UIStackView+Extensions.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

extension UIStackView {
    
    public func setupArrangedSubview(_ views: [UIView]) {
        for item in views {
            addArrangedSubview(item)
        }
    }
}
