
//
//  Keychain.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation
import Security

let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

public class Keychain {
    
    public class func deleteToken() {
        Keychain.LoginToken.delete()
        Keychain.PermanentToken.delete()
    }
    
    public class PermanentToken {
        public class func save(_ token: String) {
            Keychain.save(DSource.Generate.permanentToken, data: token)
        }
        
        public class func load() -> String? {
            return Keychain.loadKey(DSource.Generate.permanentToken) as? String
        }
        
        public class func delete() {
            Keychain.deletekey(DSource.Generate.permanentToken)
        }
    }
    
    public class LoginToken {
        public class func save(_ token: String) {
            Keychain.save(DSource.Generate.loginToken, data: token)
        }
        
        public class func load() -> String? {
            return Keychain.loadKey(DSource.Generate.loginToken) as? String
        }
        
        public class func delete() {
            Keychain.deletekey(DSource.Generate.loginToken)
        }
    }
    
    public class func save(_ service: String, data: String) {
        guard let dataFromString = data.data(using: .utf8, allowLossyConversion: false) else { return }
        
        let keychainQuery = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecValueDataValue])
        
        if SecItemCopyMatching(keychainQuery as CFDictionary, nil) == noErr {
            SecItemDelete(keychainQuery as CFDictionary)
            Keychain.save(service, data: data)
        }
        else {
            _ = SecItemAdd(keychainQuery as CFDictionary, nil)
        }
    }
    
    public class func loadKey(_ service: String) -> AnyObject? {
        var result : String!
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecReturnDataValue, kSecMatchLimitValue])
        
        var dataTypeRef :AnyObject?
        let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
        
        guard status == errSecSuccess else { return nil }
        
        let retrievedData : Data? = dataTypeRef as? Data
        result = String (data: retrievedData!, encoding: .utf8)
        return result as AnyObject?
    }
    
    public class func deletekey(_ key: String) {
        let keychainQuery = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, key], forKeys: [kSecClassValue, kSecAttrServiceValue])
        _ = SecItemDelete(keychainQuery as CFDictionary)
    }
    
}
