//
//  Validate.swift
//  MetaPropertyMember
//
//  Created by Ripin Li on 18/06/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation

public class Validate {
    
    static func phoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((628)|(08))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    static func emailValidation(_ email: String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]{3,}@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
