//
//  DebugData.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit
import SwiftDate

public class DebugData {
    
    public class func get(key: TimeInterval) -> String? {
        return UserDefaults.standard.string(forKey: "\(DModel.applicationName)-DebugData-\(key)-value")
    }
    
    class func save(_ value: String, forKey: TimeInterval) {
        let key = Date.currentTimeInterval()
        save(key: key)
        UserDefaults.standard.set(value, forKey: "\(DModel.applicationName)-DebugData-\(key)-value")
    }
    
    public class var keys: [TimeInterval] {
        return (UserDefaults.standard.string(forKey: "\(DModel.applicationName)-DebugData-keys")?.components(separatedBy: ";") ?? []).map({ (time) in
            return TimeInterval(time) ?? 0
        })
    }
    
    private class func save(key: TimeInterval) {
        var keys = UserDefaults.standard.string(forKey: "\(DModel.applicationName)-DebugData-keys") ?? ""
        keys += ";\(key)"
        UserDefaults.standard.set(keys, forKey: "\(DModel.applicationName)-DebugData-keys")
    }
    
    public class func remove(key: TimeInterval) {
        UserDefaults.standard.set(nil, forKey: "\(DModel.applicationName)-DebugData-keys")
    }
    
    public class func removeAll() {
        for key in DebugData.keys {
            DebugData.remove(key: key)
        }
        UserDefaults.standard.set(nil, forKey: "\(DModel.applicationName)-DebugData-keys")
    }
    
}
