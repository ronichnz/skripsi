//
//  ServerStateEn.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 13/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation

public enum ServerStateEn: String {
    case alpha = "ALPHA"
    case beta = "BETA"
    case production = "SSS-release_state-PRODUCTION"
}
