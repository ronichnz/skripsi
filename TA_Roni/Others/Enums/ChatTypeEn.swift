//
//  ChatTypeEn.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 10/09/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation 

enum ChatTypeEn: Int {
    case pingcode = 0
    case text = 1
    case image = 2
    case listing = 3
    case location = 4
    case contact = 5
    case star = 7
}
