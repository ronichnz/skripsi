//
//  GradientPointEn.swift
//  MetaPropertyMember
//
//  Created by Ripin Li on 13/06/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import UIKit

enum GradientPoint {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomTop
    case topLeftToBottomRight
    case bottomRightToTopLeft
    case topRightToBottomLeft
    case bottomLeftToTopRight
    
    func value() -> (start: CGPoint, end: CGPoint) {
        switch self {
        case .leftToRight:
            return (CGPoint(x: 0, y: 0.5), CGPoint(x: 1, y: 0.5))
        case .rightToLeft:
            return (CGPoint(x: 1, y: 0.5), CGPoint(x: 0, y: 0.5))
        case .topToBottom:
            return (CGPoint(x: 0.5, y: 0), CGPoint(x: 0.5, y: 1))
        case .bottomTop:
            return (CGPoint(x: 0.5, y: 1), CGPoint(x: 0.5, y: 0))
        case .topLeftToBottomRight:
            return (CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 1))
        case .bottomRightToTopLeft:
            return (CGPoint(x: 1, y: 1), CGPoint(x: 0, y: 0))
        case .topRightToBottomLeft:
            return (CGPoint(x: 1, y: 0), CGPoint(x: 0, y: 1))
        case .bottomLeftToTopRight:
            return (CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 0))
        }
    }
}
