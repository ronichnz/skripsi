//
//  ChatStatusEn.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 10/09/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation

enum ChatStatusEn: Int {
    case pending = 1
    case sent = 2
    case received = 3
    case unread = 4
    case read = 5
    case failed = 6
    case none = 0
}
