//
//  DeviceTypeEn.swift
//  MetaPropertyMember
//
//  Created by Cumaroni on 20/05/19.
//  Copyright © 2019 Meta_IOS. All rights reserved.
//

import Foundation

public enum DeviceTypeEn: String {
    case iPhone4_4S = "iPhone 4 or iPhone 4S"
    case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
    case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
    case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
    case iPhoneX_XS = "iPhone X, iPhone XS"
    case iPhoneXR = "iPhone XR"
    case iPhoneXSMax = "iPhone XS Max" 
    case unknown = "iPadOrUnknown"
}
