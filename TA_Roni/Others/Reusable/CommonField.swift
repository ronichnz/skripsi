//
//  CommonField.swift
//  TA_Roni
//
//  Created by Roni Aja on 02/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit
import GrowingTextView

final class CommonField: UIView {
    
    private let commonLbl = UILabel()
    let commonField = UITextField()
    
    init() {
        super.init(frame: CGRect())
        backgroundColor = .clear
        
        sv([commonLbl, commonField])
        
        setupConstraint()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var value: String? {
        get { return commonField.text } set { commonField.text = newValue }
    }
    
    func initialize(label: String, placeholder: String, keyboardType: UIKeyboardType = .default, labelColor: UIColor = .darkGray, isSecure: Bool = false) {
        commonLbl.text = label
        commonLbl.textColor = labelColor
        commonField.placeholder = placeholder
        commonField.keyboardType = keyboardType
        commonField.isSecureTextEntry = isSecure
    }
    
    private func setupView() {
        commonLbl.style {
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f18)
            $0.numberOfLines = 1
            $0.textAlignment = .left
        }
        
        commonField.style {
            $0.autocorrectionType = .no
            $0.enablesReturnKeyAutomatically = true
            $0.clearButtonMode = .whileEditing
            $0.borderStyle = .roundedRect
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.returnKeyType = .next
            $0.textColor = .darkGray
        }
    }
    
    private func setupConstraint() {
        commonLbl.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
        }
        
        commonField.snp.makeConstraints { (make) in
            make.top.equalTo(commonLbl.snp.bottom).offset(Margin.s4)
            make.leading.trailing.equalTo(commonLbl)
            make.bottom.equalTo(self)
            make.height.equalTo(Margin.s44.autoSize)
        }
    }
}
