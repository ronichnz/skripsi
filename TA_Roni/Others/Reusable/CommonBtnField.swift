//
//  CommonBtnField.swift
//  TA_Roni
//
//  Created by Cumaroni on 18/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import GrowingTextView

final class commonBtnField: UIView {
    
    private let commonLbl = UILabel()
    let commonBtn = UIButton()
    let commonImg = UIImageView()
    
    init() {
        super.init(frame: CGRect())
        backgroundColor = .clear
        
        sv([commonLbl, commonBtn, commonImg])
        
        setupConstraint()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var value: String? {
        get { return commonBtn.currentTitle } set { commonBtn.setTitle(newValue, for: .normal) }
    }
    
    func initialize(label: String, labelColor: UIColor = .darkGray) {
        commonLbl.text = label
        commonLbl.textColor = labelColor
    }
    
    private func setupView() {
        commonLbl.style {
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f18)
            $0.numberOfLines = 1
            $0.textAlignment = .left
        }
        
        commonBtn.style {
            $0.backgroundColor = .white
            $0.titleLabel?.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.contentHorizontalAlignment = .left
            $0.setTitle("Pilih", for: .normal)
            $0.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 0)
            $0.setTitleColor(.darkGray, for: .normal)
            $0.setLayer(cornerRadius: 4)
        }
        
        commonImg.style {
            $0.image = #imageLiteral(resourceName: "ic_down").transparent
            $0.tintColor = .darkGray
        }
    }
    
    private func setupConstraint() {
        commonLbl.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
        }
        
        commonBtn.snp.makeConstraints { (make) in
            make.top.equalTo(commonLbl.snp.bottom).offset(Margin.s4)
            make.leading.trailing.equalTo(commonLbl)
            make.bottom.equalTo(self)
            make.height.equalTo(Margin.s44.autoSize)
        }
        
        commonImg.snp.makeConstraints { (make) in
            make.centerY.equalTo(commonBtn)
            make.trailing.equalTo(commonBtn).offset(-Margin.s16)
            make.size.equalTo(Margin.i15.autoSize)
        }
    }
}
