//
//  EmptyAbsenceView.swift
//  TA_Roni
//
//  Created by Cumaroni on 12/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import UIKit

final class EmptyAbsenceView: UIView {
    
    let emptyView = UIView()
    let emptyImg = UIImageView()
    let emptyLbl = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .background
        
        emptyView.sv([emptyImg, emptyLbl])
        sv([emptyView])
        
        setupConstraint()
        setupView()
        showEmptyView(false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showEmptyView(_ show: Bool) {
        isHidden = !show
    }
    
    func setEmptyLbl(_ text: String) {
        emptyLbl.text = text
    }
    
    private func setupView() {
        emptyImg.image = #imageLiteral(resourceName: "ic_close")
        
        emptyLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f18, weight: .medium)
            $0.numberOfLines = 0
            $0.textColor = .darkGray 
            $0.textAlignment = .center
        }
    }
    
    private func setupConstraint() {
        emptyView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
        
        emptyImg.snp.makeConstraints { (make) in
            make.top.equalTo(emptyView)
            make.centerX.equalTo(emptyView)
        }
        
        emptyLbl.snp.makeConstraints { (make) in
            make.top.equalTo(emptyImg.snp.bottom).offset(Margin.s16)
            make.leading.trailing.bottom.equalTo(emptyView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
