//
//  LoadingView.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import UIKit

final class LoadingView: UIView {
    
    let spinner = UIActivityIndicatorView.init(style: .whiteLarge)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        sv(spinner)
        
        setupConstraint()
        setupView()
        showLoading(false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showLoading(_ show: Bool) {
        show ? spinner.startAnimating() : spinner.stopAnimating()
        isHidden = !show
    }
    
    private func setupView() {
        
    }
    
    private func setupConstraint() {
        spinner.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
        setNeedsLayout()
        layoutIfNeeded()
    }
}
