//
//  WebViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import WebKit
import RxSwift

class WebViewController: UIViewController {
    lazy var root = WebView()
    
    var disposable = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        root.webView.navigationDelegate = self
        root.webView.scrollView.delegate = self
        setupViewController()
        binding()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    private func setupViewController() {
        
    }
    
    private func binding() {
        bindBackBtn()
    }
    
    private func bindBackBtn() {
        func backBtnPressed() {
            if root.webView.canGoBack {
                root.webView.goBack()
            } else {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        root.backBtn.rx.tap.subscribe(onNext: backBtnPressed).disposed(by: disposable)
    }
    
    func setData(title: String, url: String) {
        root.titleLbl.text = title
        let myUrl = URL(string: url)!
        root.webView.load(URLRequest(url: myUrl))
    }

}

extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        root.loadingView.showLoading(false)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(WKNavigationActionPolicy.allow)
    }
}

extension WebViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
