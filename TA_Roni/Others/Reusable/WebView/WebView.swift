//
//  WebView.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import WebKit

class WebView: UIView {
    
    let backBtn = UIButton()
    let titleLbl = UILabel()
    let lineView = UIView()
    let webView = WKWebView()
    let loadingView = LoadingView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .background
        
        sv([titleLbl, lineView, webView, loadingView, backBtn])
        
        setupConstraint()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backBtn.style {
            $0.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
            $0.tintColor = .black
        }
        
        titleLbl.style {
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: Margin.f20, weight: .heavy)
            $0.textColor = .darkGray
        }
        
        lineView.style {
            $0.setAsLine(height: 0.7)
        }
        
        loadingView.showLoading(true)
    }
    
    private func setupConstraint() {
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset((safeInset().top) + Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        titleLbl.snp.makeConstraints { (make) in
            make.centerY.equalTo(backBtn)
            make.centerX.equalTo(self)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(titleLbl.snp.bottom).offset(Margin.s12)
            make.leading.trailing.equalTo(self)
        }
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(lineView.snp.bottom)
            make.leading.trailing.bottom.equalTo(self)
        }
        
        loadingView.snp.makeConstraints { (make) in
            make.edges.equalTo(webView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
