//
//  ProgressView.swift
//  TA_Roni
//
//  Created by Cumaroni on 27/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class ProgressView: UIView {
    
    private let myView = UIView()
    private let activityIndicator = UIActivityIndicatorView(style: .gray)
    private let progressBar = UIProgressView(progressViewStyle: .default)
    private let percentLbl = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isHidden = true
        
        myView.sv([activityIndicator, progressBar, percentLbl])
        sv(myView)
        
        setupConstraint()
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func isHidden(_ bool: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.isHidden = bool
        }
    }
    
    func hide() {
        isHidden(true)
        activityIndicator.stopAnimating()
        progressBar.progress = 0
    }
    
    func initialize(_ progress: Float, _ message: String? = "", backgroundClr: UIColor = .silverGray) {
        isHidden(false)
        myView.backgroundColor = backgroundClr
        percentLbl.text = String.empty(message) ? "Mohon Ditunggu" : message
        progressBar.setProgress(progress, animated: true)
        activityIndicator.startAnimating()
    }
    
    private func setupView() {
        myView.style {
            $0.setLayer(cornerRadius: 5, borderWidth: 0, borderColor: .clear)
        }
        
        activityIndicator.style {
            $0.startAnimating()
            $0.style = .gray
        }
        
        progressBar.style {
            $0.progressTintColor = .primary
            $0.trackTintColor = .lightGray
        }
        
        percentLbl.style {
            $0.alpha = 0.7
            $0.font = UIFont.systemFont(ofSize: 12)
            $0.textAlignment = .center
        }
    }
    
    private func setupConstraint() {
        myView.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(self)
        }
        
        activityIndicator.snp.makeConstraints { (make) in
            make.top.equalTo(myView.snp.top).offset(Margin.s12)
            make.centerX.equalTo(myView)
            make.size.equalTo(30)
        }
        
        progressBar.snp.makeConstraints { (make) in
            make.top.equalTo(activityIndicator.snp.bottom).offset(Margin.s12)
            make.leading.equalTo(myView.snp.leading).offset(Margin.s12)
            make.trailing.equalTo(myView.snp.trailing).offset(-Margin.s12)
        }
        
        percentLbl.snp.makeConstraints { (make) in
            make.top.equalTo(progressBar.snp.bottom).offset(Margin.s8)
            make.bottom.equalTo(myView).offset(-Margin.s12)
            make.centerX.equalTo(progressBar)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
