//
//  ProfileSt.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation 
import UIKit

struct ProfileSt {
    
    var categoryNmbr: Int
    var categoryImg: UIImage?
    var categoryLbl: String
    
    init(categoryNmbr: Int, categoryImg: UIImage?, categoryLbl: String) {
        self.categoryNmbr = categoryNmbr
        self.categoryImg = categoryImg
        self.categoryLbl = categoryLbl
    }
    
}
