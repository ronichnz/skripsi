//
//  ScanQRSt.swift
//  TA_Roni
//
//  Created by Cumaroni on 13/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

struct ScanQRSt {
    
    var mahasiswaId: String
    var matkulId: String
    var qrId: String
    var pertemuanAt: String
    var date: String
    var time: String
    
    init(mahasiswaId: String, matkulId: String, qrId: String, pertemuanAt: String, date: String, time: String) {
        self.mahasiswaId = mahasiswaId
        self.matkulId = matkulId
        self.qrId = qrId
        self.pertemuanAt = pertemuanAt
        self.date = date
        self.time = time
    }
}
