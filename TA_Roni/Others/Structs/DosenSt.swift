//
//  DosenSt.swift
//  TA_Roni
//
//  Created by Cumaroni on 31/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

struct DosenSt {
     
    var name: String
    var phone: String
    var email: String
    var password: String
    
    init(name: String, phone: String, email: String, password: String) {
        self.name = name
        self.phone = phone 
        self.email = email
        self.password = password
        
    }
}
