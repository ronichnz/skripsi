//
//  MahasiswaSt.swift
//  TA_Roni
//
//  Created by Cumaroni on 31/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

struct MahasiswaSt {
    
    var nim: String
    var semester: String
    var jurusan: String
    var nama: String
    var noTelpon: String
    var email: String
    var password: String
    
    init(nim: String, semester: String, jurusan: String, nama: String, noTelpon: String, email: String, password: String) {
        self.nim = nim
        self.semester = semester
        self.jurusan = jurusan
        self.nama = nama
        self.noTelpon = noTelpon 
        self.email = email
        self.password = password
    }
}
