//
//  CreateQRSt.swift
//  TA_Roni
//
//  Created by Cumaroni on 13/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import UIKit

struct CreateQRSt {
    
    var matkulId: String
    var dosenId: String
    var pertemuanAt: String
    var about: String
    var date: String
    
    init(matkulId: String, dosenId: String, pertemuanAt: String, about: String, date: String) {
        self.matkulId = matkulId
        self.dosenId = dosenId
        self.pertemuanAt = pertemuanAt
        self.about = about
        self.date = date
    } 
}
