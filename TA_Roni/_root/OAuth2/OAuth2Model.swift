//
//  OAuth2Model.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import SwiftyJSON

struct OAuth2Model: ApiModel {
    
    var tokenType: String?
    
    var accessToken: String?
    var accessTokenExpiredDate: Date?
    
    var refreshToken: String?
    var refreshTokenExpiredDate: Date?
    
    init(json: JSON) {
        tokenType = json["token_type"].string
        
        accessToken = json["access_token"].string
        if let exp = json["at_expires_in"].double {
            accessTokenExpiredDate = Date(timeIntervalSince1970: exp / 1000)
        }
        
        refreshToken = json["refresh_token"].string
        if let exp = json["rt_expires_in"].double {
            refreshTokenExpiredDate = Date(timeIntervalSince1970: exp / 1000)
        }
        
    }
    
    var convertedAccessToken: String? {
        guard let type = tokenType, let token = accessToken else { return nil }
        return type + " " + token
    }
    
    var convertedRefreshToken: String? {
        guard let type = tokenType, let token = refreshToken else { return nil }
        return type + " " + token
    }
    
}
