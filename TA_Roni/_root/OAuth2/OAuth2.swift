//
//  OAuth2.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation 
import Alamofire
import RxSwift
import SwiftDate
import RxRelay

class OAuth2: OAuth2Delegate {
    static let gate: DispatchSemaphore = DispatchSemaphore(value: 1)
    static var requesting: Bool = false
    
    var delegate: OAuth2ResponseDelegate?
    var clientCredentialHeaders: HTTPHeaders = [:]
    var refreshTokenHeaders: HTTPHeaders = [:]
    
    internal var clientCredentialUrl: String?
    internal var refreshTokenUrl: String?
    
//    internal var clientCredentialExpiredDate: Variable<Date?> = Variable<Date?>(nil)
//    internal var accessTokenExpiredDate: Variable<Date?> = Variable<Date?>(nil)
//    internal var refreshTokenExpiredDate: Variable<Date?> = Variable<Date?>(nil)
    internal var clientCredentialExpiredDate: BehaviorRelay<Date?> = BehaviorRelay<Date?>(value: nil)
    internal var accessTokenExpiredDate: BehaviorRelay<Date?> = BehaviorRelay<Date?>(value: nil)
    internal var refreshTokenExpiredDate: BehaviorRelay<Date?> = BehaviorRelay<Date?>(value: nil)
    
    // 5 minutes
    internal var numberOfSecondsToRefreshTokenBeforeExpired: TimeInterval = 5 * 60
    
    private var api: API<OAuth2Model> = API(superLevel: true)
    
    private var clientCredentialTimer: Timer = Timer()
    private var accessTokenTimer: Timer = Timer()
    private var refreshTokenTimer: Timer = Timer()
    
    private var failedToRequestClientCredential: Bool = false
    private var failedToRequestRefreshToken: Bool = false
    
//    private var isReachable: Variable<Bool> = Variable<Bool>(false)
    private var isReachable: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    private var disposable: DisposeBag = DisposeBag()
    
    init() {
        binding()
        startReachability()
    }
    
    deinit {
        disposable = DisposeBag()
    }
    
    func configure(clientCredentialUrl: String, refreshTokenUrl: String, interval: TimeInterval = (5 * 60)) {
        self.clientCredentialUrl = clientCredentialUrl
        self.refreshTokenUrl = refreshTokenUrl
        numberOfSecondsToRefreshTokenBeforeExpired = interval
    }
    
    func initialize(clientCredentialExpDate: Date?, accessTokenExpDate: Date?, refreshTokenExpDate: Date?) {
//        clientCredentialExpiredDate.value = clientCredentialExpDate
//        accessTokenExpiredDate.value = accessTokenExpDate
//        refreshTokenExpiredDate.value = refreshTokenExpDate
        clientCredentialExpiredDate.accept(clientCredentialExpDate)
        accessTokenExpiredDate.accept(accessTokenExpDate)
        refreshTokenExpiredDate.accept(refreshTokenExpDate)
    }
    
    private func binding() {
        isReachable.asObservable().subscribe(onNext: reachabilityDidChange).disposed(by: disposable)
        clientCredentialExpiredDate.asObservable().subscribe(onNext: startClientCredentialTimer).disposed(by: disposable)
        accessTokenExpiredDate.asObservable().subscribe(onNext: startAccessTokenTimer).disposed(by: disposable)
        refreshTokenExpiredDate.asObservable().subscribe(onNext: startRefreshTokenTimer).disposed(by: disposable)
    }
    
    private func startReachability() {
        let reachability = NetworkReachabilityManager(host: "www.apple.com")
        reachability?.listener = listenReachability
        reachability?.startListening()
    }
    
    private func requesting(status: Bool) {
        guard OAuth2.requesting != status else { return }
        DispatchQueue.global(qos: .utility).async {
            // if requesting API and current request status is false
            if status && !OAuth2.requesting {
                OAuth2.gate.wait()
            }
            // if end requesting API and current request status is true
            if !status && OAuth2.requesting {
                OAuth2.gate.signal()
            }
            OAuth2.requesting = status
        }
    }
    
    @objc func requestClientCredential() {
        delegate?.willRequestToGetClientCredential()
        requesting(status: true)
        api.request(
            ApiRequest(path: clientCredentialUrl!, method: .get, headers: clientCredentialHeaders),
            onSuccess: successGetClientCredential,
            onError: failedGetClientCredential
        )
    }
    
    @objc func requestRefreshToken() {
        delegate?.willRequestToGetRefreshToken()
        requesting(status: true)
        var params: Parameters = [:]
        params["refresh_token"] = Account.getUserRefreshToken()?.replace("Bearer ", "")
        api.request(
            ApiRequest(path: refreshTokenUrl!, method: .post, params: params, headers: refreshTokenHeaders),
            onSuccess: successGetRefreshToken,
            onError: failedGetRefreshToken
        )
    }
    
}

extension OAuth2 {
    
    private func reachabilityDidChange(status: Bool) {
        if failedToRequestClientCredential {
            requestClientCredential()
        }
        if failedToRequestRefreshToken {
            requestRefreshToken()
        }
    }
    
    private func startClientCredentialTimer(date: Date?) {
        if !String.empty(Account.getClientCredentials()) {
            clientCredentialTimer.invalidate()
            guard let date = date else { return }
            let today = Date.now().date
            let firedTime = date.addingTimeInterval(-numberOfSecondsToRefreshTokenBeforeExpired) + 7.hours
            let time = firedTime - today
            //            debug("CC FIRED TIME = \(firedTime) \n")
            //            debug("CC TODAY DATE = \(today) \n")
            //            debug("CC COUNTDOWN = \(time) \n")
            clientCredentialTimer = Timer.scheduledTimer(
                timeInterval: time.timeInterval,
                target: self,
                selector: #selector(requestClientCredential),
                userInfo: nil,
                repeats: false
            )
        }
    }
    
    private func startAccessTokenTimer(date: Date?) {
        if !String.empty(Account.getUserAccessToken()) {
            accessTokenTimer.invalidate()
            guard let date = date else { return }
            let today = Date.now().date
            let firedTime = date.addingTimeInterval(-numberOfSecondsToRefreshTokenBeforeExpired) + 7.hours
            let time = firedTime - today
            //            debug("AT FIRED TIME = \(firedTime) \n")
            //            debug("AT TODAY DATE = \(today) \n")
            //            debug("AT COUNTDOWN = \(time) \n")
            accessTokenTimer = Timer.scheduledTimer(
                timeInterval: time.timeInterval,
                target: self,
                selector: #selector(requestRefreshToken),
                userInfo: nil,
                repeats: false
            )
        }
    }
    
    private func startRefreshTokenTimer(date: Date?) {
        if !String.empty(Account.getUserRefreshToken()) {
            refreshTokenTimer.invalidate()
            guard let date = date else { return }
            let today = Date.now().date
            let firedTime = date.addingTimeInterval(-numberOfSecondsToRefreshTokenBeforeExpired) + 7.hours
            let time = firedTime - today
            //            debug("RT FIRED TIME = \(firedTime) \n")
            //            debug("RT TODAY DATE = \(today) \n")
            //            debug("RT COUNTDOWN = \(time) \n")
            refreshTokenTimer = Timer.scheduledTimer(
                timeInterval: time.timeInterval,
                target: self,
                selector: #selector(refreshTokenDidExpired),
                userInfo: nil,
                repeats: false
            )
        }
    }
    
    private func listenReachability(status: NetworkReachabilityManager.NetworkReachabilityStatus) {
//        isReachable.value = (status == .reachable(.wwan) || status == .reachable(.ethernetOrWiFi))
        isReachable.accept((status == .reachable(.wwan) || status == .reachable(.ethernetOrWiFi)))
    }
    
    @objc private func refreshTokenDidExpired() {
        delegate?.refreshTokenDidExpired()
    }
    
    private func successGetClientCredential(response: ApiResponse<OAuth2Model>) {
        failedToRequestClientCredential = false
        requesting(status: false)
//        clientCredentialExpiredDate.value = response.model.accessTokenExpiredDate
        clientCredentialExpiredDate.accept(response.model.accessTokenExpiredDate)
        delegate?.didSuccessToGetClientCredential(
            accessToken: response.model.convertedAccessToken,
            expiredDate: response.model.accessTokenExpiredDate
        )
    }
    
    private func failedGetClientCredential(error: ApiError) {
        failedToRequestClientCredential = true
        requesting(status: false)
        delegate?.didFailedToGetClientCredential(withError: error)
    }
    
    private func successGetRefreshToken(response: ApiResponse<OAuth2Model>) {
        failedToRequestRefreshToken = false
        requesting(status: false)
//        accessTokenExpiredDate.value = response.model.accessTokenExpiredDate
//        refreshTokenExpiredDate.value = response.model.refreshTokenExpiredDate
        accessTokenExpiredDate.accept(response.model.accessTokenExpiredDate)
        refreshTokenExpiredDate.accept(response.model.refreshTokenExpiredDate)
        delegate?.didSuccessToGetRefreshToken(
            accessToken: response.model.convertedAccessToken,
            refreshToken: response.model.convertedRefreshToken,
            accessTokenExpiredDate: response.model.accessTokenExpiredDate,
            refreshTokenExpiredDate: response.model.refreshTokenExpiredDate
        )
    }
    
    private func failedGetRefreshToken(error: ApiError) {
        failedToRequestRefreshToken = true
        requesting(status: false)
        delegate?.didFailedToGetRefreshToken(withError: error)
    }
    
}

