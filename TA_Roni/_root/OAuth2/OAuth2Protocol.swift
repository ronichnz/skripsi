//
//  OAuth2Protocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import RxSwift
import RxRelay

protocol OAuth2ResponseDelegate {
    
    /// Tells the delegate what will you do
    /// before request a client credential.
    func willRequestToGetClientCredential()
    
    /// Get a new client credential token and when the client credential token will expired.
    func didSuccessToGetClientCredential(accessToken: String?, expiredDate: Date?)
    
    /// Tells the delegate that there is an error
    /// when requesting client credential token.
    func didFailedToGetClientCredential(withError error: ApiError)
    
    
    
    /// Tells the delegate what will you do
    /// before request a refresh token.
    func willRequestToGetRefreshToken()
    
    /// Produce new access token and refresh token.
    func didSuccessToGetRefreshToken(accessToken: String?, refreshToken: String?, accessTokenExpiredDate: Date?, refreshTokenExpiredDate: Date?)
    
    /// Tells the delegate that there is an error
    /// when requesting refresh token action.
    ///
    /// - parameter error: Error that cause the action failed to perform
    func didFailedToGetRefreshToken(withError error: ApiError)
    
    
    
    /// Tells the delegate that the client credential token has expired.
    func clientCredentialDidExpired()
    
    /// Tells the delegate that the access token has expired.
    func accessTokenDidExpired()
    
    /// Tells the delegate that the refresh token has expired.
    func refreshTokenDidExpired()
    
}

/*
 Grant Types:
 - client_credentials <- /auth/client_credentials
 - password           <- /user/login
 - refresh_token      <- /auth/refresh_token
 - authorization_code <- /../..
 */

protocol OAuth2Delegate {
    var delegate: OAuth2ResponseDelegate? { get set }
    
    /// Tells the delegate what is the path for getting client token.
    var clientCredentialUrl: String? { get set }
    
    /// Tells the delegate what is the path for refreshing token.
    var refreshTokenUrl: String? { get set }
    
    /// Tells the delegate when the client credential token will expired.
    /// When you receive the new client credential token, you don't need to set the value.
//    var clientCredentialExpiredDate: Variable<Date?> { get set }
    var clientCredentialExpiredDate: BehaviorRelay<Date?> { get set }
    
    /// Tells the delegate when the refresh token will expired.
    /// When you receive the new refresh token, you don't need to set the value.
//    var refreshTokenExpiredDate: Variable<Date?> { get set }
    var refreshTokenExpiredDate: BehaviorRelay<Date?> { get set }
    
    /// Tells the delegate when the access token will expired.
    /// When you receive the new access token, you don't need to set the value.
//    var accessTokenExpiredDate: Variable<Date?> { get set }
    var accessTokenExpiredDate: BehaviorRelay<Date?> { get set }
    
    var clientCredentialHeaders: HTTPHeaders { get set }
    var refreshTokenHeaders: HTTPHeaders { get set }
    
    /// Range of time to refresh a new token before the access token expired
    var numberOfSecondsToRefreshTokenBeforeExpired: TimeInterval { get set }
    
}
