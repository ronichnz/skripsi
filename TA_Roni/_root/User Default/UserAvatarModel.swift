//
//  UserAvatarModel.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import UIKit

class UserAvatarModel: NSObject, NSCoding {
    
    var avatar: UIImage
    
    init(avatar: UIImage) {
        self.avatar = avatar
    }
    
    
    required convenience init?(coder aDecoder: NSCoder) {
        let avatar = aDecoder.decodeObject(forKey: "avatar") as! UIImage
        
        self.init(avatar: avatar)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(avatar, forKey: "avatar")
    }
}
