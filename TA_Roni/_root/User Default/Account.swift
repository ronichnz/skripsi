//
//  Account.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Account: ApiModel {
    
    /// Terpanggil di LoginModel
    init(json: JSON) {
        debug(json)
        Account.save(dictionary: json.dictionaryValue)
    }
    
    private static func save(dictionary: [String : JSON]) {
        for (key, value) in dictionary {
            if value != .null {
                if let dict = value.dictionary {
                    save(dictionary: dict)
                    continue
                }
                if let val = value.int {
                    Account.set(key, val)
                }
                if let val = value.string {
                    Account.set(key, val)
                }
            }
        }
    }
    
    fileprivate static func get<T>(_ key: String, _ defaultValue: T) -> T {
        return UserDefaults.standard.value(forKey: Account.key(key)) as? T ?? defaultValue
    }
    
    fileprivate static func set(_ key: String, _ myValue: Any) {
        UserDefaults.standard.set(myValue, forKey: Account.key(key))
        UserDefaults.standard.synchronize()
    }
    
    fileprivate static func key(_ value: String) -> String {
        return "STMIK-TIME-App-Account-" + value
    }
    
    static func remove(_ keys: [String]) {
        for key in keys {
            UserDefaults.standard.set(nil, forKey: Account.key(key))
            UserDefaults.standard.synchronize()
        }
    }
    
    static func clear() {
        Account.remove([
            "id_dosen",
            "id_mahasiswa",
            "nama_jurusan",
            "current_semester",
            "nim",
            "nama_mahasiswa",
            "nama_dosen",
            "email",
            "no_telepon",
            "password",
        ])
    }
    
    static func logout() {
        Keychain.deletekey("RefreshToken")
//        Keychain.deletekey("AccessToken")
        Keychain.deletekey("EmailAccount")
        Keychain.deletekey("PasswordAccount")
        clear()
    }
    
    // CLIENT CREDENTIALS EXPIRES IN
    static func getCredentialsExpireIn() -> Date? {
        let expDate: Double = Account.get("cc_expires_in", 0)
        guard expDate > 0 else { return nil }
        let date = Date(timeIntervalSince1970: expDate)
        return date
    }
    
    static func setCredentialsExpireIn(_ expiredDate: Date?) {
        guard let expiredDate = expiredDate?.timeIntervalSince1970 else { return }
        Account.set("cc_expires_in", expiredDate)
    }
    
    // ACCESS TOKEN EXPIRES IN
    static func getAccessExpireIn() -> Date? {
        let expDate: Double = Account.get("at_expires_in", 0)
        guard expDate > 0 else { return nil }
        let date = Date(timeIntervalSince1970: expDate)
        return date
    }
    
    static func setAccessExpireIn(_ expiredDate: Date?) {
        guard let expiredDate = expiredDate?.timeIntervalSince1970 else { return }
        Account.set("at_expires_in", expiredDate)
    }
    
    // REFRESH TOKEN EXPIRES IN
    static func getRefreshExpireIn() -> Date? {
        let expDate: Double = Account.get("rt_expires_in", 0)
        guard expDate > 0 else { return nil }
        let date = Date(timeIntervalSince1970: expDate)
        return date
    }
    
    static func setRefreshExpireIn(_ expiredDate: Date?) {
        guard let expiredDate = expiredDate?.timeIntervalSince1970 else { return }
        Account.set("rt_expires_in", expiredDate)
    }
    
    
    // CLIENT CREDENTIALS
    static func getClientCredentials() -> String? {
        return Keychain.loadKey("ClientCredentials") as? String
    }
    
    static func setClientCredentials(_ credentials: String) {
        Keychain.save("ClientCredentials", data: credentials)
    }
    
    // ACCESS TOKEN
    static func getUserAccessToken() -> String? {
        return Account.get("AccessToken", "")
    }
    
    static func setUserAccessToken(_ token: String)  {
        Account.set("AccessToken", token)
    }
    
    // REFRESH TOKEN
    static func getUserRefreshToken() -> String? {
        return Keychain.loadKey("RefreshToken") as? String
    }
    
    static func setUserRefreshToken(_ token: String) {
        Keychain.save("RefreshToken", data: token)
    }
    
    //id
    static var getDosenID: String {
        return Account.get("id_dosen", "")
    }
    
    static var getMahasiswaID: String {
        return Account.get("id_mahasiswa", "")
    }
    
    //nim
    static var getAccountNIM: String {
        return Account.get("nim", "")
    }
    
    //avatar
    static var getAccountAvatar: String {
        return Account.get("avatar", "")
    }
    
    //name
    static var getDosenName: String {
        return Account.get("nama_dosen", "")
    }
    
    static var getMahasiswaName: String {
        return Account.get("nama_mahasiswa", "")
    }
    
    //phone
    static var getAccountPhone: String {
        return Account.get("no_telepon", "")
    }
    
    static func setAccountPhone(_ text: String) {
        Account.set("no_telepon", text)
    }
    
    //email
    static var getAccountEmail: String {
        return Account.get("email", "")
    }
    
    static func setAccountEmail(_ text: String) {
        Account.set("email", text)
    }
    
    //semester
    static var getCurrentSemester: String {
        return Account.get("current_semester", "")
    }
    
    //jurusan
    static var getJurusan: String {
        return Account.get("nama_jurusan", "")
    }
}
