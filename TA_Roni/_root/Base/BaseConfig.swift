//
//  BaseConfig.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

class BaseConfig {
    
    static var projectID: String!
    
    static func initProjectID(_ id: String) {
        BaseConfig.projectID = id
    }
    
    class func initialize() {
        DSource.enableConstraintLogging(false)
        DSource.enableDebugLogging(true)
        DSource.enableParametersLogging(true)
        DSource.enableRestApiLogging(true)
        DSource.setupFontSize()
        #if DEBUG
        ApiManager.shared.set(baseUrl: "http://192.168.1.37:122/v1/")
        #else
        ApiManager.shared.set(baseUrl: "https://www.stmiktime.co.id/API/manager/")
        #endif
    }
}
