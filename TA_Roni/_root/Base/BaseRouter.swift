//
//  BaseRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//
 
import UIKit

class BaseRouter {
    
    class func createNavbar(_ root: UIViewController) -> UINavigationController {
        let navBar = UINavigationController(rootViewController: root)
        navBar.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navBar.navigationBar.shadowImage = UIImage()
        navBar.navigationBar.isTranslucent = true
        navBar.navigationBar.tintColor = .clear
        navBar.view.backgroundColor = .clear
        return navBar
    }
    
    class func createScanTabbar() -> UITabBarController {
        let tabbar = UITabBarController()
        tabbar.tabBar.backgroundColor = .white
        tabbar.tabBar.tintColor = .primary
        
//        let mainTabBarVc = MainViewController()
//        mainTabBarVc.tabBarItem = UITabBarItem(
//            title: "Beranda",
//            image: #imageLiteral(resourceName: "ic_home").resize(size: CGSize(Margin.i25)),
//            selectedImage: nil
//        )
        
        let scanTabBarVc = QRScannerViewController()
        scanTabBarVc.tabBarItem = UITabBarItem(
            title: "Attendance",
            image: #imageLiteral(resourceName: "ic_scanner").resize(size: CGSize(Margin.i25)),
            selectedImage: nil
        )
        
        let userTabBarVc = ProfileViewController()
        userTabBarVc.tabBarItem = UITabBarItem(
            title: "Profil",
            image: #imageLiteral(resourceName: "ic_user").resize(size: CGSize(Margin.i25)),
            selectedImage: nil
        )
        
        tabbar.viewControllers = [
//            mainTabBarVc,
            scanTabBarVc,
            userTabBarVc,
        ]
        tabbar.delegate = (UIApplication.shared.delegate as? AppDelegate)
        return tabbar
    }
    
    class func createGenerateTabbar() -> UITabBarController {
        let tabbar = UITabBarController()
        tabbar.tabBar.backgroundColor = .white
        tabbar.tabBar.tintColor = .primary
        
//        let mainTabBarVc = MainViewController()
//        mainTabBarVc.tabBarItem = UITabBarItem(
//            title: "Beranda",
//            image: #imageLiteral(resourceName: "ic_home").resize(size: CGSize(Margin.i25)),
//            selectedImage: nil
//        )
        
        let generateTabBarVc = QRGenerateViewController()
        generateTabBarVc.tabBarItem = UITabBarItem(
            title: "Attendance",
            image: #imageLiteral(resourceName: "ic_generate").resize(size: CGSize(Margin.i25)),
            selectedImage: nil
        )
        
        let userTabBarVc = ProfileViewController()
        userTabBarVc.tabBarItem = UITabBarItem(
            title: "Profil",
            image: #imageLiteral(resourceName: "ic_user").resize(size: CGSize(Margin.i25)),
            selectedImage: nil
        )
        
        tabbar.viewControllers = [
//            mainTabBarVc,
            generateTabBarVc,
            userTabBarVc,
        ]
        tabbar.delegate = (UIApplication.shared.delegate as? AppDelegate)
        return tabbar
    }
    
    func centerBtnPressed(_ sender: UIButton) {
        debug("centerpressed")
    }
    
    static func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}

