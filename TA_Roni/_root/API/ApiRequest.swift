//
//  ApiRequest.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

public struct ApiRequest {
    
    /// Link yang di pakai untuk merequest
    internal let path: String
    /**
     method yang di gunakan untuk request
     
     - Example:
     - post
     - get
     - pull
     */
    internal let method: HTTPMethod
    /**
     parameter yang di gunakan untuk request
     
     - [String: Any]
     */
    internal let params: Parameters?
    internal var attachmentKey: String?
    internal var attachmentsData: [Data] = []
    ///data seperti foto / video
    internal var attachments: [String : Data]?
    /// 304 Not Modified
    internal let cache: String?
    /// Headers authorization
    internal let headers: HTTPHeaders
    
    /// Passing Data
    internal var passing: [String : Any] = [:]
    
    public init(path: String, method: HTTPMethod, params: Parameters? = nil, passing: [String : Any] = [:], cache: String? = nil, headers: HTTPHeaders = [:]) {
        self.path = path
        self.method = method
        self.params = params
        self.passing = passing
        self.cache = cache
        self.headers = headers
    }
    
    /**
     menambah data foto / video satu persatu
     
     - Parameters:
     - key: String
     - data: Data
     */
    mutating func addAttachment(key: String, data: Data) {
        if attachments == nil {
            attachments = [:]
        }
        attachments?[key] = data
    }
    
    /**
     menset key foto / video yang akan di kirim ke api
     
     - Parameters:
     - key: param yang akan dikrim
     */
    mutating func setAttachment(key: String) {
        attachmentKey = key
    }
    
    /**
     menambah data foto / video sekaligus untuk di kirim ke api
     
     - Parameters:
     - data: file foto / video
     */
    mutating func addAttachment(data: Data) {
        attachmentsData.append(data)
    }
    
    /**
     menghapus data foto / video
     
     - Parameters:
     - key: key / param
     */
    mutating func removeAttachment(key: String) {
        attachments?[key] = nil
    }
    
}
