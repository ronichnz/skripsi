//
//  ApiResponse.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ApiResponse<Element: ApiModel>: ApiModel {
    public private(set) var data: [Element] = []
    public private(set) var model: Element! = Element(json: JSON.init(parseJSON: ""))
    public private(set) var total: Int = 0
    public private(set) var totals: [String : Int] = [:]
    public private(set) var messages: [String] = []
    public private(set) var message: String = ""
    public private(set) var JSONResponse: JSON = JSON(parseJSON: "")
    public private(set) var passing: [String : Any] = [:]
    
    public var prefferedMessage: String {
        return (messages.count > 0) ? messages[0] : ""
    }
    
    public var listMessage: String {
        return messages.joined(separator: "\n")
    }
    
    // standard response
    public init(json: JSON) {
        JSONResponse = json
        
        messages = json["messages"].arrayValue.map { value in
            return value.stringValue
        }
        
        message = json["message"].stringValue
        
        guard json["data"].arrayValue.count > 0 else {
            // model
            model = Element(json: json["data"])
            return
        }
        
        // array
        data = json["data"].arrayValue.map { value in
            return Element(json: value)
        }
        
        total = json["total"].int ?? Int(json["total"].string ?? "0") ?? data.count
    }
    
    public init(json: JSON, passing: [String : Any]) {
        JSONResponse = json
        self.passing = passing
        
        messages = json["messages"].arrayValue.map { value in
            return value.stringValue
        }
        
        guard json["data"].arrayValue.count > 0 else {
            // model
            model = Element(json: json["data"])
            return
        }
        
        // array
        data = json["data"].arrayValue.map { value in
            return Element(json: value)
        }
        
        total = json["total"].int ?? Int(json["total"].string ?? "0") ?? data.count
    }
    
    
    public init(json: JSON, dataKey: [String], passing: [String : Any]) {
        var json = json
        JSONResponse = json
        self.passing = passing
        
        messages = json["messages"].arrayValue.map { value in
            return value.stringValue
        }
        
        message = json["message"].stringValue
        
        for key in dataKey {
            json = json[key]
        }
        guard json.arrayValue.count > 0 else {
            // model
            model = Element(json: json)
            return
        }
        // array
        data = json.arrayValue.map { value in
            return Element(json: value)
        }
        
        total = json["total"].int ?? Int(json["total"].string ?? "0") ?? data.count
    }
    
    public init(json: JSON, dataKey: String) {
        JSONResponse = json
        
        messages = json["messages"].arrayValue.map { value in
            return value.stringValue
        }
        
        message = json["message"].stringValue
        
        guard json["data"][dataKey].arrayValue.count > 0 else {
            // model
            model = Element(json: json["data"][dataKey])
            return
        }
        // array
        data = json["data"][dataKey].arrayValue.map { value in
            return Element(json: value)
        }
        
        total = json["total"].int ?? Int(json["total"].string ?? "0") ?? data.count
    }
    
    public init(json: JSON, dataKey: String, totalKeys: [String]) {
        self.init(json: json, dataKey: dataKey)
        for key in totalKeys {
            totals[key] = json["data"][key].int ?? Int(json["data"][key].string ?? "0")
        }
        if totals.count == 1 {
            for (_, value) in totals {
                total = value
            }
        }
    }
}
