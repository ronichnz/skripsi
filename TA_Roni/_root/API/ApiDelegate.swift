//
//  ApiDelegate.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation 

public protocol ApiDelegate {
    
    func ApiRequestTimeOut(isReachable: Bool)
    
    func ApiNotFound(_ error: Error, isReachable: Bool)
    
    func ApiInternalServerError(_ error: Error, isReachable: Bool)
    
    func ApiInvalidToken(_ error: Error, isReachable: Bool)
    
}
