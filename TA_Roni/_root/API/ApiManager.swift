//
//  ApiManager.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    
    /// Diperlukan untuk memanggil ApiManager, dan agar BaseUrl tetap satu dalam satu aplikasi
    ///
    /// Bersifat static agar bisa di pakai seluruh class di karenakan ApiManager bersifat Private Init
    public static let shared = ApiManager()
    /// Untuk mengecek status koneksi internet user
    public var isReachable: Bool = false
    /// Untuk menentukan berbagai status koneksi internet
    private let manager = NetworkReachabilityManager(host: "www.apple.com")
    /// Link tetap yang di gunakan untuk merequest API yang biasanya berada di awal link request
    private var baseUrl: String = ""
    /// Header Authorization
    private var auth: String? = nil
    /// Api Error
    internal var delegate: ApiDelegate?
    
    private init() {
        manager?.listener = { status in
            self.isReachable = (status == .reachable(.wwan) || status == .reachable(.ethernetOrWiFi))
        }
        
        manager?.startListening()
    }
    
    /// Untuk mendapatkan link tetap
    public func getBaseUrl() -> String {
        return baseUrl
    }
    
    /// Untuk mengatur link tetap
    public func set(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    /// Untuk mendapatkan header Authorization
    public func getAuth() -> String? {
        return auth
    }
    
    /// Untuk mengatur header Authorization
    public func set(auth: String?) {
        self.auth = auth
    }
    
    public func set(delegate: ApiDelegate?) {
        self.delegate = delegate
    }
    
    public func stopRequest() {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
    
}
