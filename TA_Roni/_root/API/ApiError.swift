//
//  ApiError.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation 
import SwiftyJSON

public struct ApiError {
    
    public let json: JSON
    public let statusCode: Int
    public let localizedDescription: String
    
    public var errors: [String] {
        if let errorArr = json["error"].array {
            return errorArr.map { $0.stringValue }
        }
        return [json["error"].stringValue]
    }
    
    public var messages: [String] {
        if let messageArr = json["message"].array {
            return messageArr.map { $0.stringValue }
        }
        return [json["message"].stringValue]
    }
    
    init(json: JSON, error: String, code: Int) {
        self.json = json
        self.statusCode = code
        self.localizedDescription = error
    }
    
}
