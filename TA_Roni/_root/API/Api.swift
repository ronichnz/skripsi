//
//  Api.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation 
import Alamofire
import SwiftyJSON

public class API<T: ApiModel> {
    
    /// Digunakan pada Oauth2
    ///
    /// Apabila superLevel true ketika access & refresh token expired maka tidak bisa request
    private var superLevel: Bool
    
    init(superLevel: Bool = false) {
        self.superLevel = superLevel
    }
    
    /// Untuk merequest Authorization biasa
    private var requestHeader: HTTPHeaders {
        var headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        if let auth = ApiManager.shared.getAuth() {
            headers["Authorization"] = auth
        }
        return headers
    }
    
    /// Untuk merequest Authorization upload seperti foto / video
    private var uploadHeader: HTTPHeaders {
        var headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "multipart-form-data",
        ]
        if let auth = ApiManager.shared.getAuth() {
            headers["Authorization"] = auth
        }
        return headers
    }
    
    /// Untuk menambah header tambahan
    /// - parameter header1: headers untuk request
    /// - parameter header2: headers untuk request
    private func combineHeaders(header1: HTTPHeaders, header2: HTTPHeaders) -> HTTPHeaders {
        var result = header1
        for (key, value) in header2 {
            result[key] = value
        }
        return result
    }
    
    /// 304 Not Modified
    /// - parameter headers: headers untuk request
    /// - parameter value: String
    private func addCache(headers: HTTPHeaders, _ value: String?) -> HTTPHeaders {
        var result = headers
        if let value = value {
            result[""] = value
        }
        return result
    }
    
    /**
     Request data API
     
     - Parameters:
     - data: ApiRequest
     - onSuccess: ApiResponse
     - onError: ApiError
     - request: DataRequest
     */
    public func request(_ data: ApiRequest, onSuccess: @escaping (ApiResponse<T>)->Void, onError: @escaping (ApiError)->Void, request: ((DataRequest)->Void)? = nil) {
        /// Untuk membuat loading di status bar
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        func requestAPI() {
            DispatchQueue.global(qos: .utility).async {
                debug("\(data.path) : request")
                if !self.superLevel && OAuth2.requesting {
                    debug("\(data.path) : wait")
                    OAuth2.gate.wait()
                    debug("\(data.path) : run")
                    OAuth2.gate.signal()
                }
                
                let combinedHeader = self.combineHeaders(header1: self.requestHeader, header2: data.headers)
                let headerWithCache = self.addCache(headers: combinedHeader, data.cache)
                
                let apiRequest = Alamofire
                    .request(
                        ApiManager.shared.getBaseUrl() + data.path,
                        method: data.method,
                        parameters: data.params,
                        encoding: URLEncoding.default,
                        headers: headerWithCache
                    )
                    .validate(statusCode: 200..<300)
                    .responseJSON(completionHandler: { (response) in
                        self.responseJSON(response: response, passing: data.passing, onSuccess: onSuccess, onError: onError)
                    })
                request?(apiRequest)
            }
        }
        requestAPI()
    }
    
    /**
     Upload data seperti Foto / video ke API
     
     - Parameters:
     - data: ApiRequest
     - onSuccess: ApiResponse
     - onError: ApiError
     - progress: Download Progress
     */
    public func upload(_ data: ApiRequest, onSuccess: @escaping (ApiResponse<T>)->Void, onError: @escaping (ApiError)->Void, progress: @escaping Request.ProgressHandler) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        DispatchQueue.global(qos: .utility).async { [weak self] in
            guard let `self` = self else { return }
            
            if !self.superLevel && OAuth2.requesting {
                debug("\(data.path) : wait")
                OAuth2.gate.wait()
                debug("\(data.path) : run")
                OAuth2.gate.signal()
            }
            
            let combinedHeader = self.combineHeaders(header1: self.requestHeader, header2: data.headers)
            let headerWithCache = self.addCache(headers: combinedHeader, data.cache)
            
            Alamofire.upload(
                multipartFormData: { (form) in
                    self.createMultipartFormData(form: form, data: data)
            },
                usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold,
                to: ApiManager.shared.getBaseUrl() + data.path,
                method: data.method,
                headers: headerWithCache,
                encodingCompletion: { (upload) in
                    switch upload {
                    case .success(let request, _, _):
                        request.uploadProgress(closure: progress)
                        request
                            .validate(statusCode: 200..<300)
                            .responseJSON(completionHandler: { (response) in
                                self.responseJSON(response: response, passing: data.passing, onSuccess: onSuccess, onError: onError)
                            }
                        )
                        
                    case .failure(let error):
                        self.onRequestFailed(error, jsonData: nil, statusCode: 415, onError: onError)
                    }
                }
            )
            
        }
    }
    
    /// Untuk membuat banyak form data
    /// - parameter form: MultipartFormData
    /// - parameter data: ApiRequest
    private func createMultipartFormData(form: MultipartFormData, data: ApiRequest) {
        //PDF FILE
        if data.path.contains("label") {
            if let key = data.attachmentKey {
                for pdf in data.attachmentsData {
                    form.append(pdf, withName: key, fileName: "document.pdf", mimeType: "application/pdf")
                }
            } else {
                for (key, value) in data.attachments ?? [:] {
                    form.append(value, withName: key, fileName: "document.pdf", mimeType: "application/pdf")
                }
            }
            
            for (key, value) in data.params ?? [:] {
                if let data = "\(value)".data(using: .utf8, allowLossyConversion: false) {
                    form.append(data, withName: key, fileName: "document.pdf", mimeType: "application/pdf")
                }
            }
        } else {
            //PHOTO FILE
            if let key = data.attachmentKey {
                for image in data.attachmentsData {
                    form.append(image, withName: key, fileName: "image.jpeg", mimeType: "image/jpeg")
                }
            } else {
                for (key, value) in data.attachments ?? [:] {
                    form.append(value, withName: key, fileName: "image.jpeg", mimeType: "image/jpeg")
                }
            }
            
            for (key, value) in data.params ?? [:] {
                if let data = "\(value)".data(using: .utf8, allowLossyConversion: false) {
                    form.append(data, withName: key, fileName: "image.jpeg", mimeType: "image/jpeg")
                }
            }
        }
    }
    
    ///  hasil response dari onSuccess request
    /// - parameter response: MultipartFormData
    /// - parameter onSuccess: ApiResponse
    /// - parameter onError: ApiError
    private func responseJSON<T: ApiModel>(response: DataResponse<Any>, passing: [String : Any] = [:], onSuccess: @escaping (ApiResponse<T>)->Void, onError: @escaping (ApiError)->Void) {
        guard let statusCode = response.response?.statusCode else {
            ApiManager.shared.delegate?.ApiRequestTimeOut(isReachable: ApiManager.shared.isReachable)
            setError(onError: onError)
            return
        }
        
        switch response.result {
        case .success(let value):
            onRequestSuccess(value, statusCode: statusCode, passing: passing, onSuccess: onSuccess, onError: onError)
        case .failure(let error):
            onRequestFailed(error, jsonData: response.result.value, statusCode: statusCode, onError: onError)
        }
        
    }
    
    private func setError(onError: @escaping (ApiError)-> Void) {
        let error = ApiError(json: JSON(parseJSON: ""), error: "", code: 0)
        onError(error)
    }
    
    /// hasil respon yang sukses dari responsJSON onRequestSuccess
    /// - parameter result: Any
    /// - parameter statusCode: Int
    /// - parameter onSuccess: ApiResponse
    /// - parameter onError: ApiError
    private func onRequestSuccess<T: ApiModel>(_ result: Any, statusCode: Int, passing: [String : Any] = [:], onSuccess: @escaping (ApiResponse<T>)->Void, onError: @escaping (ApiError)->Void) {
        let json = JSON(result)
        let model = ApiResponse<T>(json: json, passing: passing)
        let error = ApiError(json: json, error: "", code: statusCode)
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if json["error"].bool == false {
                onSuccess(model)
            } else {
                onError(error)
            }
        }
    }
    
    /**
     hasil respon yang gagal dari responseJSON onRequestFailed
     
     - Parameters:
     - erorr: Error
     - jsonData: Any?
     - statusCode: Int
     - onError: ApiError
     */
    private func onRequestFailed(_ error: Error, jsonData: Any?, statusCode: Int, onError: @escaping (ApiError)->Void) {
        if isNotModifiedResponse(statusCode) {
            return
        }
        if isInvalidTokenResponse(statusCode) {
            ApiManager.shared.delegate?.ApiInvalidToken(error, isReachable: ApiManager.shared.isReachable)
            return
        }
        if isNotFoundResponse(statusCode) {
            ApiManager.shared.delegate?.ApiNotFound(error, isReachable: ApiManager.shared.isReachable)
            return
        }
        if isNotSupportedMediaTypeResponse(statusCode) {
            onError(ApiError(json: .null, error: "Unsupported Media Type", code: statusCode))
            return
        }
        if isInternalServerErrorResponse(statusCode) {
            onError(ApiError(json: .null, error: statusCodeFullText(statusCode), code: statusCode))
            ApiManager.shared.delegate?.ApiInternalServerError(error, isReachable: ApiManager.shared.isReachable)
            return
        }
        
        guard let value = jsonData else {
            onError(ApiError(json: .null, error: statusCodeFullText(statusCode), code: statusCode))
            return
        }
        let json = JSON(value)
        guard json != .null else {
            // unable to parse JSON
            onError(ApiError(json: .null, error: statusCodeFullText(statusCode), code: statusCode))
            return
        }
        // we got the failed response from server
        let error = ApiError(json: json, error: "Unable to complete the request", code: statusCode)
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            onError(error)
        }
    }
    
    /**
     Kode status respons HTTP yang menunjukkan bahwa sumber daya yang diminta belum dimodifikasi sejak transmisi sebelumnya,
     sehingga tidak perlu mengirim ulang sumber daya yang diminta ke clien
     
     - parameter statusCode: Int
     
     - returns: true if statusCode is 304
     */
    private func isNotModifiedResponse(_ statusCode: Int) -> Bool {
        return statusCode == 304
    }
    
    /**
     Kode status respons HTTP yang menunjukkan bahwa
     halaman yang Anda coba akses tidak dapat dimuat sampai Anda pertama kali masuk dengan ID pengguna dan kata sandi yang valid.
     
     - parameter statusCode: Int
     
     - returns: true if statusCode is 401
     */
    private func isInvalidTokenResponse(_ statusCode: Int) -> Bool {
        return statusCode == 401
    }
    
    /**
     Kode status respons HTTP yang menunjukkan bahwa
     situs yang diminta tetapi tidak dapat ditemukan di URL yang diberikan.
     
     - parameter statusCode: Int
     
     - returns: true if statusCode is 403 or 404
     */
    private func isNotFoundResponse(_ statusCode: Int) -> Bool {
        return (statusCode == 403 || statusCode == 404)
    }
    
    /**
     Kode status respons HTTP yang menunjukkan bahwa
     server menolak untuk menerima permintaan karena format payload dalam format yang tidak didukung.
     
     - parameter statusCode: Int
     
     - returns: true if statusCode is 415
     */
    private func isNotSupportedMediaTypeResponse(_ statusCode: Int) -> Bool {
        return statusCode == 415
    }
    
    /**
     Kode status respons HTTP yang menunjukkan bahwa
     ada yang salah di server situs web tetapi server tidak bisa lebih spesifik tentang masalah yang sebenarnya.
     
     - parameter statusCode: Int
     
     - returns: true if statusCode is 500
     */
    private func isInternalServerErrorResponse(_ statusCode: Int) -> Bool {
        return statusCode == 500
    }
    
    private func statusCodeFullText(_ code: Int) -> String {
        var result = "\(code) - "
        switch code {
        case 100: result += "Continue"
        case 101: result += "Switching Protocols"
        case 102: result += "Processing"
        case 103: result += "Early Hints"
        case 200: result += "OK"
        case 201: result += "Created"
        case 202: result += "Accepted"
        case 203: result += "Non-Authoritative Information"
        case 204: result += "No Content"
        case 205: result += "Reset Content"
        case 206: result += "Partial Content"
        case 207: result += "Multi-Status"
        case 208: result += "Already Reported"
        case 226: result += "IM Used"
        case 300: result += "Multiple Choices"
        case 301: result += "Move Permanently"
        case 302: result += "Found"
        case 303: result += "See Other"
        case 304: result += "Not Modified"
        case 305: result += "Use Proxy"
        case 306: result += "Switch Proxy"
        case 307: result += "Temporary Redirect"
        case 308: result += "Permanent Redirect"
        case 400: result += "Bad Requset"
        case 401: result += "Unauthorized"
        case 402: result += "Payment Required"
        case 403: result += "Forbidden"
        case 404: result += "Not Found"
        case 405: result += "Method Not Allowed"
        case 406: result += "Not Acceptable"
        case 407: result += "Proxy Authentication Required"
        case 408: result += "Requset Timeout"
        case 409: result += "Conflict"
        case 410: result += "Gone"
        case 411: result += "Length Required"
        case 412: result += "Precondition Failed"
        case 413: result += "Payload Too Large"
        case 414: result += "URI Too Long"
        case 415: result += "Unsupported Media Type"
        case 416: result += "Range Not Satisflable"
        case 417: result += "Expectation Falied"
        case 418: result += "I'm a teapot"
        case 421: result += "Misdirected Request"
        case 422: result += "Unprocessable Entity"
        case 423: result += "Locked"
        case 424: result += "Failed Dependency"
        case 426: result += "Upgrade Required"
        case 428: result += "Precondition Required"
        case 429: result += "Too Many Requsets"
        case 431: result += "Requset Header Fields Too Large"
        case 451: result += "Unavailable For Legal Reasons"
        case 500: result += "Internal Server Error"
        case 501: result += "Not Implemented"
        case 502: result += "Bad Gateway"
        case 503: result += "Service Unavailable"
        case 504: result += "Gateway Timeout"
        case 505: result += "HTTP Version Not Supported"
        case 506: result += "Variant Also Negotiates"
        case 507: result += "Insufficient Storage"
        case 508: result += "Loop Detected"
        case 510: result += "Not Extended"
        case 511: result += "Network Authentication Required"
        default: result += "Undefined Status Code"
        }
        return result
    }
    
}
