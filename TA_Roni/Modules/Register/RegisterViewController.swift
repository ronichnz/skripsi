//
//  RegisterViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class RegisterViewController: UIViewController {
    lazy var root = RegisterView()
    lazy var presenter: RegisterPresenterDelegate = RegisterPresenter(view: self)
    
    var disposable = DisposeBag()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    } 
    
    private func binding() {
        bindBackBtn()
        bindCloseBtn()
        bindJurusanBtn()
        bindDosenBtn()
        bindMahasiswaBtn()
        bindRegisterDosenBtn()
        bindRegisterMahasiswaBtn()
        bindDosenViewPassBtn()
        bindDosenViewRePassBtn()
        bindMahasiswaViewPassBtn()
        bindMahasiswaViewRePassBtn()
    }
    
    private func setupViewController() {
        
    }
    
    private func bindBackBtn() {
        func backBtnPressed() {
            self.showRegisterView()
        }
        root.backBtn.rx.tap.subscribe(onNext: backBtnPressed).disposed(by: disposable)
    }
    
    private func bindCloseBtn() {
        func closeBtnPressed() {
            self.dismiss(animated: true, completion: nil)
        }
        root.closeBtn.rx.tap.subscribe(onNext: closeBtnPressed).disposed(by: disposable)
    }
    
    private func bindJurusanBtn() {
        func jurusanBtnPressed() {
            self.presentActionSheet(title: "Nama Jurusan", message: "", alertAction: [
                UIAlertAction(title: "Teknik Informatika", style: .default, handler: { _ in
                    self.root.mahasiswaView.jurusanField.value = "Teknik Informatika"
                }),
                UIAlertAction(title: "Sistem Informasi", style: .default, handler: { _ in
                    self.root.mahasiswaView.jurusanField.value = "Sistem Informasi"
                }),
                UIAlertAction(title: "Manajemen", style: .default, handler: { _ in
                    self.root.mahasiswaView.jurusanField.value = "Manajemen"
                }),
                UIAlertAction(title: "Akuntansi", style: .default, handler: { _ in
                    self.root.mahasiswaView.jurusanField.value = "Akuntansi"
                }),
                UIAlertAction(title: "Administrasi Bisnis", style: .default, handler: { _ in
                    self.root.mahasiswaView.jurusanField.value = "Administrasi Bisnis"
                }),
                UIAlertAction(title: "Batal", style: .cancel, handler: nil),
                
            ])
        }
        root.mahasiswaView.jurusanField.commonBtn.rx.tap.subscribe(onNext: jurusanBtnPressed).disposed(by: disposable)
    }
    
    private func bindDosenBtn() {
        func dosenBtnPressed() {
            self.clearDosenField()
            self.showDosenView()
        }
        root.dosenBtn.rx.tap.subscribe(onNext: dosenBtnPressed).disposed(by: disposable)
    }
    
    private func bindMahasiswaBtn() {
        func mahasiswaBtnPressed() {
            self.clearMahasiswaField()
            self.showMahasiswaView()
        }
        root.mahasiswaBtn.rx.tap.subscribe(onNext: mahasiswaBtnPressed).disposed(by: disposable)
    }
    
    private func bindRegisterDosenBtn() {
        func registerBtnPressed() {
            checkDosenField()
        }
        root.dosenView.registerBtn.rx.tap.subscribe(onNext: registerBtnPressed).disposed(by: disposable)
    }
    
    private func bindRegisterMahasiswaBtn() {
        func registerBtnPressed() {
            checkMahasiswaField()
        }
        root.mahasiswaView.registerBtn.rx.tap.subscribe(onNext: registerBtnPressed).disposed(by: disposable)
    }
    
    private func bindDosenViewPassBtn() {
        let dosen = root.dosenView
        func viewPassBtnPressed() {
            if dosen.viewPassBtn.currentImage == UIImage(named: "ic_unsee") {
                dosen.passwordField.commonField.isSecureTextEntry = false
                dosen.viewPassBtn.setImage(#imageLiteral(resourceName: "ic_see"), for: .normal)
            } else {
                dosen.passwordField.commonField.isSecureTextEntry = true
                dosen.viewPassBtn.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
            }
        }
        dosen.viewPassBtn.rx.tap.subscribe(onNext: viewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindDosenViewRePassBtn() {
        let dosen = root.dosenView
        func viewPassBtnPressed() {
            if dosen.viewRePassBtn.currentImage == UIImage(named: "ic_unsee") {
                dosen.rePasswordField.commonField.isSecureTextEntry = false
                dosen.viewRePassBtn.setImage(#imageLiteral(resourceName: "ic_see"), for: .normal)
            } else {
                dosen.rePasswordField.commonField.isSecureTextEntry = true
                dosen.viewRePassBtn.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
            }
        }
        dosen.viewRePassBtn.rx.tap.subscribe(onNext: viewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindMahasiswaViewPassBtn() {
        let mahasiswa = root.mahasiswaView
        func viewPassBtnPressed() {
            if mahasiswa.viewPassBtn.currentImage == UIImage(named: "ic_unsee") {
                mahasiswa.passwordField.commonField.isSecureTextEntry = false
                mahasiswa.viewPassBtn.setImage(#imageLiteral(resourceName: "ic_see"), for: .normal)
            } else {
                mahasiswa.passwordField.commonField.isSecureTextEntry = true
                mahasiswa.viewPassBtn.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
            }
        }
        mahasiswa.viewPassBtn.rx.tap.subscribe(onNext: viewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindMahasiswaViewRePassBtn() {
        let mahasiswa = root.mahasiswaView
        func viewPassBtnPressed() {
            if mahasiswa.viewRePassBtn.currentImage == UIImage(named: "ic_unsee") {
                mahasiswa.rePasswordField.commonField.isSecureTextEntry = false
                mahasiswa.viewRePassBtn.setImage(#imageLiteral(resourceName: "ic_see"), for: .normal)
            } else {
                mahasiswa.rePasswordField.commonField.isSecureTextEntry = true
                mahasiswa.viewRePassBtn.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
            }
        }
        mahasiswa.viewRePassBtn.rx.tap.subscribe(onNext: viewPassBtnPressed).disposed(by: disposable)
    }
    
}

extension RegisterViewController: RegisterViewControllerDelegate {
    
    func showLoading(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
    func enableDosenBtn(_ enable: Bool) {
        root.dosenView.registerBtn.isEnabled = enable
    }
    
    func clearDosenField() {
        let dosenView = root.dosenView
        dosenView.nameField.value = ""
        dosenView.phoneField.value = ""
        dosenView.emailField.value = ""
        dosenView.passwordField.value = ""
        dosenView.rePasswordField.value = ""
    }
    
    func enableMahasiswaBtn(_ enable: Bool) {
        root.mahasiswaView.registerBtn.isUserInteractionEnabled = enable
    }
    
    func clearMahasiswaField() {
        let mahasiswaView = root.mahasiswaView
        mahasiswaView.nimField.value = ""
        mahasiswaView.semesterField.value = ""
        mahasiswaView.jurusanField.value = "Pilih"
        mahasiswaView.nameField.value = ""
        mahasiswaView.phoneField.value = ""
        mahasiswaView.emailField.value = ""
        mahasiswaView.passwordField.value = ""
        mahasiswaView.rePasswordField.value = ""
    }
     
}

//show_which_view
extension RegisterViewController {
    
    private func showDosenView() {
        UIView.animate(withDuration: 0.3) {
            self.root.mahasiswaView.isHidden = true
            self.root.dosenView.isHidden = false
            self.root.registerView.isHidden = true
            self.root.closeBtn.isHidden = true
            self.root.backBtn.isHidden = false
        }
    }
    
    private func showMahasiswaView() {
        UIView.animate(withDuration: 0.3) {
            self.root.mahasiswaView.isHidden = false
            self.root.dosenView.isHidden = true
            self.root.registerView.isHidden = true
            self.root.closeBtn.isHidden = true
            self.root.backBtn.isHidden = false
        }
    }
    
    private func showRegisterView() {
        UIView.animate(withDuration: 0.3) {
            self.root.mahasiswaView.isHidden = true
            self.root.dosenView.isHidden = true
            self.root.registerView.isHidden = false
            self.root.closeBtn.isHidden = false
            self.root.backBtn.isHidden = true
        }
    }
}

extension RegisterViewController {
    
    //REGISTER_DOSEN
    private func checkDosenField() {
        let dosenView = self.root.dosenView
        if checkFieldDosenFilled() {
            if (dosenView.phoneField.value?.count ?? 0) < 10 {
                self.presenter.presentErrorAlert("No Telepon tidak valid")
                return
            }
            
            if !Validate.emailValidation(dosenView.emailField.value ?? "") {
                self.presenter.presentErrorAlert("Email anda tidak valid")
                return
            }
            
            if (dosenView.passwordField.value?.count ?? 0) < 7 {
                self.presenter.presentErrorAlert("Password harus memiliki lebih dari 6 karakter")
                return
            }
            
            if dosenView.passwordField.value != dosenView.rePasswordField.value {
                self.presenter.presentErrorAlert("Password dan Konfirmasi Password harus sama")
                return
            }
            
            showLoading(true)
            enableDosenBtn(false)
            registerDosen()
        } else {
            self.presenter.presentErrorAlert("Terdapat kesalahan dalam form yang anda input")
        }
    }
    
    private func checkFieldDosenFilled() -> Bool {
        let dosenView = root.dosenView
        return !String.empty(dosenView.nameField.value) && !String.empty(dosenView.phoneField.value) && !String.empty(dosenView.emailField.value) && !String.empty(dosenView.passwordField.value) && !String.empty(dosenView.rePasswordField.value)
    }
    
    private func registerDosen() {
        let dosenView = root.dosenView
        let data = DosenSt(
            name: dosenView.nameField.value ?? "",
            phone: dosenView.phoneField.value ?? "",
            email: dosenView.emailField.value ?? "",
            password: dosenView.passwordField.value ?? ""
        )
        presenter.postRegisterDosen(data)
    }
    
    
    //REGISTER_MAHASISWA
    private func checkMahasiswaField() {
        let mahasiswaView = self.root.mahasiswaView
        if checkFieldMahasiswaFilled() {
            if (mahasiswaView.phoneField.value?.count ?? 0) < 10 {
                self.presenter.presentErrorAlert("No Telepon tidak valid")
                return
            }
            
            if !Validate.emailValidation(mahasiswaView.emailField.value ?? "") {
                self.presenter.presentErrorAlert("Email anda tidak valid")
                return
            }
            
            if (mahasiswaView.passwordField.value?.count ?? 0) < 7 {
                self.presenter.presentErrorAlert("Password harus memiliki lebih dari 6 karakter")
                return
            }
                
            if mahasiswaView.passwordField.value != mahasiswaView.rePasswordField.value {
                self.presenter.presentErrorAlert("Password dan Konfirmasi Password harus sama")
                return
            }
            
            showLoading(true)
//            enableMahasiswaBtn(false)
            registerMahasiswa()
            
        } else { 
            self.presenter.presentErrorAlert("Data yang anda masukkan belum komplit")
        }
        
    }
    
    private func checkFieldMahasiswaFilled() -> Bool {
        let mahasiswaView = root.mahasiswaView
        return !String.empty(mahasiswaView.nimField.value)
            && !String.empty(mahasiswaView.semesterField.value)
            && mahasiswaView.jurusanField.value != "Pilih"
            && !String.empty(mahasiswaView.nameField.value)
            && !String.empty(mahasiswaView.phoneField.value)
            && !String.empty(mahasiswaView.emailField.value)
            && !String.empty(mahasiswaView.passwordField.value)
            && !String.empty(mahasiswaView.rePasswordField.value)
    }
    
    private func registerMahasiswa() {
        let mahasiswaView = root.mahasiswaView
        let data = MahasiswaSt(
            nim: mahasiswaView.nimField.value ?? "",
            semester: mahasiswaView.semesterField.value ?? "",
            jurusan: mahasiswaView.jurusanField.value ?? "",
            nama: mahasiswaView.nameField.value ?? "",
            noTelpon: mahasiswaView.phoneField.value ?? "",
            email: mahasiswaView.emailField.value ?? "",
            password: mahasiswaView.passwordField.value ?? ""
        )
        presenter.postRegisterMahasiswa(data)
    }
    
}
