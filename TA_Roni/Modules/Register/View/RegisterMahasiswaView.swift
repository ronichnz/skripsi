//
//  RegisterMahasiswaView.swift
//  TA_Roni
//
//  Created by Cumaroni on 13/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class RegisterMahasiswaView: UIView {
     
    let registerLbl = UILabel()
    
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let stackView = UIStackView()
    
    let nimField = CommonField()
    let semesterField = CommonField()
    let jurusanField = commonBtnField()
    let nameField = CommonField()
    let phoneField = CommonField()
    let emailField = CommonField()
    let passwordField = CommonField()
    let viewPassBtn = UIButton()
    let rePasswordField = CommonField()
    let viewRePassBtn = UIButton()
    
    let registerBtn = UIButton()
    let agreementLbl = UILabel()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .clear
        
        stackView.setupArrangedSubview([
            nimField,
            semesterField,
            jurusanField,
            nameField,
            phoneField,
            emailField,
            passwordField,
            rePasswordField,
        ])
        
        contentView.sv([
            stackView,
            viewPassBtn,
            viewRePassBtn,
            registerBtn,
            agreementLbl,
        ])
        scrollView.sv([contentView])
        sv([registerLbl, scrollView])
        
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        registerLbl.text = "Daftar sebagai Mahasiswa"
        nimField.initialize(label: "NIM", placeholder: "Ex: 1644062", keyboardType: .numberPad, labelColor: .white)
        semesterField.initialize(label: "Semester", placeholder: "Semester anda saat ini", keyboardType: .numberPad, labelColor: .white)
        jurusanField.initialize(label: "Nama Jurusan", labelColor: .white)
        phoneField.initialize(label: "No. Telp", placeholder: "Masukkan no telepon anda", keyboardType: .numberPad, labelColor: .white)
        nameField.initialize(label: "Nama", placeholder: "Masukkan nama anda", labelColor: .white)
        emailField.initialize(label: "Email", placeholder: "Masukkan email anda", keyboardType: .emailAddress, labelColor: .white)
        passwordField.initialize(label: "Password", placeholder: "Harus 6+ karakter", labelColor: .white, isSecure: true)
        rePasswordField.initialize(label: "Konfirmasi Password", placeholder: "Masukkan ulang password anda", labelColor: .white, isSecure: true)
        registerBtn.setTitle("DAFTAR", for: .normal)
        agreementLbl.text = "Dengan mendaftar, anda menyetujui Ketentuan Layanan dan Kebijakan Privasi kami"
    }
    
    private func setupView() {
        scrollView.style {
            $0.bounces = false
            $0.showsVerticalScrollIndicator = false
        }
        
        registerLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f20, weight: .semibold)
            $0.textAlignment = .center
            $0.textColor = .white
        }
        
        stackView.style {
            $0.axis = .vertical
            $0.spacing = Margin.s12
        }
        
        registerBtn.style {
            $0.backgroundColor = .white
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f16)
            $0.setTitleColor(.secondary, for: .normal)
            $0.setLayer(cornerRadius: 8)
        }
        
        agreementLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f14)
            $0.numberOfLines = 0
            $0.textAlignment = .center
            $0.textColor = .white
        }
        
        [viewPassBtn, viewRePassBtn].style {
            $0.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
        }
    }
    
    private func setupConstraint() {
        registerLbl.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
        }
        
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(registerLbl.snp.bottom).offset(Margin.s32)
            make.leading.trailing.bottom.equalTo(self)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.edges.width.equalTo(scrollView)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView)
            make.leading.equalTo(contentView).offset(Margin.s8)
            make.trailing.equalTo(contentView).offset(-Margin.s8)
        }
        
        viewPassBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordField.commonField)
            make.trailing.equalTo(passwordField).offset(-Margin.s12)
            make.size.equalTo(Margin.i25.autoSize)
        }
               
        viewRePassBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(rePasswordField.commonField)
            make.trailing.equalTo(rePasswordField).offset(-Margin.s12)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        registerBtn.snp.makeConstraints { (make) in
            make.top.equalTo(stackView.snp.bottom).offset(Margin.s24)
            make.leading.trailing.equalTo(registerLbl)
            make.height.equalTo(Margin.s44.autoSize)
        }
        
        agreementLbl.snp.makeConstraints { (make) in
            make.top.equalTo(registerBtn.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(registerLbl)
            make.bottom.equalTo(contentView).offset(-Margin.s12)
        }
         
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
