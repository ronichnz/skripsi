//
//  RegisterProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

protocol RegisterViewControllerDelegate: AnyObject {
    
    func showLoading(_ show: Bool)
    func enableDosenBtn(_ enable: Bool)
    func clearDosenField()
    func enableMahasiswaBtn(_ enable: Bool)
    func clearMahasiswaField()
     
}//VIEW_CONTROLLER

protocol RegisterPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func postRegisterDosen(_ data: DosenSt)
    func postRegisterMahasiswa(_ data: MahasiswaSt)
    
    func presentErrorAlert(_ message: String)
    
}//PRESENTER

protocol RegisterRouterDelegate: AnyObject {
     
    func presentSuccessRegisAlert() 
    func presentErrorAlert(_ message: String)

}//ROUTER

protocol RegisterInteractorInputDelegate: AnyObject {
    
    func postRegisterDosen(_ data: DosenSt)
    func postRegisterMahasiswa(_ data: MahasiswaSt)
    
}//INTERACTOR_INPUT

protocol RegisterInteractorOutputDelegate: AnyObject {
    
    func successPostRegisterDosen(_ response: ApiResponse<EmptyModel>)
    func failedPostRegisterDosen(_ error: ApiError)
    
    func successPostRegisterMahasiswa(_ response: ApiResponse<EmptyModel>)
    func failedPostRegisterMahasiswa(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
