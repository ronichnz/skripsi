//
//  RegisterRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class RegisterRouter: RegisterRouterDelegate {
    
    weak var source: UIViewController?
    
    init(view: RegisterViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func presentSuccessRegisAlert() {
        source?.presentAlert(title: "Berhasil", message: "Selamat anda berhasil mendaftar", alertAction: [
            UIAlertAction(title: "Login", style: .default, handler: { _ in
                self.source?.dismiss(animated: true, completion: nil)
            })
        ])
    } 
    
    func presentErrorAlert(_ message: String) {
        source?.presentAlert(title: "Error", message: message)
    }
}
