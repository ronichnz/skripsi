//
//  RegisterView.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class RegisterView: UIView {
    
    let backgroundImg = UIImageView()
    
    let registerView = UIView()
    let titleLbl = UILabel()
    let registerLbl = UILabel()
    
    let dosenBtn = UIButton()
    let mahasiswaBtn = UIButton()
     
    let shadeView = UIView()
    
    let dosenView = RegisterDosenView()
    let mahasiswaView = RegisterMahasiswaView()
    
    let backBtn = UIButton()
    let closeBtn = UIButton()
    
    let loadView = LoadingView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .background
        
        registerView.sv([registerLbl, dosenBtn, mahasiswaBtn])
        sv([backgroundImg, shadeView, titleLbl, registerView, dosenView, mahasiswaView, backBtn, closeBtn, loadView])
        
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        registerLbl.text = "Anda ingin mendaftar sebagai?"
        dosenBtn.setTitle("DOSEN", for: .normal)
        mahasiswaBtn.setTitle("MAHASISWA", for: .normal)
        titleLbl.text = "STMIK TIME"
    }
    
    private func setupView() {
        backgroundImg.style {
            $0.setImage(#imageLiteral(resourceName: "ic_background"))
        }
        
        shadeView.style {
            $0.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }
        
        titleLbl.style {
            $0.textColor = .white
            $0.font = UIFont.systemFont(ofSize: Margin.f20, weight: .semibold)
            $0.textAlignment = .center
        }
        
        registerLbl.style {
            $0.textColor = .white
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f22)
            $0.textAlignment = .center
            $0.numberOfLines = 0
        }
        
        [dosenBtn, mahasiswaBtn].style {
            $0.backgroundColor = .white
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f16)
            $0.setTitleColor(.primary, for: .normal)
            $0.setLayer(cornerRadius: $0.frame.height / 2)
        }
        
        dosenBtn.style {
            $0.setTitleColor(.primary, for: .normal)
        }
        
        mahasiswaBtn.style {
            $0.setTitleColor(.secondary, for: .normal)
        }
        
        closeBtn.style {
            $0.setImage(#imageLiteral(resourceName: "ic_close").transparent, for: .normal)
            $0.tintColor = .white
        }
        
        backBtn.style {
            $0.setImage(#imageLiteral(resourceName: "ic_back").transparent, for: .normal)
            $0.tintColor = .white
        }
        
        [dosenView, mahasiswaView, backBtn].style {
            $0.isHidden = true
        }
    }
    
    private func setupConstraint() {
        backgroundImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        shadeView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(safeInset().top + Margin.s24)
            make.leading.equalTo(self).offset(Margin.s12)
            make.trailing.equalTo(self).offset(-Margin.s12)
        }
        
        closeBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLbl)
            make.trailing.equalTo(self).offset(-Margin.s16)
            make.size.equalTo(Margin.i20.autoSize)
        }
        
        backBtn.snp.makeConstraints { (make) in
            make.centerY.size.equalTo(closeBtn)
            make.leading.equalTo(self).offset(Margin.s16)
        }
        
        registerView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLbl.snp.bottom).offset(Margin.s44)
            make.leading.trailing.equalTo(titleLbl)
        }
        
        registerLbl.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(registerView)
        }
         
        dosenBtn.snp.makeConstraints { (make) in
            make.top.equalTo(registerLbl.snp.bottom).offset(Margin.s24)
            make.leading.equalTo(registerView).offset(Margin.s12)
            make.trailing.equalTo(registerView).offset(-Margin.s12)
            make.height.equalTo(Margin.s44.autoSize)
        }
        
        mahasiswaBtn.snp.makeConstraints { (make) in
            make.top.equalTo(dosenBtn.snp.bottom).offset(Margin.s12)
            make.leading.trailing.equalTo(dosenBtn)
            make.bottom.equalTo(registerView)
            make.height.equalTo(Margin.s44.autoSize)
        }
        
        dosenView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(registerLbl)
            make.bottom.equalTo(self)
        }
        
        mahasiswaView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(registerLbl)
            make.bottom.equalTo(self)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
