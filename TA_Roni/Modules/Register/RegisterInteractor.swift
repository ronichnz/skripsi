//
//  RegisterInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class RegisterInteractor {
    weak var presenter: RegisterInteractorOutputDelegate?
    
    private var registerDM: RegisterDMRequestProtocol?
    
    init(presenter: RegisterInteractorOutputDelegate?) {
        self.presenter = presenter
        registerDM = RegisterDataManager(self)
    }
    
}//MAIN

extension RegisterInteractor: RegisterInteractorInputDelegate {
    
    func postRegisterDosen(_ data: DosenSt) {
        registerDM?.postRegisterDosen(data)
    }
    
    func postRegisterMahasiswa(_ data: MahasiswaSt) {
        registerDM?.postRegisterMahasiswa(data)
    }
    
}//DELEGATE

extension RegisterInteractor: RegisterResponseProtocol {
    
    func successPostRegisterDosen(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPostRegisterDosen(response)
    }
    
    func failedPostRegisterDosen(_ error: ApiError) {
        presenter?.failedPostRegisterDosen(error)
    }
    
    func successPostRegisterMahasiswa(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPostRegisterMahasiswa(response)
    }
    
    func failedPostRegisterMahasiswa(_ error: ApiError) {
        presenter?.failedPostRegisterMahasiswa(error)
    }
    
}//DATA_MANAGER
