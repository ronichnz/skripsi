//
//  RegisterPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 07/11/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class RegisterPresenter {
    weak var view: RegisterViewControllerDelegate?
    lazy var router: RegisterRouterDelegate = RegisterRouter(view: view)
    lazy var interactor: RegisterInteractorInputDelegate = RegisterInteractor(presenter: self)
    
    init(view: RegisterViewControllerDelegate?) {
        self.view = view
    }
    
    func setBannerData() {
        
    }
    
}//MAIN

extension RegisterPresenter: RegisterPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func postRegisterDosen(_ data: DosenSt) {
        interactor.postRegisterDosen(data)
    }
    
    func postRegisterMahasiswa(_ data: MahasiswaSt) {
        interactor.postRegisterMahasiswa(data)
    }
    
    func presentErrorAlert(_ message: String) {
        router.presentErrorAlert(message)
    }
    
    
}//DELEGATE

extension RegisterPresenter: RegisterInteractorOutputDelegate {
    
    func successPostRegisterDosen(_ response: ApiResponse<EmptyModel>) {
        debug(response.JSONResponse)
        view?.showLoading(false)
//        view?.enableDosenBtn(true)
        view?.clearDosenField()
        router.presentSuccessRegisAlert()
    }
    
    func failedPostRegisterDosen(_ error: ApiError) {
        debug(error.json)
        view?.showLoading(false)
//        view?.enableDosenBtn(true)
        router.presentErrorAlert(error.messages.joined())
    }
    
    func successPostRegisterMahasiswa(_ response: ApiResponse<EmptyModel>) {
        view?.showLoading(false)
//        view?.enableMahasiswaBtn(true)
        view?.clearMahasiswaField()
        router.presentSuccessRegisAlert()
    }
    
    func failedPostRegisterMahasiswa(_ error: ApiError) {
        view?.showLoading(false)
//        view?.enableMahasiswaBtn(true)
        router.presentErrorAlert(error.messages.joined())
    }
    
    
}//INTERACTOR
