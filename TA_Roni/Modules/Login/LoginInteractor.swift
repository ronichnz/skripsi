//
//  LoginInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class LoginInteractor {
    weak var presenter: LoginInteractorOutputDelegate?
    
    private var loginDM: LoginDMRequestProtocol?
    
    init(presenter: LoginInteractorOutputDelegate?) {
        self.presenter = presenter
        loginDM = LoginDataManager(self)
    }
    
}//MAIN

extension LoginInteractor: LoginInteractorInputDelegate {
    
    func postLoginDosen(_ email: String, _ password: String) {
        loginDM?.postLoginDosen(email, password)
    }
    
    func postLoginMahasiswa(_ nim: String, _ password: String) {
        loginDM?.postLoginMahasiswa(nim, password)
    }
    
}//DELEGATE

extension LoginInteractor: LoginResponseProtocol {
    
    func successPostLoginDosen(_ response: ApiResponse<LoginModel>) {
        presenter?.successPostLoginDosen(response)
    }
    
    func failedPostLoginDosen(_ error: ApiError) {
        presenter?.failedPostLoginDosen(error)
    }
    
    func successPostLoginMahasiswa(_ response: ApiResponse<LoginModel>) {
        presenter?.successPostLoginMahasiswa(response)
    }
    
    func failedPostLoginMahasiswa(_ error: ApiError) {
        presenter?.failedPostLoginMahasiswa(error)
    }
    
}//DATA_MANAGER
