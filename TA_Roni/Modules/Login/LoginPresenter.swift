//
//  LoginPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class LoginPresenter {
    weak var view: LoginViewControllerDelegate?
    lazy var router: LoginRouterDelegate = LoginRouter(view: view)
    lazy var interactor: LoginInteractorInputDelegate = LoginInteractor(presenter: self)
    
    init(view: LoginViewControllerDelegate?) {
        self.view = view
    }
    
}//MAIN

extension LoginPresenter: LoginPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func postLoginDosen(_ email: String, _ password: String) {
        interactor.postLoginDosen(email, password)
    }
    
    func postLoginMahasiswa(_ nim: String, _ password: String) {
        interactor.postLoginMahasiswa(nim, password)
    }
    
    func pushToRegister() {
        router.pushToRegister()
    }
    
}//DELEGATE

extension LoginPresenter: LoginInteractorOutputDelegate {
    
    func successPostLoginDosen(_ response: ApiResponse<LoginModel>) {
        view?.showLoadView(false)
        debug(response.JSONResponse)
        view?.successLoginDosen()
    }
    
    func failedPostLoginDosen(_ error: ApiError) {
        view?.showLoadView(false)
        debug(error.json)
        router.presentFailedLoginAlert()
    }
    
    func successPostLoginMahasiswa(_ response: ApiResponse<LoginModel>) {
        view?.showLoadView(false)
        debug(response.JSONResponse)
        view?.successLoginMahasiswa()
    }
    
    func failedPostLoginMahasiswa(_ error: ApiError) {
        view?.showLoadView(false)
        debug(error.json)
        router.presentFailedLoginAlert()
    }
    
    
}//INTERACTOR
