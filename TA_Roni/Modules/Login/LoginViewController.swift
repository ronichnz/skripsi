//
//  LoginViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class LoginViewController: UIViewController {
    lazy var root = LoginView()
    lazy var presenter: LoginPresenterDelegate = LoginPresenter(view: self)
    
    var disposable = DisposeBag()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        root.updateLogo()
    }
    
    private func binding() { 
        bindViewPassBtn()
        bindDosenLoginBtn()
        bindMahasiswaLoginBtn()
        bindBackBtn()
        bindSignInBtn()
        bindForgotBtn()
    }
    
    private func setupViewController() {
        
    }
    
    private func bindViewPassBtn() {
        func viewPassBtnPressed() {
            if root.viewPassBtn.currentImage == UIImage(named: "ic_unsee") {
                root.passwordField.commonField.isSecureTextEntry = false
                root.viewPassBtn.setImage(#imageLiteral(resourceName: "ic_see"), for: .normal)
            } else {
                root.passwordField.commonField.isSecureTextEntry = true
                root.viewPassBtn.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
            }
        }
        root.viewPassBtn.rx.tap.subscribe(onNext: viewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindDosenLoginBtn() {
        func dosenLoginBtnPressed() {
            isDosen(true)
        }
        root.dosenBtn.rx.tap.subscribe(onNext: dosenLoginBtnPressed).disposed(by: disposable)
    }
    
    private func bindMahasiswaLoginBtn() {
        func mahasiswaBtnPressed() {
            isDosen(false)
        }
        root.mahasiswaBtn.rx.tap.subscribe(onNext: mahasiswaBtnPressed).disposed(by: disposable)
    }
    
    private func bindBackBtn() {
        func backBtnPressed() {
            root.emailField.value = ""
            root.nimField.value = ""
            root.passwordField.value = ""
            showLoginField(false)
        }
        root.backBtn.rx.tap.subscribe(onNext: backBtnPressed).disposed(by: disposable)
    }
    
    private func bindSignInBtn() {
        func signInBtnPressed() {
            if root.emailField.isHidden == true {
                checkMahasiswaField()
            } else {
                checkDosenField()
            } 
        }
        root.signInBtn.rx.tap.subscribe(onNext: signInBtnPressed).disposed(by: disposable)
    }
    
    private func bindForgotBtn() {
        func forgotBtnPressed() {
            self.presentAlert(title: "Coming Soon", message: "")
        }
        root.forgotBtn.rx.tap.subscribe(onNext: forgotBtnPressed).disposed(by: disposable)
    }
    
}

extension LoginViewController: LoginViewControllerDelegate {
    
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
    func successLoginDosen() {
        let tabBar = BaseRouter.createGenerateTabbar()
        DSource.window?.rootViewController = BaseRouter.createNavbar(tabBar)
        setTabbar() 
    }
    
    func successLoginMahasiswa() {
        let tabBar = BaseRouter.createScanTabbar()
        DSource.window?.rootViewController = BaseRouter.createNavbar(tabBar)
        setTabbar()
    }
    
}

extension LoginViewController { 
      
    private func pushToRegister() {
        presenter.pushToRegister()
    }
    
    private func showLoginField(_ show: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.root.firstView.isHidden = show
            self.root.secondView.isHidden = !show
            self.root.forgotLbl.isHidden = !show
            self.root.forgotBtn.isHidden = !show
            self.root.viewPassBtn.isHidden = !show
            self.root.signInBtn.isHidden = !show
        }
    }
    
    private func isDosen(_ sign: Bool) {
        showLoginField(true)
        root.nimField.isHidden = sign
        root.emailField.isHidden = !sign
    }
    
    private func setTabbar() {
        let transition = CATransition()
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromRight
        DSource.window?.rootViewController?.view.window!.layer.add(transition, forKey: kCATransition)
        
        DSource.window?.makeKeyAndVisible()
        DSource.navbar?.popToRootViewController(animated: true)
        DSource.tabbar.selectedIndex = 0
    }
    
    private func checkDosenField() {
        let email = root.emailField.value ?? ""
        let password = root.passwordField.value ?? ""
        
        if !String.empty(email) && !String.empty(password) && Validate.emailValidation(email) {
            showLoadView(true)
            presenter.postLoginDosen(email, password)
        } else {
            self.presentAlert(title: "Gagal", message: "Terjadi kesalahan dalam mengisi data")
        }
    }
    
    private func checkMahasiswaField() {
        let nim = root.nimField.value ?? ""
        let password = root.passwordField.value ?? ""
        
        if !String.empty(nim) && !String.empty(password) {
            showLoadView(true)
            presenter.postLoginMahasiswa(nim, password)
        } else {
            self.presentAlert(title: "Gagal", message: "Terjadi kesalahan dalam mengisi data")
        }
    }
}
	
