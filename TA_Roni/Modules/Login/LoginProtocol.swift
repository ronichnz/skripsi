//
//  LoginProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: AnyObject {
    
    func showLoadView(_ show: Bool)
    func successLoginDosen()
    func successLoginMahasiswa()
     
}//VIEW_CONTROLLER

protocol LoginPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func postLoginDosen(_ email: String, _ password: String)
    func postLoginMahasiswa(_ nim: String, _ password: String)
    
    func pushToRegister()
    
}//PRESENTER

protocol LoginRouterDelegate: AnyObject {
    
    func pushToRegister()
     
    func presentFailedLoginAlert()
    
}//ROUTER

protocol LoginInteractorInputDelegate: AnyObject {
    
    func postLoginDosen(_ email: String, _ password: String)
    func postLoginMahasiswa(_ nim: String, _ password: String)
    
}//INTERACTOR_INPUT

protocol LoginInteractorOutputDelegate: AnyObject {
    
    func successPostLoginDosen(_ response: ApiResponse<LoginModel>)
    func failedPostLoginDosen(_ error: ApiError)
    
    func successPostLoginMahasiswa(_ response: ApiResponse<LoginModel>)
    func failedPostLoginMahasiswa(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
