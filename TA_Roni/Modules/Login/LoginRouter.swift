//
//  LoginRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class LoginRouter: LoginRouterDelegate {
    
    weak var source: UIViewController?
    
    init(view: LoginViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToRegister() {
        let registerVc = RegisterViewController()
        registerVc.modalPresentationStyle = .custom
        registerVc.modalTransitionStyle = .coverVertical
        source?.present(registerVc, animated: true, completion: nil)
    } 
    
    func presentFailedLoginAlert() {
        source?.presentAlert(title: "Gagal", message: "Terjadi kesalahan saat masuk \nSilahkan coba kembali")
    }
}
