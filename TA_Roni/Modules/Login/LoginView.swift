//
//  LoginView.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit 

final class LoginView: UIView {
    
    let backgroundImg = UIImageView()
    
    let scrollView = UIScrollView()
    let contentsView = UIView()
    
    let logoImg = UIImageView()
    
    let spaceView = UIView()
    
    let mainStackView = UIStackView()
    
    let firstView = UIView()
    let firstLbl = UILabel()
    let dosenBtn = UIButton()
    let mahasiswaBtn = UIButton()
    
    let secondView = UIView()
    let stackView = UIStackView()
    let emailField = CommonField()
    let nimField = CommonField()
    let passwordField = CommonField()
    let viewPassBtn = UIButton()
    let forgotLbl = UILabel()
    let forgotBtn = UIButton()
    let backBtn = UIButton()
    let signInBtn = UIButton()
    
    let loadView = LoadingView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .primary
        
        firstView.sv([firstLbl, dosenBtn, mahasiswaBtn])
        stackView.setupArrangedSubview([emailField, nimField, passwordField])
        secondView.sv([stackView, forgotLbl, forgotBtn, backBtn, signInBtn])
        mainStackView.setupArrangedSubview([firstView, secondView])
        contentsView.sv([logoImg, spaceView, mainStackView, loadView, viewPassBtn])
        scrollView.sv([contentsView])
        sv([backgroundImg, scrollView])
        
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        firstLbl.text = "Anda ingin login sebagai?"
        dosenBtn.setTitle("Login sebagai Dosen", for: .normal)
        mahasiswaBtn.setTitle("Login sebagai Mahasiswa", for: .normal)
        
        emailField.initialize(label: "Email", placeholder: "Masukkan email anda", keyboardType: .emailAddress)
        nimField.initialize(label: "NIM", placeholder: "Masukkan NIM anda", keyboardType: .numberPad)
        passwordField.initialize(label: "Password", placeholder: "Masukkan password anda", isSecure: true)
        
        forgotLbl.text = "Lupa Password?"
        forgotBtn.setTitle("Klik disini", for: .normal)
        
        backBtn.setTitle("Kembali", for: .normal)
        signInBtn.setTitle("Masuk", for: .normal)
    }
    
    private func setupView() {
        backgroundImg.style {
            $0.setImage(#imageLiteral(resourceName: "ic_background"))
        }
        
        logoImg.style {
            $0.setImage(#imageLiteral(resourceName: "ic_logo"))
        }
        
        [firstView, secondView].style {
            $0.backgroundColor = .white
            $0.setLayer(cornerRadius: 10)
            $0.setShadow(opacity: 0.2)
        }
        
        viewPassBtn.style {
            $0.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
            $0.isHidden = true
        }
        
        firstLbl.style {
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f18)
            $0.numberOfLines = 0
            $0.textAlignment = .center
        }
        
        [dosenBtn, mahasiswaBtn].style {
            $0.setLayer(cornerRadius: $0.frame.height / 2)
            $0.setTitleColor(.white, for: .normal)
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f16)
        }
        
        dosenBtn.style {
            $0.backgroundColor = .primary
        }
        
        mahasiswaBtn.style {
            $0.backgroundColor = .secondary
        }
        
        stackView.style {
            $0.axis = .vertical
            $0.spacing = Margin.s12
        }
        
        forgotLbl.style {
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f14)
        }
        
        forgotBtn.style {
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f14)
            $0.setTitleColor(.primary, for: .normal)
        }
        
        [backBtn, signInBtn].style {
            $0.setLayer(cornerRadius: 8)
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f16)
            $0.titleEdgeInsets = UIEdgeInsets(horizontal: 12, vertical: 8)
            $0.setShadow(radius: 2, opacity: 0.4)
        }
        
        backBtn.style {
            $0.backgroundColor = .silverGray
            $0.setTitleColor(.darkGray, for: .normal)
        }
        
        signInBtn.style {
            $0.backgroundColor = .primary
        }
        
        mainStackView.style {
            $0.axis = .horizontal
            $0.spacing = Margin.s12
            $0.alignment = .center
        }
        
        firstView.isHidden = false
        secondView.isHidden = true
    }
    
    func updateLogo() {
        UIView.animate(withDuration: 1) {
            self.logoImg.frame.origin.y = Margin.s4.autoSize
            self.mainStackView.frame.origin.x = 16
            self.mainStackView.snp.makeConstraints { (make) in
                make.leading.trailing.bottom.equalTo(self.spaceView)
             }
        }
    }
    
    private func setupConstraint() {
        backgroundImg.snp.makeConstraints { (make) in
            make.edges.height.equalTo(self)
        }
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.height.equalTo(self)
        }
        
        contentsView.snp.makeConstraints { (make) in
            make.edges.width.equalTo(scrollView)
        }
        
        logoImg.snp.makeConstraints { (make) in
            make.top.equalTo(contentsView).offset(Margin.i120.autoSize)
            make.centerX.equalTo(contentsView)
//            make.size.equalTo(Margin.i90.autoSize * 2)
            make.width.equalTo(250)
            make.height.equalTo(230)
        }
        
        spaceView.snp.makeConstraints { (make) in
            make.top.equalTo(logoImg.snp.bottom)
            make.leading.equalTo(contentsView).offset(Margin.s16)
            make.trailing.bottom.equalTo(contentsView).offset(-Margin.s16)
            make.height.equalTo(mainScreen.height / 2.3)
        }
        
        mainStackView.snp.makeConstraints { (make) in
            make.leading.equalTo(contentsView.snp.trailing).offset(Margin.s8)
            make.bottom.equalTo(spaceView)
        }
        
        firstLbl.snp.makeConstraints { (make) in
            make.top.equalTo(firstView).offset(Margin.s12)
            make.leading.equalTo(firstView).offset(Margin.s32)
            make.trailing.equalTo(firstView).offset(-Margin.s32)
        }
        
        dosenBtn.snp.makeConstraints { (make) in
            make.top.equalTo(firstLbl.snp.bottom).offset(Margin.s16)
            make.leading.trailing.equalTo(firstLbl)
            make.height.equalTo(Margin.s44.autoSize)
        }
        
        mahasiswaBtn.snp.makeConstraints { (make) in
            make.top.equalTo(dosenBtn.snp.bottom).offset(Margin.s12)
            make.leading.trailing.height.equalTo(dosenBtn)
            make.bottom.equalTo(firstView).offset(-Margin.s12)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.leading.equalTo(secondView).offset(Margin.s12)
            make.trailing.equalTo(secondView).offset(-Margin.s12)
        }
        
        viewPassBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(passwordField.commonField)
            make.trailing.equalTo(passwordField).offset(-Margin.s12)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        forgotBtn.snp.makeConstraints { (make) in
            make.top.equalTo(stackView.snp.bottom).offset(Margin.s8)
            make.trailing.equalTo(stackView)
        }
        
        forgotLbl.snp.makeConstraints { (make) in
            make.trailing.equalTo(forgotBtn.snp.leading).offset(-Margin.s4)
            make.centerY.equalTo(forgotBtn)
        }
        
        signInBtn.snp.makeConstraints { (make) in
            make.top.equalTo(forgotLbl.snp.bottom).offset(Margin.s24)
            make.trailing.equalTo(stackView)
            make.bottom.equalTo(secondView).offset(-Margin.s12)
            make.width.equalTo(mainScreen.width / 3)
            make.height.equalTo(Margin.s32.autoSize)
        }
        
        backBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(signInBtn)
            make.leading.equalTo(stackView)
            make.width.equalTo(mainScreen.width / 3)
            make.height.equalTo(Margin.s32.autoSize)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
