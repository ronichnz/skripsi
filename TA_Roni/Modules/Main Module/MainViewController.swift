//
//  MainViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 22/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift

class MainViewController: UIViewController {
    lazy var root = MainView()
    lazy var presenter: MainPresenterDelegate = MainPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var loading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
    } 
    
    private func setupViewController() { 
    }
}

extension MainViewController: MainViewControllerDelegate {
    
}
