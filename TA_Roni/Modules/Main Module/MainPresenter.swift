//
//  MainPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 22/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class MainPresenter {
    weak var view: MainViewControllerDelegate?
    lazy var router: MainRouterDelegate = MainRouter(view: view)
    lazy var interactor: MainInteractorInputDelegate = MainInteractor(presenter: self)
    
    init(view: MainViewControllerDelegate?) {
        self.view = view
    }
    
}//MAIN

extension MainPresenter: MainPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
}//DELEGATE

extension MainPresenter: MainInteractorOutputDelegate {
    
}//INTERACTOR
