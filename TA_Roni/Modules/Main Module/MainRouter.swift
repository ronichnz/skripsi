//
//  MainRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 22/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class MainRouter: MainRouterDelegate {
    weak var source: UIViewController?
    
    init(view: MainViewControllerDelegate?) {
        source = view as? UIViewController
    }
}
