//
//  MainView.swift
//  TA_Roni
//
//  Created by Cumaroni on 22/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class MainView: UIView {
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .background
          
        setupConstraint()
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        
    }
    
    private func setupView() {
        
    }
    
    private func setupConstraint() {
        
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
