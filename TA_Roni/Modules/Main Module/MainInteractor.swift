
//
//  MainInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 22/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import Foundation

final class MainInteractor {
    weak var presenter: MainInteractorOutputDelegate?
    
    init(presenter: MainInteractorOutputDelegate?) {
        self.presenter = presenter
    }
    
}//MAIN

extension MainInteractor: MainInteractorInputDelegate {
    
}//DELEGATE


extension MainInteractor: MainInteractorOutputDelegate {
    
}//DATA_MANAGER
