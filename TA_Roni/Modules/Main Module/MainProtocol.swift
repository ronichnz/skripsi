//
//  MainProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 22/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol MainViewControllerDelegate: AnyObject {
    
}//VIEW_CONTROLLER

protocol MainPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
}//PRESENTER

protocol MainRouterDelegate: AnyObject {
    
}//ROUTER

protocol MainInteractorInputDelegate: AnyObject {
    
}//INTERACTOR_INPUT

protocol MainInteractorOutputDelegate: AnyObject {
    
}//INTERACTOR_OUTPUT
