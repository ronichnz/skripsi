//
//  QRScannerInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//
 
import Foundation

final class QRScannerInteractor {
    weak var presenter: QRScannerInteractorOutputDelegate?
    
    private var scanDM: ScanQRDMRequestProtocol?
    
    init(presenter: QRScannerInteractorOutputDelegate?) {
        self.presenter = presenter
        scanDM = ScanQRDataManager(self)
    }
    
}//MAIN

extension QRScannerInteractor: QRScannerInteractorInputDelegate {
    
    func postScanQR(_ data: ScanQRSt) {
        scanDM?.postScanQR(data)
    }
    
}//DELEGATE


extension QRScannerInteractor: ScanQRResponseProtocol {
    
    func successPostScanQR(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPostScanQR(response)
    }
    
    func failedPostScanQR(_ error: ApiError) {
        presenter?.failedPostScanQR(error)
    }
    
}//DATA_MANAGER
