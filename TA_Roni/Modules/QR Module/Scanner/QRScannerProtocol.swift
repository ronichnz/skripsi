//
//  QRScannerProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

protocol QRScannerViewControllerDelegate: AnyObject {
    
    func scanSuccess(_ message: String)
    func scanFailed(_ message: String)
    
}//VIEW_CONTROLLER

protocol QRScannerPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func postScanQR(_ data: ScanQRSt)
    
}//PRESENTER

protocol QRScannerRouterDelegate: AnyObject { 
    
}//ROUTER

protocol QRScannerInteractorInputDelegate: AnyObject {
    
    func postScanQR(_ data: ScanQRSt)
    
}//INTERACTOR_INPUT

protocol QRScannerInteractorOutputDelegate: AnyObject {
    
    func successPostScanQR(_ response: ApiResponse<EmptyModel>)
    func failedPostScanQR(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
