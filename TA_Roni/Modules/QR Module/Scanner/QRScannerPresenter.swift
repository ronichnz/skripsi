//
//  QRScannerPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class QRScannerPresenter {
    weak var view: QRScannerViewControllerDelegate?
    lazy var router: QRScannerRouterDelegate = QRScannerRouter(view: view)
    lazy var interactor: QRScannerInteractorInputDelegate = QRScannerInteractor(presenter: self)
    
    init(view: QRScannerViewControllerDelegate?) {
        self.view = view
    }
    
}//MAIN

extension QRScannerPresenter: QRScannerPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func postScanQR(_ data: ScanQRSt) {
        interactor.postScanQR(data)
    }
    
}//DELEGATE

extension QRScannerPresenter: QRScannerInteractorOutputDelegate {
    
    func successPostScanQR(_ response: ApiResponse<EmptyModel>) {
        view?.scanSuccess(response.JSONResponse["message"].stringValue)
    }
    
    func failedPostScanQR(_ error: ApiError) {
        view?.scanFailed(error.messages.joined())
    }
    
}//INTERACTOR
