//
//  QRScannerRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class QRScannerRouter: QRScannerRouterDelegate {
    weak var source: UIViewController?
    
    init(view: QRScannerViewControllerDelegate?) {
        source = view as? UIViewController
    } 
     
}
