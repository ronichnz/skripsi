//
//  QRScannerViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import AVFoundation
import RNCryptor

class QRScannerViewController: UIViewController {
    
    lazy var root = QRScannerView()
    lazy var presenter: QRScannerPresenterDelegate = QRScannerPresenter(view: self)
    
    var disposable = DisposeBag()
     
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var timer = Timer()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        setupScanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
         
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        
        timer.invalidate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated) 
        setupScannerFrame()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setCurrentDate), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    private func setupViewController() {
        captureSession = AVCaptureSession()
    }
    
    private func setupScannerFrame() {
        previewLayer.frame = root.scannerView.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        root.scannerView.layer.addSublayer(previewLayer)
        captureSession.startRunning()
    }
    
    private func setupScanner() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        
        let videoInput: AVCaptureDeviceInput
        do { videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice) } catch { return }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            presentScanFailed("Your device does not support scanning a code from an item. Please use a device with a camera.")
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            presentScanFailed("Your device does not support scanning a code from an item. Please use a device with a camera.")
            return
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    }
    
    func decryptMessage(encryptedMessage: String, encryptionKey: String) throws -> String {
        let encryptedData = Data.init(base64Encoded: encryptedMessage)!
        let decryptedData = try RNCryptor.decrypt(data: encryptedData, withPassword: encryptionKey)
        let decryptedString = String(data: decryptedData, encoding: .utf8)!
        return decryptedString
    }
}

extension QRScannerViewController: QRScannerViewControllerDelegate {
    
    func scanSuccess(_ message: String) {
        self.presentAlert(title: "Scan Berhasil", message: message)
        root.timeLbl.textColor = .systemGreen
    }
    
    func scanFailed(_ message: String) {
        root.timeLbl.textColor = .systemRed
        presentScanFailed(message)
    }
    
}

extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            if metadataObject.type == AVMetadataObject.ObjectType.qr && !String.empty(stringValue) {
                debug(stringValue)
                do {
                    let decrypted = try decryptMessage(encryptedMessage: stringValue, encryptionKey: "STMIK_TIME_QR_CODE")
                    debug(decrypted)
                    guard let convertedJSON = decrypted.convertToDictionary() else { return }
                    guard let qrID = convertedJSON["id_qr"] as? String else { return }
                    guard let pertemuanAt = convertedJSON["pertemuan_at"] as? String else { return }
                    guard let matkulID = convertedJSON["id_matkul"] as? String else { return }
                    guard let dosenID = convertedJSON["id_dosen"] as? String else { return }
                    guard let date = convertedJSON["created_at"] as? String else { return }
                    
                    let currentDate = Date()
                    let dateFormatter = DateFormatter()
                    let timeFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    timeFormatter.dateFormat = "HH:mm"
                    
                    let today = dateFormatter.string(from: currentDate)
                    let time = timeFormatter.string(from: currentDate)
                    
                    if today != date {
                        presentScanFailed("Kode QR yang anda scan sudah kadaluarsa")
                        return
                    }
                    
                    if String.empty(qrID) && String.empty(matkulID) && String.empty(dosenID) {
                        presentScanFailed("Kode QR yang anda scan bukan digenerate oleh Dosen STMIK TIME")
                        return
                    }
                    
                    let data = ScanQRSt(
                        mahasiswaId: Account.getMahasiswaID,
                        matkulId: matkulID,
                        qrId: qrID,
                        pertemuanAt: pertemuanAt,
                        date: today,
                        time: time
                    )
                    presenter.postScanQR(data)
                    captureSession.stopRunning()
                } catch {
                    debug(error)
                }
            } else {
                presentScanFailed("Kode QR yang anda scan bukan digenerate oleh Dosen STMIK TIME")
            }
        } else {
            presentScanFailed("Kode QR yang anda scan bukan digenerate oleh Dosen STMIK TIME")
        }
    }
    
}

extension QRScannerViewController {
    
    @objc private func setCurrentDate() {
        //current date
        let curentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "ID")
        let todayDate = dateFormatter.string(from: curentDate)
        root.dateLbl.text = todayDate
        
        //today
        let dayDate = Date()
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat = "EEEE"
        dayFormatter.locale = Locale(identifier: "ID")
        let today = dayFormatter.string(from: dayDate)
        root.dayLbl.text = today
        
        //time
        let timeDate = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        let time = timeFormatter.string(from: timeDate)
        root.timeLbl.text = time
    }
    
    private func presentScanFailed(_ message: String) {
        root.timeLbl.textColor = .systemRed
        
        if (captureSession?.isRunning == true) { 
            captureSession.stopRunning()
        }
        
        self.presentAlert(title: "Scan Gagal", message: message, alertAction: [UIAlertAction(title: "Tutup", style: .default, handler: { _ in
            self.captureSession.startRunning()
        })])
    }
     
}
