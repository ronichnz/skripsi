//
//  QRScannerView.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

class QRScannerView: UIView {
    
    let scannerView = UIView()    
    let scannerLbl = UILabel()
    
    let dayLbl = UILabel()
    let dateLbl = UILabel()
    let timeLbl = UILabel()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .background
         
        sv([scannerView, scannerLbl, dayLbl, dateLbl, timeLbl])
        setupConstraints()
        setupViews()
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("Error")
    }
        
    func setupViews() {
        scannerView.style {
            $0.setLayer(cornerRadius: 1)
            $0.backgroundColor = .black
            $0.clipsToBounds = true
        }
        
        scannerLbl.style {
            $0.textColor = .darkGray
            $0.text = "Scan QR Code yang sudah di generate oleh dosen yang sedang mengajar"
            $0.font = UIFont.systemFont(ofSize: Margin.f14, weight: .regular)
            $0.numberOfLines = 0
            $0.textAlignment = .center
        }
        
        dayLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f25, weight: .medium)
            $0.textAlignment = .center
            $0.textColor = .darkGray
        }
        
        dateLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f30, weight: .medium)
            $0.textAlignment = .center
            $0.textColor = .darkGray
        }
        
        timeLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f60, weight: .semibold)
            $0.textAlignment = .center
            $0.textColor = .darkGray
        } 
        
    }
        
    func setupConstraints() {
        scannerView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
            make.height.equalTo(mainScreen.height / 2)
        }
        
        scannerLbl.snp.makeConstraints { (make) in
            make.top.equalTo(scannerView.snp.bottom).offset(Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        }
        
        dayLbl.snp.makeConstraints { (make) in
            make.top.equalTo(scannerLbl.snp.bottom).offset(Margin.s32)
            make.leading.trailing.equalTo(scannerLbl)
        }
        
        dateLbl.snp.makeConstraints { (make) in
            make.top.equalTo(dayLbl.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(dayLbl)
        }
        
        timeLbl.snp.makeConstraints { (make) in
            make.top.equalTo(dateLbl.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(dateLbl)
        }
            
        setNeedsLayout()
        layoutIfNeeded()
    }
}

