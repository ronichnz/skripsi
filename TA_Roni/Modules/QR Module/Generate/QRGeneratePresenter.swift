	//
//  QRGeneratePresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class QRGeneratePresenter {
    weak var view: QRGenerateViewControllerDelegate?
    lazy var router: QRGenerateRouterDelegate = QRGenerateRouter(view: view)
    lazy var interactor: QRGenerateInteractorInputDelegate = QRGenerateInteractor(presenter: self)
    
    init(view: QRGenerateViewControllerDelegate?) {
        self.view = view
    }
    
}//MAIN

extension QRGeneratePresenter: QRGeneratePresenterDelegate {
    
    func viewDidLoad() {
        interactor.getMatkulDosen()
    }
    
    func postGenerateQR(_ data: CreateQRSt) {
        interactor.postGenerateQR(data)
    }
    
}//DELEGATE

extension QRGeneratePresenter: QRGenerateInteractorOutputDelegate {
    
    func successGetMatkulDosen(_ response: ApiResponse<MatKulModel>) {
        debug(response.JSONResponse)
        view?.setData(response.data)
    }
    
    func failedGetMatkulDosen(_ error: ApiError) {
        debug(error)
    }
    
    func successPostGenerateQR(_ response: ApiResponse<QRModel>) {
        let result = response.JSONResponse["data"].stringValue
        let decodedData = Data(base64Encoded: result)!
        let decodedString = String(data: decodedData, encoding: .utf8)!
        view?.qrCreated(decodedString)
    }
    
    func failedPostGenerateQR(_ error: ApiError) {
        view?.qrFailed()
        debug("failed generate")
    }
    
}//INTERACTOR
