
//
//  QRGenerateViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit
import RxRelay
import RxSwift
    import RNCryptor

class QRGenerateViewController: UIViewController {
    
    lazy var root = QRGenerateView()
    lazy var presenter: QRGeneratePresenterDelegate = QRGeneratePresenter(view: self)
    
    var disposable = DisposeBag()
    
    var matkulData = BehaviorRelay<[MatKulModel]>(value: [])
    var searchData: [MatKulModel] = []
    
    var namaMatkul: String = ""
    var matkulID: String = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        binding()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewController() 
    }
    
    private func binding() {
        bindMatKulTbl()
        bindSearchField()
        bindPertemuanField()
        bindAboutField()
        bindQRBtn()
        bindDismissBtn()
        bindBackBtn()
        bindGenerateBtn()
    }
    
    private func setupViewController() {
        
    }
    
    private func bindMatKulTbl() {
        matkulData.bind(to: root.searchTbl.rx.items(cellIdentifier: "cell", cellType: QRGenerateCell.self)) { index, model, cell in
            cell.categoryLbl.text = model.namaMatkul
        }.disposed(by: disposable)
        
        root.searchTbl.rx.modelSelected(MatKulModel.self).subscribe(onNext: { (model) in
            self.showPertemuanView(true)
            self.root.matkulLbl.text = model.namaMatkul
            self.namaMatkul = model.namaMatkul
            self.matkulID = model.id
        }).disposed(by: disposable)
    }
    
    private func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
    private func bindQRBtn() {
        func generateBtnPressed() {
            showMatkulView(true)
        }
        root.qrBtn.rx.tap.subscribe(onNext: generateBtnPressed).disposed(by: disposable)
    }
    
    private func bindDismissBtn() {
        func dismissBtnPressed() {
            showPertemuanView(false)
        }
        root.dismissBtn.rx.tap.subscribe(onNext: dismissBtnPressed).disposed(by: disposable)
    }
    
    private func bindSearchField() {
        func searchEditingChanged() {
            if String.empty(root.searchField.text) {
                matkulData.accept(searchData)
            } else {
                matkulData.accept(searchData.filter({
                    $0.namaMatkul.localizedCaseInsensitiveContains(root.searchField.text ?? "")
                }))
            }
            root.searchTbl.reloadData()
        }
        root.searchField.rx.controlEvent(.editingChanged).subscribe(onNext: searchEditingChanged).disposed(by: disposable)
    }
    
    private func bindPertemuanField() {
        root.pertemuanField.commonField.rx.value.changed.subscribe(onNext: { value in
            self.root.generateBtn.isHidden = (String.empty(value) || String.empty(self.root.aboutField.value))
        }).disposed(by: disposable)
    }
    
    private func bindAboutField() {
        root.aboutField.commonField.rx.value.changed.subscribe(onNext: { value in
            self.root.generateBtn.isHidden = (String.empty(value) || String.empty(self.root.aboutField.value))
        }).disposed(by: disposable)
    }
    
    private func bindBackBtn() {
        func backBtnPressed() {
            showMatkulView(true)
        }
        root.backBtn.rx.tap.subscribe(onNext: backBtnPressed).disposed(by: disposable)
    }
    
    private func bindGenerateBtn() {
        func generateBtnPressed() {
            if String.empty(root.pertemuanField.value) && String.empty(root.aboutField.value) {
                self.presentAlert(title: "Error", message: "Tidak boleh ada kolom yang kosong")
                return
            }
            showPertemuanView(false)
            showLoadView(true)
            generateQR()
        }
        root.generateBtn.rx.tap.subscribe(onNext: generateBtnPressed).disposed(by: disposable)
    }
    
    private func generateQR() {
        let data = CreateQRSt(
            matkulId: matkulID,
            dosenId: "\(Account.getDosenID)",
            pertemuanAt: root.pertemuanField.value ?? "",
            about: root.aboutField.value ?? "",
            date: self.getCurrentDate()
        )
        self.presenter.postGenerateQR(data)
    }
    
    private func showMatkulView(_ show: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.root.dismissBtn.isHidden = false
            self.root.pertemuanField.value = ""
            self.root.aboutField.value = ""
            self.root.matkulView.isHidden = !show
            self.root.pertemuanView.isHidden = true
        }
    }
    
    private func showPertemuanView(_ show: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.root.dismissBtn.isHidden = !show
            self.root.matkulView.isHidden = true
            self.root.pertemuanView.isHidden = !show
        }
    }
    
    private func encryptMessage(message: String, encryptionKey: String) throws -> String {
        let messageData = message.data(using: .utf8)!
        let cipherData = RNCryptor.encrypt(data: messageData, withPassword: encryptionKey)
        return cipherData.base64EncodedString()
    }
}

extension QRGenerateViewController: QRGenerateViewControllerDelegate {
    
    func setData(_ data: [MatKulModel]) {
        self.matkulData.accept(data)
        self.searchData = data
    }
    
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
        root.qrBtn.setTitle("", for: .normal)
    }
    
    func qrFailed() {
        showLoadView(false)
        root.qrImg.setImage(#imageLiteral(resourceName: "ic_reload"), size: Margin.i50.autoSize)
    }
    
    func qrCreated(_ data: String) {
        showLoadView(false)
        self.root.qrLbl.text = namaMatkul
        guard let timeCIImage = CIImage(image: #imageLiteral(resourceName: "ic_stmiktime").resize(size: CGSize(Margin.i35.autoSize))) else { return }
        do {
            guard let combined = try encryptMessage(message: data, encryptionKey: "STMIK_TIME_QR_CODE").generateQRCode().combined(with: timeCIImage) else { return }
            let result = UIImage(ciImage: combined)
            root.qrImg.image = result
        } catch {
            debug(error)
        }
        
    }
    
}
