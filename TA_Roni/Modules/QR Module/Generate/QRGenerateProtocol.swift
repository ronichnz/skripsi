//
//  QRGenerateProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

protocol QRGenerateViewControllerDelegate: AnyObject {
    
    func setData(_ data: [MatKulModel])
    
    func qrFailed()
    func qrCreated(_ data: String)
    
}//VIEW_CONTROLLER

protocol QRGeneratePresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func postGenerateQR(_ data: CreateQRSt)
    
}//PRESENTER

protocol QRGenerateRouterDelegate: AnyObject {
    
}//ROUTER

protocol QRGenerateInteractorInputDelegate: AnyObject {
    
    func getMatkulDosen()
    func postGenerateQR(_ data: CreateQRSt)
    
}//INTERACTOR_INPUT

protocol QRGenerateInteractorOutputDelegate: AnyObject {
    
    func successGetMatkulDosen(_ response: ApiResponse<MatKulModel>)
    func failedGetMatkulDosen(_ error: ApiError)
    
    func successPostGenerateQR(_ response: ApiResponse<QRModel>)
    func failedPostGenerateQR(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
