//
//  QRGenerateInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class QRGenerateInteractor {
    weak var presenter: QRGenerateInteractorOutputDelegate?
    
    private var matkulDosenDM: MatkulDosenDMRequestProtocol?
    private var generateQRDM: GenerateQRDMRequestProtocol?
    
    init(presenter: QRGenerateInteractorOutputDelegate?) {
        self.presenter = presenter
        matkulDosenDM = MatkulDosenDataManager(self)
        generateQRDM = GenerateQRDataManager(self)
    }
    
}//MAIN

extension QRGenerateInteractor: QRGenerateInteractorInputDelegate {
    
    func getMatkulDosen() {
        matkulDosenDM?.getMatkulDosen("\(Account.getDosenID)")
    }
    
    func postGenerateQR(_ data: CreateQRSt) {
        generateQRDM?.postGenerateQR(data)
    }
    
}//DELEGATE

extension QRGenerateInteractor: MatkulDosenResponseProtocol {
    
    func successGetMatkulDosen(_ response: ApiResponse<MatKulModel>) {
        presenter?.successGetMatkulDosen(response)
    }
    
    func failedGetMatkulDosen(_ error: ApiError) {
        presenter?.failedGetMatkulDosen(error)
    }
    
}//DATA_MANAGER

extension QRGenerateInteractor: GenerateQRResponseProtocol {
    
    func successPostGenerateQR(_ response: ApiResponse<QRModel>) {
        presenter?.successPostGenerateQR(response)
    }
    
    func failedPostGenerateQR(_ error: ApiError) {
        presenter?.failedPostGenerateQR(error)
    }
    
}//DATA_MANAGER
