//
//  QRGenerateView.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

class QRGenerateView: UIView {
     
    let shadeView = UIView()
    let qrLbl = UILabel()
    let qrView = UIView()
    let qrImg = UIImageView()
    let qrBtn = UIButton()
    
    let dismissBtn = UIButton()
    let matkulView = UIView()
    let searchImg = UIImageView()
    let searchField = UITextField()
    let lineView = UIView()
    let searchTbl = UITableView()
    
    let pertemuanView = UIView()
    let matkulLbl = UILabel()
    let pertemuanLineView = UIView()
    let pertemuanField = CommonField()
    let aboutField = CommonField()
    let backBtn = UIButton()
    let generateBtn = UIButton()
    
    let loadView = LoadingView()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .primary
        
        matkulView.sv([searchImg, searchField, lineView, searchTbl])
        pertemuanView.sv([matkulLbl, pertemuanLineView, pertemuanField, aboutField, backBtn, generateBtn])
        qrView.sv([qrImg])
        sv([shadeView, qrLbl, qrView, qrBtn, dismissBtn, matkulView, pertemuanView, loadView])
        
        setupConstraints()
        setupViews()
        setupText()
    } 
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("Error")
    }
    
    func setupText() {
        qrBtn.setTitle("Generate", for: .normal)
        searchField.placeholder = "Cari..."
        pertemuanField.initialize(label: "Pertemuan Ke", placeholder: "Ex: 1", keyboardType: .numberPad)
        aboutField.initialize(label: "Tentang", placeholder: "Hari ini tentang?")
        backBtn.setTitle("Kembali", for: .normal)
        generateBtn.setTitle("Generate", for: .normal)
    }
        
    func setupViews() {
        qrView.style {
            $0.setLayer(cornerRadius: 10)
            $0.backgroundColor = .white
        }
        
        qrLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f30, weight: .heavy)
            $0.numberOfLines = 0
            $0.textAlignment = .center
            $0.textColor = .white
        }
        
        qrBtn.style {
            $0.titleLabel?.font = UIFont.systemFont(ofSize: Margin.f40, weight: .heavy)
            $0.setTitleColor(.darkGray, for: .normal)
        }   
        
        shadeView.style {
            $0.backgroundColor = .black
            $0.alpha = 0.6
        }
        
        [matkulView, pertemuanView].style {
            $0.backgroundColor = .white
            $0.setLayer(cornerRadius: 4)
        }
        
        searchImg.style {
            $0.image = #imageLiteral(resourceName: "ic_search").transparent
            $0.tintColor = .lightGray
        }
        
        searchField.style {
            $0.autocorrectionType = .no
            $0.returnKeyType = .search
            $0.enablesReturnKeyAutomatically = true
            $0.textColor = .darkGray
        }
        
        [lineView, pertemuanLineView].style {
            $0.setAsLine(height: 0.5)
            $0.backgroundColor = .silverGray
        }
        
        searchTbl.style {
            $0.backgroundColor = .white
            $0.bounces = false
            $0.flexibleHeight()
            $0.register(QRGenerateCell.self, forCellReuseIdentifier: "cell")
            $0.showsVerticalScrollIndicator = true
            $0.separatorStyle = .none
            $0.setLayer(cornerRadius: 10)
            $0.tableFooterView = UIView()
        }
        
        matkulLbl.style {
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f20)
            $0.textAlignment = .center
            $0.textColor = .darkGray
            $0.numberOfLines = 0
        }
        
        [backBtn, generateBtn].style {
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f18)
            $0.setTitleColor(.primary, for: .normal)
        }
        
        dismissBtn.style {
            $0.backgroundColor = .black
            $0.alpha = 0.6
        }
        
        [dismissBtn, matkulView, pertemuanView, generateBtn].style {
            $0.isHidden = true
        }
    }
        
    func setupConstraints() {
        shadeView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        qrLbl.snp.makeConstraints { (make) in
            make.bottom.equalTo(qrView.snp.top).offset(-Margin.s16)
            make.leading.equalTo(self).offset(Margin.s24)
            make.trailing.equalTo(self).offset(-Margin.s24)
        }
        
        qrView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
        
        qrImg.snp.makeConstraints { (make) in
            make.top.leading.equalTo(qrView).offset(Margin.s12)
            make.trailing.bottom.equalTo(qrView).offset(-Margin.s12)
            make.width.equalTo(mainScreen.width / 1.5)
            make.height.equalTo(mainScreen.width / 1.4)
        }
            
        qrBtn.snp.makeConstraints { (make) in
            make.edges.equalTo(qrView)
        }
        
        matkulView.snp.makeConstraints { (make) in
            make.height.equalTo(mainScreen.height / 1.4)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
            make.center.equalTo(self)
        }
        
        searchImg.snp.makeConstraints { (make) in
            make.centerY.equalTo(searchField)
            make.leading.equalTo(matkulView).offset(Margin.s12)
            make.size.equalTo(Margin.i20.autoSize)
        }
        
        searchField.snp.makeConstraints { (make) in
            make.top.equalTo(matkulView).offset(Margin.s8)
            make.leading.equalTo(searchImg.snp.trailing).offset(Margin.s12)
            make.trailing.equalTo(matkulView).offset(-Margin.s12)
            make.height.equalTo(Margin.s40.autoSize)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.top.equalTo(searchField.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(matkulView)
        }
        
        searchTbl.snp.makeConstraints { (make) in
            make.top.equalTo(lineView.snp.bottom).offset(Margin.s8)
            make.leading.equalTo(searchImg)
            make.trailing.equalTo(searchField)
            make.bottom.equalTo(matkulView)
        }
        
        pertemuanView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        }
        
        matkulLbl.snp.makeConstraints { (make) in
            make.top.equalTo(pertemuanView).offset(Margin.s8)
            make.leading.equalTo(pertemuanView).offset(Margin.s16)
            make.trailing.equalTo(pertemuanView).offset(-Margin.s16)
        }
        
        pertemuanLineView.snp.makeConstraints { (make) in
            make.top.equalTo(matkulLbl.snp.bottom).offset(Margin.s12)
            make.leading.trailing.equalTo(pertemuanView)
        }
        
        pertemuanField.snp.makeConstraints { (make) in
            make.top.equalTo(pertemuanLineView.snp.bottom).offset(Margin.s16)
            make.leading.trailing.equalTo(matkulLbl)
        }
        
        aboutField.snp.makeConstraints { (make) in
            make.top.equalTo(pertemuanField.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(matkulLbl)
        }
        
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(aboutField.snp.bottom).offset(Margin.s12)
            make.leading.equalTo(aboutField)
        }
        
        generateBtn.snp.makeConstraints { (make) in
            make.top.equalTo(aboutField.snp.bottom).offset(Margin.s12)
            make.trailing.equalTo(aboutField)
            make.bottom.equalTo(pertemuanView).offset(-Margin.s12)
        }
        
        dismissBtn.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.edges.equalTo(qrView)
        }
            
        setNeedsLayout()
        layoutIfNeeded()
    }
}

