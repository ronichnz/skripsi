//
//  QRGenerateCell.swift
//  TA_Roni
//
//  Created by Cumaroni on 05/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class QRGenerateCell: UITableViewCell {
     
    let categoryLbl = UILabel()
    let categoryLineView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .background
        
        sv([ 
            categoryLbl,
            categoryLineView
        ])
        
        setupConstraint()
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupCell() {
        categoryLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.textColor = .darkGray
            $0.numberOfLines = 0
        }
        
        categoryLineView.style {
            $0.setAsLine(height: 0.5)
            $0.backgroundColor = .silverGray
        }
    }
    
    private func setupConstraint() {
        categoryLbl.snp.makeConstraints { make in
            make.top.leading.equalTo(contentView).offset(Margin.s8)
            make.trailing.bottom.equalTo(contentView).offset(-Margin.s8)
        }
        
        categoryLineView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(contentView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
