//
//  SplashScreenInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class SplashScreenInteractor {
    weak var presenter: SplashScreenInteractorOutputDelegate?
    
    init(presenter: SplashScreenInteractorOutputDelegate?) {
        self.presenter = presenter
    }
    
}//MAIN

extension SplashScreenInteractor: SplashScreenInteractorInputDelegate {
    
}//DELEGATE


extension SplashScreenInteractor: SplashScreenInteractorOutputDelegate {
    
}//DATA_MANAGER
