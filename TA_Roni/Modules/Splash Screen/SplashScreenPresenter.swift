//
//  SplashScreenPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

final class SplashScreenPresenter {
    weak var view: SplashScreenViewControllerDelegate?
    lazy var router: SplashScreenRouterDelegate = SplashScreenRouter(view: view)
    lazy var interactor: SplashScreenInteractorInputDelegate = SplashScreenInteractor(presenter: self)
    
    init(view: SplashScreenViewControllerDelegate?) {
        self.view = view
    }
    
}//MAIN

extension SplashScreenPresenter: SplashScreenPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
}//DELEGATE

extension SplashScreenPresenter: SplashScreenInteractorOutputDelegate {
    
}//INTERACTOR
