//
//  SplashScreenViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit
import RxSwift

class SplashScreenViewController: UIViewController {
    lazy var root = SplashScreenView()
    lazy var presenter: SplashScreenPresenterDelegate = SplashScreenPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var loading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewController() 
        setAnimate()
    }
    
    private func setupViewController() {
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(splashTimeOut(_:)), userInfo: nil, repeats: false)
    }
    
    @objc private func splashTimeOut(_ sender: Timer) {
        loading = false
        
        setTransition()
        setNavbar()
//        changeBaseAPIRequest()
        setMainMenu()
    }
    
    private func changeBaseAPIRequest() {
        self.presentTextFieldAlert(title: "Set BaseAPI link", message: "Confirm API Request Address", actionTitle: "IP Address", cancelTitle: "Full Address", inputPlaceholder: "API Request Link", inputKeyboardType: .default, inputReturnKeyType: .done, cancelHandler: { fullAddress in
            ApiManager.shared.set(baseUrl: "\(fullAddress)")
            self.setMainMenu()
        }) { ipAddress in
            ApiManager.shared.set(baseUrl: "http://\(ipAddress ?? "")/SkripsiRoni/v1/")
            self.setMainMenu()
        }
    }
    
    private func setMainMenu() {
        if !String.empty(Account.getMahasiswaID) && String.empty(Account.getDosenID){ //mahasiswaView
            self.setMahasiswa()
            self.setTabbar()
        } else if String.empty(Account.getMahasiswaID) && !String.empty(Account.getDosenID) { //dosenView
            self.setDosen()
            self.setTabbar()
        } else if String.empty(Account.getMahasiswaID) && String.empty(Account.getDosenID) { //belumlogin
            DSource.window?.rootViewController = BaseRouter.createNavbar(LoginViewController())
        }
    }
    
    private func setMahasiswa() {
        let tabBar = BaseRouter.createScanTabbar()
        DSource.window?.rootViewController = BaseRouter.createNavbar(tabBar)
    }
    
    private func setDosen() {
        let tabBar = BaseRouter.createGenerateTabbar()
        DSource.window?.rootViewController = BaseRouter.createNavbar(tabBar)
    }
    
    private func setTabbar() {
        let transition = CATransition() 
        transition.subtype = CATransitionSubtype.fromRight
        DSource.window?.rootViewController?.view.window!.layer.add(transition, forKey: kCATransition)
        
        DSource.window?.makeKeyAndVisible()
        DSource.navbar?.popToRootViewController(animated: true)
        DSource.tabbar.selectedIndex = 0
    }
    
    private func setTransition() {
        let transition = CATransition()
        transition.startProgress = 0
        transition.endProgress = 1.0
        transition.type = CATransitionType.fade
        transition.duration = 0.35
        DSource.window?.layer.add(transition, forKey: "transition")
    }
    
    private func setNavbar() {
        DSource.navbar?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        DSource.navbar?.navigationBar.shadowImage = UIImage()
        DSource.navbar?.navigationBar.isTranslucent = true
        DSource.navbar?.view.backgroundColor = .clear
    }
    
    private func setAnimate() {
        root.logoImg.moveUpViewInY(duration: 0.6, delay: 0, distance: 0, options: [.curveEaseOut], isHidden: false, direct: false, completion: nil)
        root.shadowLogoImg.moveUpViewInY(duration: 0.6, delay: 0, distance: 0, options: [.curveEaseOut], isHidden: false, direct: false) { (bool) in
            self.dropWave()
        }
    }
    
    private func dropWave() {
        if loading {
            root.shadowLogoImg.hide(in: 1.5, delay: 0, withTransform: CGAffineTransform(scaleX: 1.15, y: 1.15), options: [.curveEaseOut], withHiddenProperty: false) { (bool) in
                self.root.shadowLogoImg.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.root.shadowLogoImg.alpha = 1.0
                self.dropWave()
            }
        }
    } 
}

extension SplashScreenViewController: SplashScreenViewControllerDelegate {
    
}
