//
//  SplashScreenRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//
 
import UIKit

final class SplashScreenRouter: SplashScreenRouterDelegate {
    weak var source: UIViewController?
    
    init(view: SplashScreenViewControllerDelegate?) {
        source = view as? UIViewController
    } 
}
