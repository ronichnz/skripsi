//
//  SplashScreenView.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import UIKit

final class SplashScreenView: UIView {
    
    let backgroundImg = UIImageView()
    
    let shadowLogoImg = UIImageView()
    let logoImg = UIImageView()
    
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .primary
        
        sv([backgroundImg, shadowLogoImg, logoImg])
        
        setupConstraint()
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        
    }
    
    private func setupView() {
        backgroundImg.style {
            $0.setImage(#imageLiteral(resourceName: "ic_background"))
        }
        
        [logoImg, shadowLogoImg].style {
            $0.setImage(#imageLiteral(resourceName: "ic_logo"))
        }
    }
    
    private func setupConstraint() {
        backgroundImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        logoImg.snp.makeConstraints { (make) in
//            make.size.equalTo(Margin.i100.autoSize * 2)
            make.width.equalTo(250)
            make.height.equalTo(230)
            make.center.equalTo(self)
        }
        
        shadowLogoImg.snp.makeConstraints { (make) in
            make.center.size.equalTo(logoImg)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}

