//
//  SplashScreenProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/10/19.
//  Copyright © 2019 Mojave. All rights reserved.
//
 
import UIKit

protocol SplashScreenViewControllerDelegate: AnyObject {
    
}//VIEW_CONTROLLER

protocol SplashScreenPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
}//PRESENTER

protocol SplashScreenRouterDelegate: AnyObject {
    
}//ROUTER

protocol SplashScreenInteractorInputDelegate: AnyObject {
    
}//INTERACTOR_INPUT

protocol SplashScreenInteractorOutputDelegate: AnyObject {
    
}//INTERACTOR_OUTPUT
