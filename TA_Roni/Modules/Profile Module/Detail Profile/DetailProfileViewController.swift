//
//  DetailProfileViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 19/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift

class DetailProfileViewController: UIViewController {
    lazy var root = DetailProfileView()
    lazy var presenter: DetailProfilePresenterDelegate = DetailProfilePresenter(view: self)
    
    var disposable = DisposeBag()
    
    var loading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
    
    private func setupViewController() {
        checkUser()
    }
    
    private func binding() {
        bindSaveBtn()
        bindEditProfileField()
        bindChangePasswordBtn()
    }
    
    private func bindSaveBtn() {
        func saveBtnPressed() {
            debug("pressed")
            guard let phone = root.phoneField.value else { return }
            guard let email = root.emailField.value else { return }
            if String.empty(Account.getDosenID) { //mahasiswa
                presenter.putProfileMahasiswa(phone, email, Account.getMahasiswaID)
            } else {
                presenter.putProfileDosen(phone, email, Account.getDosenID)
            }
            showLoadView(true)
        }
        root.saveBtn.rx.tap.subscribe(onNext: saveBtnPressed).disposed(by: disposable)
    }
    
    private func checkUser() {
        root.avatarImg.setImage(#imageLiteral(resourceName: "ic_logo"))
        root.phoneField.value = Account.getAccountPhone
        root.emailField.value = Account.getAccountEmail
        
        if String.empty(Account.getDosenID) { //mahasiswa
            root.nimField.value = Account.getAccountNIM
            root.semesterField.value = Account.getCurrentSemester
            root.jurusanField.value = Account.getJurusan
            root.nameField.value = Account.getMahasiswaName
        } else {
            root.nameField.value = Account.getDosenName
            root.nimField.isHidden = true
            root.semesterField.isHidden = true
            root.jurusanField.isHidden = true
        }
    }
    
    private func bindEditProfileField() {
        func editProfileFieldDidChange() {
            UIView.animate(withDuration: 0.5) {
                self.showsaveBtn(!self.checkUserData())
            }
        }
        [root.phoneField.commonField, root.emailField.commonField].style {
            $0.rx.controlEvent(.editingChanged).subscribe(onNext: editProfileFieldDidChange).disposed(by: disposable)
        }
    }
    
    private func bindChangePasswordBtn() {
        func changePassBtnPressed() {
            presenter.pushToChangePassword()
        }
        root.passBtn.rx.tap.subscribe(onNext: changePassBtnPressed).disposed(by: disposable)
    }
    
    private func showsaveBtn(_ show: Bool) {
        root.saveBtn.isHidden = !show
    }
    
    private func checkUserData() -> Bool {
        return root.phoneField.value == Account.getAccountPhone && root.emailField.value == Account.getAccountEmail || String.empty(root.phoneField.value ?? "") || String.empty(root.emailField.value ?? "")
    }
}

extension DetailProfileViewController: DetailProfileViewControllerDelegate {
    
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
    func successChangeProfile() {
        showLoadView(false)
        Account.setAccountEmail(root.emailField.value ?? "")
        Account.setAccountPhone(root.phoneField.value ?? "")
    }
    
    
}
