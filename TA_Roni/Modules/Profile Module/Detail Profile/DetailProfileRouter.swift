//
//  DetailProfileRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 19/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class DetailProfileRouter: DetailProfileRouterDelegate {
    weak var source: UIViewController?
    
    init(view: DetailProfileViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToChangePassword() {
        let changePassVc = ChangePasswordViewController()
        source?.navigationController?.pushViewController(changePassVc, animated: true)
    }
    
    func presentMessageAlert(_ message: String) {
        source?.presentAlert(title: "", message: message)
    }
}
