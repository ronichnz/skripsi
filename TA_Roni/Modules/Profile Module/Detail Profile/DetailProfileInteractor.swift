//
//  DetailProfileInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 19/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class DetailProfileInteractor {
    weak var presenter: DetailProfileInteractorOutputDelegate?
    
    private var changeProfileMahasiswDM: PutMahasiswaProfileDMRequestProtocol?
    private var changeProfileDosenDM: PutDosenProfileDMRequestProtocol?
    
    init(presenter: DetailProfileInteractorOutputDelegate?) {
        self.presenter = presenter
        changeProfileMahasiswDM = PutMahasiswaProfileDataManager(self)
        changeProfileDosenDM = PutDosenProfileDataManager(self)
    }
    
}//DetailProfile

extension DetailProfileInteractor: DetailProfileInteractorInputDelegate {
    
    func putProfileMahasiswa(_ phone: String, _ email: String, _ idMahasiswa: String) {
        changeProfileMahasiswDM?.putMahasiswaProfile(phone, email, idMahasiswa)
    }
    
    func putProfileDosen(_ phone: String, _ email: String, _ idDosen: String) {
        changeProfileDosenDM?.putDosenProfile(phone, email, idDosen)
    }
    
}//DELEGATE


extension DetailProfileInteractor: PutMahasiswaProfileResponseProtocol {
    
    func successPutMahasiswaProfile(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPutMahasiswaProfile(response)
    }
    
    func failedPutMahasiswaProfile(_ error: ApiError) {
        presenter?.failedPutMahasiswaProfile(error)
    }
    
}//DATA_MANAGER

extension DetailProfileInteractor: PutDosenProfileResponseProtocol {
    
    func successPutDosenProfile(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPutDosenProfile(response)
    }
    
    func failedPutDosenProfile(_ error: ApiError) {
        presenter?.failedPutDosenProfile(error)
    }
    
}//DATA_MANAGER
