//
//  DetailProfileView.swift
//  TA_Roni
//
//  Created by Cumaroni on 19/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class DetailProfileView: UIView {
    
    let backBtn = FakeBackBtn(title: "Detail Profil")
    
    let saveBtn = UIButton()
    
    let avatarImg = UIImageView()
    
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let stackView = UIStackView()
    
    let nimField = CommonField()
    let semesterField = CommonField()
    let jurusanField = CommonField()
    let nameField = CommonField()
    let phoneField = CommonField()
    let emailField = CommonField()
    let passField = CommonField()
    let passBtn = UIButton()
    
    let progressView = ProgressView()
    let loadView = LoadingView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .background
        
        stackView.setupArrangedSubview([
            nimField,
            semesterField,
            jurusanField,
            nameField,
            phoneField,
            emailField,
            passField,
            saveBtn,
        ])
        contentView.sv([stackView])
        scrollView.sv([contentView])
        sv([backBtn, avatarImg, progressView, scrollView, passBtn, loadView])
          
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        saveBtn.setTitle("Simpan", for: .normal)
        nimField.initialize(label: "NIM", placeholder: "...", keyboardType: .numberPad, labelColor: .gray)
        semesterField.initialize(label: "Semester", placeholder: "...", labelColor: .gray)
        jurusanField.initialize(label: "Nama Jurusan", placeholder: "...", labelColor: .gray)
        nameField.initialize(label: "Nama", placeholder: "...", labelColor: .gray)
        phoneField.initialize(label: "No. Telp", placeholder: "Masukkan no telp anda", keyboardType: .numberPad, labelColor: .darkGray)
        emailField.initialize(label: "Email", placeholder: "Masukkan email anda", keyboardType: .emailAddress, labelColor: .darkGray)
        passField.initialize(label: "Password", placeholder: "Ganti Password disini", keyboardType: .default, labelColor: .darkGray)
    }
    
    private func setupView() {
        scrollView.style {
            $0.bounces = false
            $0.backgroundColor = .white
            $0.setLayer(cornerRadius: Margin.s12)
            $0.showsVerticalScrollIndicator = false
        }
        
        avatarImg.style {
            $0.backgroundColor = .white
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.setLayer(cornerRadius: 0, borderWidth: 0.5, borderColor: .primary)
        }
        
        stackView.style {
            $0.axis = .vertical
            $0.spacing = Margin.s12
        }
        
        [nimField, jurusanField, semesterField, nameField, passField].style {
            $0.commonField.isUserInteractionEnabled = false
            $0.commonField.textColor = .lightGray
        }
        
        saveBtn.style {
            $0.backgroundColor = .primary
            $0.setTitleColor(.white, for: .normal)
            $0.setLayer(cornerRadius: 8, borderWidth: 0.5, borderColor: .primary)
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f20)
        }
    }
    
    private func setupConstraint() {
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(safeInset().top + Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        } 
        
        avatarImg.snp.makeConstraints { (make) in
            make.top.equalTo(backBtn.snp.bottom).offset(Margin.s24)
            make.centerX.equalTo(self)
            make.size.equalTo(Margin.i180.autoSize)
        }
        
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(avatarImg.snp.bottom).offset(Margin.s12)
            make.leading.equalTo(self).offset(Margin.s12)
            make.trailing.equalTo(self).offset(-Margin.s12)
            make.bottom.equalTo(self)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.edges.width.equalTo(scrollView)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.leading.equalTo(contentView).offset(Margin.s12)
            make.trailing.bottom.equalTo(contentView).offset(-Margin.s12)
        }
        
        saveBtn.snp.makeConstraints { (make) in
            make.height.equalTo(Margin.h42.autoSize)
        }
        
        progressView.snp.makeConstraints { (make) in
            make.edges.equalTo(avatarImg)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        passBtn.snp.makeConstraints { (make) in
            make.edges.equalTo(passField.commonField)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}

