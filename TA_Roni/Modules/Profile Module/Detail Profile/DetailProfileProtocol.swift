//
//  DetailProfileProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 19/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol DetailProfileViewControllerDelegate: AnyObject {
    
    func showLoadView(_ show: Bool)
    
    func successChangeProfile()
    
}//VIEW_CONTROLLER

protocol DetailProfilePresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func pushToChangePassword()
    
    func putProfileMahasiswa(_ phone: String, _ email: String, _ idMahasiswa: String)
    func putProfileDosen(_ phone: String, _ email: String, _ idDosen: String)
    
}//PRESENTER

protocol DetailProfileRouterDelegate: AnyObject {
    
    func pushToChangePassword()
    
    func presentMessageAlert(_ message: String)
    
}//ROUTER

protocol DetailProfileInteractorInputDelegate: AnyObject {
    
    func putProfileMahasiswa(_ phone: String, _ email: String, _ idMahasiswa: String)
    func putProfileDosen(_ phone: String, _ email: String, _ idDosen: String)
    
}//INTERACTOR_INPUT

protocol DetailProfileInteractorOutputDelegate: AnyObject {
    
    func successPutMahasiswaProfile(_ response: ApiResponse<EmptyModel>)
    func failedPutMahasiswaProfile(_ error: ApiError)
    
    func successPutDosenProfile(_ response: ApiResponse<EmptyModel>)
    func failedPutDosenProfile(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
