//
//  DetailProfilePresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 19/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class DetailProfilePresenter {
    weak var view: DetailProfileViewControllerDelegate?
    lazy var router: DetailProfileRouterDelegate = DetailProfileRouter(view: view)
    lazy var interactor: DetailProfileInteractorInputDelegate = DetailProfileInteractor(presenter: self)
    
    init(view: DetailProfileViewControllerDelegate?) {
        self.view = view
    }
    
}//DetailProfile

extension DetailProfilePresenter: DetailProfilePresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func pushToChangePassword() {
        router.pushToChangePassword()
    }
    
    func putProfileMahasiswa(_ phone: String, _ email: String, _ idMahasiswa: String) {
        interactor.putProfileMahasiswa(phone, email, idMahasiswa)
    }
    
    func putProfileDosen(_ phone: String, _ email: String, _ idDosen: String) {
        interactor.putProfileDosen(phone, email, idDosen)
    }
    
}//DELEGATE

extension DetailProfilePresenter: DetailProfileInteractorOutputDelegate {
    
    func successPutMahasiswaProfile(_ response: ApiResponse<EmptyModel>) {
        view?.successChangeProfile()
        router.presentMessageAlert(response.JSONResponse["message"].stringValue)
    }
    
    func failedPutMahasiswaProfile(_ error: ApiError) {
        view?.showLoadView(false)
        debug(error)
        router.presentMessageAlert(error.messages.joined(separator: ""))
    }
    
    func successPutDosenProfile(_ response: ApiResponse<EmptyModel>) {
        view?.successChangeProfile()
        router.presentMessageAlert(response.JSONResponse["message"].stringValue)
    }
    
    func failedPutDosenProfile(_ error: ApiError) {
        view?.showLoadView(false)
        debug(error)
        router.presentMessageAlert(error.messages.joined(separator: ""))
    }
    
    
}//INTERACTOR
