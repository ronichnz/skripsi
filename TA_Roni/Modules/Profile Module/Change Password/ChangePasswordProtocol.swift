//
//  ChangePasswordProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/06/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol ChangePasswordViewControllerDelegate: AnyObject {
    
    func showLoadView(_ show: Bool)
    
}//VIEW_CONTROLLER

protocol ChangePasswordPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func putPassMahasiswa(_ oldPass: String, _ newPass: String, _ idMahasiswa: String)
    func putPassDosen(_ oldPass: String, _ newPass: String, _ idDosen: String)
    
}//PRESENTER

protocol ChangePasswordRouterDelegate: AnyObject {
    
    func presentSuccessAlert()
    func presentFailedAlert(_ message: String)
    
}//ROUTER

protocol ChangePasswordInteractorInputDelegate: AnyObject {
    
    func putPassMahasiswa(_ oldPass: String, _ newPass: String, _ idMahasiswa: String)
    func putPassDosen(_ oldPass: String, _ newPass: String, _ idDosen: String)
    
}//INTERACTOR_INPUT

protocol ChangePasswordInteractorOutputDelegate: AnyObject {
    
    func successPutMahasiswaPassword(_ response: ApiResponse<EmptyModel>)
    func failedPutMahasiswaPassword(_ error: ApiError)
    
    func successPutDosenPassword(_ response: ApiResponse<EmptyModel>)
    func failedPutDosenPassword(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
