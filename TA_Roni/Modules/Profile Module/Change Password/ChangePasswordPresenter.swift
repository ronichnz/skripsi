//
//  ChangePasswordPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/06/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class ChangePasswordPresenter {
    weak var view: ChangePasswordViewControllerDelegate?
    lazy var router: ChangePasswordRouterDelegate = ChangePasswordRouter(view: view)
    lazy var interactor: ChangePasswordInteractorInputDelegate = ChangePasswordInteractor(presenter: self)
    
    init(view: ChangePasswordViewControllerDelegate?) {
        self.view = view
    }
    
}//ChangePassword

extension ChangePasswordPresenter: ChangePasswordPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func putPassMahasiswa(_ oldPass: String, _ newPass: String, _ idMahasiswa: String) {
        interactor.putPassMahasiswa(oldPass, newPass, idMahasiswa)
    }
    
    func putPassDosen(_ oldPass: String, _ newPass: String, _ idDosen: String) {
        interactor.putPassDosen(oldPass, newPass, idDosen)
    }
    
   
}//DELEGATE

extension ChangePasswordPresenter: ChangePasswordInteractorOutputDelegate {
    
    func successPutMahasiswaPassword(_ response: ApiResponse<EmptyModel>) {
        view?.showLoadView(false)
        debug(response.JSONResponse)
        router.presentSuccessAlert()
    }
    
    func failedPutMahasiswaPassword(_ error: ApiError) {
        view?.showLoadView(false)
        router.presentFailedAlert(error.messages.joined(separator: ""))
    }
    
    func successPutDosenPassword(_ response: ApiResponse<EmptyModel>) {
        view?.showLoadView(false)
        debug(response.JSONResponse)
        router.presentSuccessAlert()
    }
    
    func failedPutDosenPassword(_ error: ApiError) {
        view?.showLoadView(false)
        router.presentFailedAlert(error.messages.joined(separator: ""))
    }
    
    
}//INTERACTOR
