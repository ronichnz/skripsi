//
//  ChangePasswordInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/06/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class ChangePasswordInteractor {
    weak var presenter: ChangePasswordInteractorOutputDelegate?
    
    private var changePassMahasiswaDM: PutMahasiswaPasswordDMRequestProtocol?
    private var changePassDosenDM: PutDosenPasswordDMRequestProtocol?
    
    init(presenter: ChangePasswordInteractorOutputDelegate?) {
        self.presenter = presenter
        changePassMahasiswaDM = PutMahasiswaPasswordDataManager(self)
        changePassDosenDM = PutDosenPasswordDataManager(self)
    }
    
}//ChangePassword

extension ChangePasswordInteractor: ChangePasswordInteractorInputDelegate {
    
    func putPassMahasiswa(_ oldPass: String, _ newPass: String, _ idMahasiswa: String) {
        changePassMahasiswaDM?.putMahasiswaPassword(oldPass, newPass, idMahasiswa)
    }
    
    func putPassDosen(_ oldPass: String, _ newPass: String, _ idDosen: String) {
        changePassDosenDM?.putDosenPassword(oldPass, newPass, idDosen)
    }
    
}//DELEGATE


extension ChangePasswordInteractor: PutMahasiswaPasswordResponseProtocol {
    
    func successPutMahasiswaPassword(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPutMahasiswaPassword(response)
    }
    
    func failedPutMahasiswaPassword(_ error: ApiError) {
        presenter?.failedPutMahasiswaPassword(error)
    }
    
}//DATA_MANAGER

extension ChangePasswordInteractor: PutDosenPasswordResponseProtocol {

    func successPutDosenPassword(_ response: ApiResponse<EmptyModel>) {
        presenter?.successPutDosenPassword(response)
    }
    
    func failedPutDosenPassword(_ error: ApiError) {
        presenter?.failedPutDosenPassword(error)
    }
    
}//DATA_MANAGER
