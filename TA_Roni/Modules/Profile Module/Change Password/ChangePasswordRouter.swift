//
//  ChangePasswordRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/06/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class ChangePasswordRouter: ChangePasswordRouterDelegate {
    weak var source: UIViewController?
    
    init(view: ChangePasswordViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func presentSuccessAlert() {
        source?.presentAlert(title: "Ubah Password Berhasil", message: "Silahkan login kembali menggunakan password baru anda", alertAction: [
            UIAlertAction(title: "Okay", style: .default, handler: { _ in
                Account.logout()
                DSource.window?.rootViewController = LoginViewController()
            })
        ])
    }
    
    func presentFailedAlert(_ message: String) {
        source?.presentAlert(title: "Gagal", message: message)
    }
}
