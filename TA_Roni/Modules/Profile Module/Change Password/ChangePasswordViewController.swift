//
//  ChangePasswordViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/06/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift

class ChangePasswordViewController: UIViewController {
    lazy var root = ChangePasswordView()
    lazy var presenter: ChangePasswordPresenterDelegate = ChangePasswordPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var loading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
    
    private func setupViewController() {
        
    }
    
    private func binding() {
        bindOldViewPassBtn()
        bindNewViewPassBtn()
        bindConfirmViewPassBtn()
        bindSaveBtn()
    }
    
    private func bindOldViewPassBtn() {
        func oldViewPassBtnPressed() {
            root.oldPassField.commonField.isSecureTextEntry = !(root.oldViewPassBtn.currentImage == UIImage(named: "ic_unsee"))
            root.oldViewPassBtn.setImage(root.oldViewPassBtn.currentImage == UIImage(named: "ic_unsee") ? #imageLiteral(resourceName: "ic_see") : #imageLiteral(resourceName: "ic_unsee") , for: .normal)
        }
        root.oldViewPassBtn.rx.tap.subscribe(onNext: oldViewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindNewViewPassBtn() {
        func newViewPassBtnPressed() {
            root.newPassField.commonField.isSecureTextEntry = !(root.newViewPassBtn.currentImage == UIImage(named: "ic_unsee"))
            root.newViewPassBtn.setImage(root.newViewPassBtn.currentImage == UIImage(named: "ic_unsee") ? #imageLiteral(resourceName: "ic_see") : #imageLiteral(resourceName: "ic_unsee") , for: .normal)
        }
        root.newViewPassBtn.rx.tap.subscribe(onNext: newViewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindConfirmViewPassBtn() {
        func confirmViewPassBtnPressed() {
            root.confirmPassField.commonField.isSecureTextEntry = !(root.confirmViewPassBtn.currentImage == UIImage(named: "ic_unsee"))
            root.confirmViewPassBtn.setImage(root.confirmViewPassBtn.currentImage == UIImage(named: "ic_unsee") ? #imageLiteral(resourceName: "ic_see") : #imageLiteral(resourceName: "ic_unsee") , for: .normal)
        }
        root.confirmViewPassBtn.rx.tap.subscribe(onNext: confirmViewPassBtnPressed).disposed(by: disposable)
    }
    
    private func bindSaveBtn() {
        func saveBtnPressed() {
            checkPasswordField()
        }
        root.saveBtn.rx.tap.subscribe(onNext: saveBtnPressed).disposed(by: disposable)
    }
    
    private func checkPasswordField() {
        let oldPass = root.oldPassField.value ?? ""
        let newPass = root.newPassField.value ?? ""
        let confirmPass = root.confirmPassField.value ?? ""
        
        if String.empty(oldPass) {
            self.presentAlert(title: "Gagal", message: "Password Lama tidak boleh kosong")
        } else if String.empty(newPass) {
            self.presentAlert(title: "Gagal", message: "Password Baru tidak boleh kosong")
        } else if String.empty(confirmPass) {
            self.presentAlert(title: "Gagal", message: "Konfirmasi Password tidak boleh kosong")
        } else if newPass != confirmPass {
            self.presentAlert(title: "Gagal", message: "Konfirmasi Password tidak sama dengan password baru")
        } else {
            if String.empty(Account.getDosenID) { //mahasiswa
                presenter.putPassMahasiswa(oldPass, newPass, Account.getMahasiswaID)
            } else {
                presenter.putPassDosen(oldPass, newPass, Account.getDosenID)
            }
            showLoadView(true)
            root.endEditing(true)
        }
    }
    
}

extension ChangePasswordViewController: ChangePasswordViewControllerDelegate {
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
}
