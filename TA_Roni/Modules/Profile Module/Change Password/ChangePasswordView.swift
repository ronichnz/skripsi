//
//  ChangePasswordView.swift
//  TA_Roni
//
//  Created by Cumaroni on 30/06/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class ChangePasswordView: UIView {
    
    let backBtn = FakeBackBtn(title: "Ganti Password")
    
    let stackView = UIStackView()
    
    let oldPassField = CommonField()
    let oldViewPassBtn = UIButton()
    
    let newPassField = CommonField()
    let newViewPassBtn = UIButton()
    
    let confirmPassField = CommonField()
    let confirmViewPassBtn = UIButton()
    
    let saveBtn = UIButton()
    let loadView = LoadingView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .background
        
        stackView.setupArrangedSubview([oldPassField, newPassField, confirmPassField])
        sv([backBtn, stackView, oldViewPassBtn, newViewPassBtn, confirmViewPassBtn, saveBtn, loadView])
          
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        oldPassField.initialize(label: "Password Lama", placeholder: "Masukkan password lama anda", isSecure: true)
        newPassField.initialize(label: "Password Baru", placeholder: "Masukkan password baru anda", isSecure: true)
        confirmPassField.initialize(label: "Konfirmasi Password Baru", placeholder: "Masukkan ulang password baru anda", isSecure: true)
        saveBtn.setTitle("SIMPAN", for: .normal)
    }
    
    private func setupView() {
        stackView.style {
            $0.axis = .vertical
            $0.spacing = Margin.s12
        }
        
        [oldViewPassBtn, newViewPassBtn, confirmViewPassBtn].style {
            $0.setImage(#imageLiteral(resourceName: "ic_unsee"), for: .normal)
        }
        
        saveBtn.style {
            $0.backgroundColor = .primary
            $0.setTitleColor(.white, for: .normal)
            $0.setLayer(cornerRadius: 8, borderWidth: 0.5, borderColor: .primary)
            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: Margin.f20)
        }
    }
    
    private func setupConstraint() {
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(safeInset().top + Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(backBtn.snp.bottom).offset(Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        }
        
        oldViewPassBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(oldPassField.commonField)
            make.trailing.equalTo(oldPassField).offset(-Margin.s12)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        newViewPassBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(newPassField.commonField)
            make.trailing.equalTo(newPassField).offset(-Margin.s12)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        confirmViewPassBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(confirmPassField.commonField)
            make.trailing.equalTo(confirmPassField).offset(-Margin.s12)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        saveBtn.snp.makeConstraints { (make) in
            make.top.equalTo(stackView.snp.bottom).offset(Margin.s24)
            make.leading.trailing.equalTo(stackView)
            make.height.equalTo(Margin.h42.autoSize)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
