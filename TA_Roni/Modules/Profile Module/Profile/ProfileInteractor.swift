//
//  ProfileInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class ProfileInteractor {
    weak var presenter: ProfileInteractorOutputDelegate?
    
    init(presenter: ProfileInteractorOutputDelegate?) {
        self.presenter = presenter
    }
    
}//Profile

extension ProfileInteractor: ProfileInteractorInputDelegate {
    
}//DELEGATE


extension ProfileInteractor: ProfileInteractorOutputDelegate {
    
}//DATA_MANAGER
