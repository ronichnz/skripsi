
//
//  ProfileCell.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class ProfileCell: UITableViewCell {
    
    let categoryImg = UIImageView()
    let categoryLbl = UILabel()
    let categoryLineView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .background
        
        sv([
            categoryImg,
            categoryLbl,
            categoryLineView
        ])
        
        setupConstraint()
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupCell() {
        categoryLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.textColor = .darkGray
        }
        
        categoryLineView.style {
            $0.setAsLine(height: 0.5)
            $0.backgroundColor = .lightGray
        }
    }
    
    private func setupConstraint() {
        categoryImg.snp.makeConstraints { (make) in
            make.centerY.equalTo(categoryLbl)
            make.leading.equalTo(contentView).offset(Margin.s24)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        categoryLbl.snp.makeConstraints { make in
            make.top.equalTo(contentView).offset(Margin.s16)
            make.leading.equalTo(categoryImg.snp.trailing).offset(Margin.s12)
            make.trailing.bottom.equalTo(contentView).offset(-Margin.s16)
        }
        
        categoryLineView.snp.makeConstraints { (make) in
            make.leading.equalTo(categoryImg)
            make.trailing.equalTo(contentView).offset(Margin.s16)
            make.bottom.equalTo(contentView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

