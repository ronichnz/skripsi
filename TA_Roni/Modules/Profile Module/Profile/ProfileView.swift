//
//  ProfileView.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class ProfileView: UIView {
    
    let avatarImg = UIImageView()
    let nameLbl = UILabel()
    let stackView = UIStackView()
    let emailLbl = UILabel()
    let phoneLbl = UILabel()
    let editBtn = UIButton()
    
    let leftStCircleView = UIView()
    let leftNdCircleView = UIView()
    
    let rightStCircleView = UIView()
    let rightNdCircleView = UIView()
    
    let profileTblView = UITableView()
    
    let loadView = LoadingView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .primary
        
        stackView.setupArrangedSubview([emailLbl, phoneLbl])
        sv([
            avatarImg,
            nameLbl,
            stackView,
            editBtn,
            leftStCircleView, leftNdCircleView,
            rightStCircleView, rightNdCircleView,
            profileTblView,
            loadView,
        ])
          
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        editBtn.setTitle("Detail Profil", for: .normal)
    }
    
    private func setupView() {
        avatarImg.style {
            $0.clipsToBounds = true
//            $0.setLayer(cornerRadius: 0, borderWidth: 0.5, borderColor: .clear)
        }
        
        nameLbl.style {
            $0.font = UIFont.boldSystemFont(ofSize: Margin.f18)
            $0.textAlignment = .center
            $0.textColor = .white
        }
        
        stackView.style {
            $0.spacing = Margin.s8
            $0.distribution = .equalCentering
            $0.alignment = .fill
            $0.axis = .vertical
        }
        
        [emailLbl, phoneLbl].style {
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.textAlignment = .center
            $0.textColor = .white
        }
        
        editBtn.style {
            $0.setLayer(cornerRadius: 8, borderWidth: 0.7, borderColor: .white)
            $0.backgroundColor = .clear
            $0.setTitleColor(.white, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.contentEdgeInsets = UIEdgeInsets(horizontal: Margin.s24, vertical: Margin.s12)
        }
        
        [leftStCircleView, leftNdCircleView, rightStCircleView, rightNdCircleView].style {
            $0.backgroundColor = .white
            $0.alpha = 0.3
            $0.setLayer(cornerRadius: 0)
        }
        
        profileTblView.style {
            $0.backgroundColor = .white
            $0.bounces = false
            $0.flexibleHeight()
            $0.register(ProfileCell.self, forCellReuseIdentifier: "cell")
            $0.showsVerticalScrollIndicator = false
            $0.isScrollEnabled = false
            $0.separatorStyle = .none
            $0.setLayer(cornerRadius: 10)
        }
    }
    
    func setupTopTblConstraints() {
        profileTblView.snp.remakeConstraints { (make) in
            make.top.equalTo(self).offset((safeInset().top) + Margin.s8)
            make.leading.trailing.bottom.equalTo(self)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    func setupDownTblConstraints() {
        profileTblView.snp.remakeConstraints { (make) in
            make.top.equalTo(editBtn.snp.bottom).offset(Margin.s16)
            make.leading.trailing.height.equalTo(self)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    private func setupConstraint() {
        avatarImg.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset((safeInset().top) + Margin.s44)
            make.centerX.equalTo(self)
            make.width.equalTo(180)
            make.height.equalTo(160)
        }
        
        nameLbl.snp.makeConstraints { (make) in
            make.top.equalTo(avatarImg.snp.bottom).offset(Margin.s12)
            make.leading.equalTo(self).offset(Margin.s32)
            make.trailing.equalTo(self).offset(-Margin.s32)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(nameLbl.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(nameLbl)
        }
        
        editBtn.snp.makeConstraints { (make) in
            make.top.equalTo(stackView.snp.bottom).offset(Margin.s16)
            make.centerX.equalTo(self)
        }
        
        leftStCircleView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.leading).offset(Margin.s12)
            make.centerY.equalTo(profileTblView.snp.top).offset(-Margin.s12)
            make.size.equalTo(Margin.i100)
        }
        
        leftNdCircleView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.leading)
            make.centerY.equalTo(leftStCircleView.snp.top).offset(Margin.s8)
            make.size.equalTo(Margin.i70)
        }
        
        rightStCircleView.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileTblView.snp.top).offset(Margin.s12)
            make.trailing.equalTo(self)
            make.size.equalTo(Margin.i120)
        }
        
        rightNdCircleView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.trailing)
            make.centerY.equalTo(rightStCircleView.snp.top).offset(Margin.s12)
            make.size.equalTo(Margin.i100)
        }
        
        profileTblView.snp.makeConstraints { (make) in
            make.top.equalTo(editBtn.snp.bottom).offset(Margin.s16)
            make.leading.trailing.height.equalTo(self)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
            make.width.equalTo(mainScreen.width / 2)
            make.height.equalTo(mainScreen.height / 7)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
