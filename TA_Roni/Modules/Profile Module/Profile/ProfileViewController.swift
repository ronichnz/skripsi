//
//  ProfileViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import RxGesture
import RxRelay

class ProfileViewController: UIViewController {
    lazy var root = ProfileView()
    lazy var presenter: ProfilePresenterDelegate = ProfilePresenter(view: self)
    
    var disposable = DisposeBag()
    
    var categoryData = BehaviorRelay<[ProfileSt]>(value: [])
    
    var tblPosition = CGFloat()
    var firstTime: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        binding()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
        setupViewController()
    }
    
    private func setupViewController() {
        setData()
        if firstTime {
            setTblPosition()
            firstTime = false
        }
    }
    
    private func binding() {
        bindProfileCategoryTbl()
        bindSwipeTbl()
        bindEditBtn()
    }
    
    private func bindProfileCategoryTbl() {
        categoryData.bind(to: root.profileTblView.rx.items(cellIdentifier: "cell", cellType: ProfileCell.self)) { index, model, cell in
            cell.categoryImg.image = model.categoryImg?.transparent
            cell.categoryLbl.text = model.categoryLbl
            cell.categoryImg.tintColor = model.categoryNmbr == 3 ? .red : .darkGray
            cell.categoryLbl.textColor = model.categoryNmbr == 3 ? .red : .darkGray
        }.disposed(by: disposable)

        root.profileTblView.rx.modelSelected(ProfileSt.self).subscribe { model in
            switch model.element?.categoryNmbr {
            case 0: //absensi
                self.presenter.pushToAbsenReport()
            case 1://lowongan
                self.presenter.pushToWebView("Lowongan Pekerjaan", "http://www.stmik-time.ac.id/page.php?content=lowongan")
            case 2://hubungi kami
                self.presenter.pushToWebView("Hubungi Kami", "http://www.stmik-time.ac.id/contact.php")
            case 3://logout
                self.presenter.logout()
            case 4: //absensi mahasiswa
                self.presenter.pushToDosenReport()
            default:
                self.presentAlert(title: "Coming Soon", message: "")
            }
        }.disposed(by: disposable)
    }
        
    private func bindSwipeTbl() {
        root.profileTblView.rx.panGesture().when(.changed).subscribe(onNext: { (gestureRecognizer) in
            let translation = gestureRecognizer.translation(in: self.root.profileTblView)
            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.root.profileTblView)
            if self.root.profileTblView.frame.origin.y < ((safeInset().top) + Margin.s8) {
                self.root.profileTblView.frame.origin.y = ((safeInset().top) + Margin.s8)
            } else if self.root.profileTblView.frame.origin.y > self.tblPosition {
                self.root.profileTblView.frame.origin.y = self.tblPosition
            }
        }).disposed(by: disposable)
        
        root.profileTblView.rx.panGesture().when(.ended).subscribe(onNext: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                self.root.profileTblView.frame.origin.y > self.tblPosition / 2 ? self.root.setupDownTblConstraints() : self.root.setupTopTblConstraints()
            })
        }).disposed(by: disposable)
    }
    
    private func bindEditBtn() {
        func editBtnPressed() {
            presenter.pushToDetailProfile()
        }
        root.editBtn.rx.tap.subscribe(onNext: editBtnPressed).disposed(by: disposable)
    }
    
    private func setData() {
        if String.empty(Account.getAccountAvatar) {
            root.avatarImg.setImage(#imageLiteral(resourceName: "ic_logo"))
        } else {
            let url = URL(string: Account.getAccountAvatar)
            root.avatarImg.kf.setImage(with: url)
        }
        root.nameLbl.text = !String.empty(Account.getMahasiswaID) ? Account.getMahasiswaName : Account.getDosenName
        root.emailLbl.text = !String.empty(Account.getAccountEmail) ? Account.getAccountEmail : "stmiktime@gmail.com"
        root.phoneLbl.text = !String.empty(Account.getMahasiswaID) ? Account.getAccountNIM : Account.getAccountPhone
    }
    
}

extension ProfileViewController: ProfileViewControllerDelegate {
    
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
    func setProfileCategoryData(_ data: [ProfileSt]) {
        categoryData.accept(data)
    }
    
    func userLogout() {
        UIView.animate(withDuration: 4) {
            Account.logout()
            self.root.loadView.showLoading(false)
            self.presenter.presentSuccessLogout()
        } 
    }
    
}

extension ProfileViewController {
     
    private func setTblPosition() {
        tblPosition = root.profileTblView.frame.origin.y
    }
    
}


