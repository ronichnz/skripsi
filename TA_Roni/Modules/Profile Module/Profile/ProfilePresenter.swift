//
//  ProfilePresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class ProfilePresenter {
    weak var view: ProfileViewControllerDelegate?
    lazy var router: ProfileRouterDelegate = ProfileRouter(view: view)
    lazy var interactor: ProfileInteractorInputDelegate = ProfileInteractor(presenter: self)
    
    init(view: ProfileViewControllerDelegate?) {
        self.view = view
    }
    
    private func setMahasiswaData() {
        let data = [
            ProfileSt(categoryNmbr: 0, categoryImg: #imageLiteral(resourceName: "ic_calendar"), categoryLbl: "Laporan Kehadiran"),
            ProfileSt(categoryNmbr: 1, categoryImg: #imageLiteral(resourceName: "ic_suitcase"), categoryLbl: "Lowongan Pekerjaan"),
            ProfileSt(categoryNmbr: 2, categoryImg: #imageLiteral(resourceName: "ic_contact_us"), categoryLbl: "Hubungi Kami"),
            ProfileSt(categoryNmbr: 3, categoryImg: #imageLiteral(resourceName: "ic_logout"), categoryLbl: "Keluar"),
        ]
        view?.setProfileCategoryData(data)
    }
    
    private func setDosenData() {
        let data = [
            ProfileSt(categoryNmbr: 4, categoryImg: #imageLiteral(resourceName: "ic_calendar"), categoryLbl: "Laporan Kehadiran Mahasiswa"),
            ProfileSt(categoryNmbr: 2, categoryImg: #imageLiteral(resourceName: "ic_contact_us"), categoryLbl: "Hubungi Kami"),
            ProfileSt(categoryNmbr: 3, categoryImg: #imageLiteral(resourceName: "ic_logout"), categoryLbl: "Keluar"),
        ]
        view?.setProfileCategoryData(data)
    }
    
}//Profile

extension ProfilePresenter: ProfilePresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func viewDidAppear() {
        if !String.empty(Account.getMahasiswaID) && String.empty(Account.getDosenID) { //mahasiswaView
            setMahasiswaData()
        } else { //dosenView
            setDosenData()
        }
    }
    
    func pushToDetailProfile() {
        router.pushToDetailProfile()
    }
    
    func pushToAbsenReport() {
        router.pushToAbsenReport()
    }
    
    func pushToDosenReport() {
        router.pushToDosenReport()
    }
    
    func pushToWebView(_ title: String, _ url: String) {
        router.pushToWebView(with: title, url)
    }
    
    func logout() {
        router.logout { isLogout in
            if isLogout {
                self.view?.showLoadView(true)
                self.view?.userLogout()
            }
        }
    }
    
    func presentSuccessLogout() {
        router.presentSuccessLogout()
    }
    
}//DELEGATE

extension ProfilePresenter: ProfileInteractorOutputDelegate {
    
}//INTERACTOR
