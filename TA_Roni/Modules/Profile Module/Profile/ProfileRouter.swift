//
//  ProfileRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class ProfileRouter: ProfileRouterDelegate {
    weak var source: UIViewController?
    
    init(view: ProfileViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToDetailProfile() {
        let detailvc = DetailProfileViewController()
        source?.navigationController?.pushViewController(detailvc, animated: true)
    }
    
    func pushToAbsenReport() {
        let absenVc = AbsenReportViewController()
        source?.navigationController?.pushViewController(absenVc, animated: true)
    }
    
    func pushToDosenReport() {
        let reportVc = DosenMatkulViewController()
        source?.navigationController?.pushViewController(reportVc, animated: true)
    }
    
    func pushToWebView(with title: String, _ url: String) {
        let webVc = WebViewController()
        webVc.setData(title: title, url: url)
        source?.navigationController?.pushViewController(webVc, animated: true)
    }
    
    func logout(_ isLogout: @escaping(Bool) -> Void) {
        source?.presentAlert(title: "Konfirmasi", message: "Apakah anda yakin ingin keluar?", alertAction: [
            UIAlertAction(title: "Keluar", style: .destructive, handler: { _ in
                isLogout(true)
            }),
            UIAlertAction(title: "Batal", style: .cancel, handler: nil)
        ])
    }
    
    func presentSuccessLogout() {
        source?.presentAlert(title: "Sampai jumpa kembali", message: "", alertAction: [
            UIAlertAction(title: "Tutup", style: .default, handler: { _ in
                DSource.window?.rootViewController = LoginViewController()
            })
        ])
    }
}
