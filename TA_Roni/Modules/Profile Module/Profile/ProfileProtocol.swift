//
//  ProfileProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 23/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol ProfileViewControllerDelegate: AnyObject {
    
    func showLoadView(_ show: Bool)
    
    func userLogout()
    
    func setProfileCategoryData(_ data: [ProfileSt])
    
}//VIEW_CONTROLLER

protocol ProfilePresenterDelegate: AnyObject {
    
    func viewDidLoad()
    func viewDidAppear()
    
    func pushToDetailProfile()
    
    func pushToAbsenReport()
    func pushToDosenReport()
    
    func pushToWebView(_ title: String, _ url: String)
    
    func logout()
    func presentSuccessLogout()
    
}//PRESENTER

protocol ProfileRouterDelegate: AnyObject {
    
    func pushToDetailProfile()
    
    func pushToAbsenReport()
    func pushToDosenReport()
    
    func pushToWebView(with title: String, _ url: String)
    
    func logout(_ isLogout: @escaping(Bool) -> Void)
    func presentSuccessLogout()
    
}//ROUTER

protocol ProfileInteractorInputDelegate: AnyObject {
    
}//INTERACTOR_INPUT

protocol ProfileInteractorOutputDelegate: AnyObject {
    
}//INTERACTOR_OUTPUT
