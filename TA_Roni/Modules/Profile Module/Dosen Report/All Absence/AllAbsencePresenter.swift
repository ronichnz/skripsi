//
//  AllAbsencePresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class AllAbsencePresenter {
    weak var view: AllAbsenceViewControllerDelegate?
    lazy var router: AllAbsenceRouterDelegate = AllAbsenceRouter(view: view)
    lazy var interactor: AllAbsenceInteractorInputDelegate = AllAbsenceInteractor(presenter: self)
    
    init(view: AllAbsenceViewControllerDelegate?) {
        self.view = view
    }
    
}//AllAbsence

extension AllAbsencePresenter: AllAbsencePresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
}//DELEGATE

extension AllAbsencePresenter: AllAbsenceInteractorOutputDelegate {
    
    
}//INTERACTOR
