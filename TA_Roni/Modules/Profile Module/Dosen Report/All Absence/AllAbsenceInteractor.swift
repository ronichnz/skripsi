//
//  AllAbsenceInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class AllAbsenceInteractor {
    weak var presenter: AllAbsenceInteractorOutputDelegate?
    
    init(presenter: AllAbsenceInteractorOutputDelegate?) {
        self.presenter = presenter
    }
    
}//AllAbsence

extension AllAbsenceInteractor: AllAbsenceInteractorInputDelegate {
    
}//DELEGATE


extension AllAbsenceInteractor: AllAbsenceInteractorOutputDelegate {
    
}//DATA_MANAGER
