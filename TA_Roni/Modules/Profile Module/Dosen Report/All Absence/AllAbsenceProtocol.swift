//
//  AllAbsenceProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol AllAbsenceViewControllerDelegate: AnyObject {
    
}//VIEW_CONTROLLER

protocol AllAbsencePresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
}//PRESENTER

protocol AllAbsenceRouterDelegate: AnyObject {
    
}//ROUTER

protocol AllAbsenceInteractorInputDelegate: AnyObject {
     
    
}//INTERACTOR_INPUT

protocol AllAbsenceInteractorOutputDelegate: AnyObject {
    
}//INTERACTOR_OUTPUT
