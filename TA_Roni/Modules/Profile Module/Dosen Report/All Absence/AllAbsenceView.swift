//
//  AllAbsenceView.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class AllAbsenceView: UIView {
    
    let backBtn = FakeBackBtn(title: "")
    
    let detailView = UIView()
    let absenTitleLbl = UILabel()
    let absenLineView = UIView()
    let absenTbl = UITableView()
    
    let emptyView = EmptyAbsenceView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .background
        
        detailView.sv([absenTitleLbl, absenLineView, absenTbl])
        sv([backBtn, detailView, emptyView])
          
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        emptyView.setEmptyLbl("Tidak ada mahasiswa yang\nmenghadiri kelas ini")
    }
    
    private func setupView() {
        detailView.style {
            $0.backgroundColor = .white
            $0.setLayer(cornerRadius: 8)
            $0.setShadow(radius: 2, opacity: 0.4)
        }
        
        absenTitleLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f20, weight: .semibold)
            $0.numberOfLines = 0
            $0.textColor = .darkGray
            $0.textAlignment = .center
        }
        
        absenLineView.style {
            $0.setAsLine(height: 0.3)
            $0.backgroundColor = .silverGray
        }
        
        absenTbl.style {
            $0.backgroundColor = .white
            $0.bounces = false
            $0.flexibleHeight()
            $0.register(AbsenDetailCell.self, forCellReuseIdentifier: "cell")
            $0.showsVerticalScrollIndicator = false
            $0.separatorStyle = .none
            $0.tableFooterView = UIView()
        }
    }
    
    private func setupConstraint() {
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(safeInset().top + Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        }
        
        detailView.snp.makeConstraints { (make) in
            make.top.equalTo(backBtn.snp.bottom).offset(Margin.s24)
            make.leading.trailing.equalTo(backBtn)
            make.bottom.equalTo(self).offset(-Margin.s16)
        }
        
        absenTitleLbl.snp.makeConstraints { (make) in
            make.top.leading.equalTo(detailView).offset(Margin.s12)
            make.trailing.equalTo(detailView).offset(-Margin.s12)
        }
        
        absenLineView.snp.makeConstraints { (make) in
            make.top.equalTo(absenTitleLbl.snp.bottom).offset(Margin.s12)
            make.leading.trailing.equalTo(detailView)
        }
        
        absenTbl.snp.makeConstraints { (make) in
            make.top.equalTo(absenLineView.snp.bottom)
            make.leading.trailing.equalTo(absenTitleLbl)
            make.bottom.equalTo(detailView).offset(-Margin.s12)
        }
        
        emptyView.snp.makeConstraints { (make) in
            make.edges.equalTo(absenTbl)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}

