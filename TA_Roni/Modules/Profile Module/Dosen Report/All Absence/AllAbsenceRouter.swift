//
//  AllAbsenceRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class AllAbsenceRouter: AllAbsenceRouterDelegate {
    weak var source: UIViewController?
    
    init(view: AllAbsenceViewControllerDelegate?) {
        source = view as? UIViewController
    }
     
}
