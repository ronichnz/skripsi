//
//  DosenMatkulRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class DosenMatkulRouter: DosenMatkulRouterDelegate {
    weak var source: UIViewController?
    
    init(view: DosenMatkulViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToDosenPertemuan(_ title: String, _ detailData: [DosenPertemuanModel]) {
        let detailVc = DosenPertemuanViewController()
        detailVc.setData(title, detailData)
        source?.navigationController?.pushViewController(detailVc, animated: true)
    }
}
