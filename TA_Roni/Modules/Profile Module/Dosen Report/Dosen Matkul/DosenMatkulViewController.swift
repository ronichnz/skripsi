//
//  DosenMatkulViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay

class DosenMatkulViewController: UIViewController {
    lazy var root = DosenMatkulView()
    lazy var presenter: DosenMatkulPresenterDelegate = DosenMatkulPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var matkulData = BehaviorRelay<[DosenReportModel]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
    
    private func setupViewController() {
        
    }
    
    private func binding() {
        bindStudyTbl()
    }
    
    private func bindStudyTbl() {
        matkulData.asObservable().bind(to: root.studyTbl.rx.items(cellIdentifier: "cell", cellType: AbsenReportCell.self)) { (index, model, cell) in
            cell.categoryLbl.text = model.namaMatkul
        }.disposed(by: disposable)
        
        root.studyTbl.rx.modelSelected(DosenReportModel.self).subscribe(onNext: { model in
            self.presenter.pushToDosenPertemuan(model.namaMatkul, model.pertemuan)
        }).disposed(by: disposable)
    }
}

extension DosenMatkulViewController: DosenMatkulViewControllerDelegate {
    
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
    func showEmptyView(_ show: Bool) {
        showLoadView(false)
        root.emptyView.showEmptyView(show)
    }
    
    func setData(_ data: [DosenReportModel]) { 
        self.matkulData.accept(data)
        showEmptyView(data.count == 0)
    }
    
}
