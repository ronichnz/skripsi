//
//  DosenMatkulPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class DosenMatkulPresenter {
    weak var view: DosenMatkulViewControllerDelegate?
    lazy var router: DosenMatkulRouterDelegate = DosenMatkulRouter(view: view)
    lazy var interactor: DosenMatkulInteractorInputDelegate = DosenMatkulInteractor(presenter: self)
    
    init(view: DosenMatkulViewControllerDelegate?) {
        self.view = view
    }
    
}//DosenMatkul

extension DosenMatkulPresenter: DosenMatkulPresenterDelegate {
    
    func viewDidLoad() {
        view?.showLoadView(true)
        interactor.getDosenReport(Account.getDosenID)
    }
    
    func pushToDosenPertemuan(_ title: String, _ detailData: [DosenPertemuanModel]) {
        router.pushToDosenPertemuan(title, detailData)
    }
    
}//DELEGATE

extension DosenMatkulPresenter: DosenMatkulInteractorOutputDelegate {
    
    func successGetDosenReport(_ response: ApiResponse<DosenReportModel>) { 
        view?.setData(response.data)
    }
    
    func failedGetDosenReport(_ error: ApiError) {
        view?.showLoadView(false)
    }
    
}//INTERACTOR
