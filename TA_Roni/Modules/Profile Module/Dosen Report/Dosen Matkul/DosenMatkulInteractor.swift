//
//  DosenMatkulInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class DosenMatkulInteractor {
    weak var presenter: DosenMatkulInteractorOutputDelegate?
    
    private var reportDM: DosenReportDMRequestProtocol?
    
    init(presenter: DosenMatkulInteractorOutputDelegate?) {
        self.presenter = presenter
        reportDM = DosenReportDataManager(self)
    }
    
}//DosenMatkul

extension DosenMatkulInteractor: DosenMatkulInteractorInputDelegate {
    
    func getDosenReport(_ dosenId: String) {
        reportDM?.getDosenReport(dosenId)
    } 
    
}//DELEGATE


extension DosenMatkulInteractor: DosenReportResponseProtocol {
    
    func successGetDosenReport(_ response: ApiResponse<DosenReportModel>) {
        presenter?.successGetDosenReport(response)
    }
    
    func failedGetDosenReport(_ error: ApiError) {
        presenter?.failedGetDosenReport(error)
    }
    
}//DATA_MANAGER
