//
//  DosenMatkulProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol DosenMatkulViewControllerDelegate: AnyObject {
    
    func showLoadView(_ show: Bool)
    func setData(_ data: [DosenReportModel])
    
}//VIEW_CONTROLLER

protocol DosenMatkulPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func pushToDosenPertemuan(_ title: String, _ detailData: [DosenPertemuanModel])
      
}//PRESENTER

protocol DosenMatkulRouterDelegate: AnyObject {
    
    func pushToDosenPertemuan(_ title: String, _ detailData: [DosenPertemuanModel])
    
}//ROUTER

protocol DosenMatkulInteractorInputDelegate: AnyObject {
    
    func getDosenReport(_ dosenId: String) 
    
}//INTERACTOR_INPUT

protocol DosenMatkulInteractorOutputDelegate: AnyObject {
    
    func successGetDosenReport(_ response: ApiResponse<DosenReportModel>)
    func failedGetDosenReport(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
