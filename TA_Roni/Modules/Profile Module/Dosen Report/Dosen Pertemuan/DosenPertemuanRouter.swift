//
//  DosenPertemuanRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class DosenPertemuanRouter: DosenPertemuanRouterDelegate {
    weak var source: UIViewController?
    
    init(view: DosenPertemuanViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToAllAbsence(_ title: String, _ about: String, _ detailData: [AllAbsenceModel]) {
        let detailVc = AllAbsenceViewController()
        detailVc.setData(title, about, detailData)
        source?.navigationController?.pushViewController(detailVc, animated: true)
    }
}
