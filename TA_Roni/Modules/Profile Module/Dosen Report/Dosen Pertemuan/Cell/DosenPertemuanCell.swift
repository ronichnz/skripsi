//
//  DosenPertemuanCell.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class DosenPertemuanCell: UITableViewCell {
     
    let pertemuanLbl = UILabel()
    let aboutLbl = UILabel()
    let dateLbl = UILabel()
    let lineView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .background
        
        sv([
            pertemuanLbl,
            aboutLbl,
            dateLbl,
            lineView
        ])
        
        setupConstraint()
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupCell() {
        pertemuanLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.textColor = .darkGray
            $0.textAlignment = .left
            $0.numberOfLines = 0
        }
        
        aboutLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f14)
            $0.textColor = .gray
            $0.textAlignment = .left
            $0.numberOfLines = 0
        }
        
        dateLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f12)
            $0.textColor = .lightGray 
            $0.textAlignment = .left
            $0.numberOfLines = 0
        }
        
        lineView.style {
            $0.setAsLine(height: 0.3)
            $0.backgroundColor = .silverGray
        }
        
    }
    
    private func setupConstraint() {
        pertemuanLbl.snp.makeConstraints { make in
            make.top.leading.equalTo(contentView).offset(Margin.s16)
            make.trailing.equalTo(contentView).offset(-Margin.s16)
        }
        
        aboutLbl.snp.makeConstraints { make in
            make.top.equalTo(pertemuanLbl.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(pertemuanLbl)
            make.bottom.equalTo(contentView).offset(-Margin.s16)
        }
        
        dateLbl.snp.makeConstraints { (make) in
            make.top.equalTo(pertemuanLbl)
            make.trailing.equalTo(pertemuanLbl)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(pertemuanLbl)
            make.bottom.equalTo(contentView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
