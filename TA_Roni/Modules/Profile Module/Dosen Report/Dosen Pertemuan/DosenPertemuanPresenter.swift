//
//  DosenPertemuanPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class DosenPertemuanPresenter {
    weak var view: DosenPertemuanViewControllerDelegate?
    lazy var router: DosenPertemuanRouterDelegate = DosenPertemuanRouter(view: view)
    lazy var interactor: DosenPertemuanInteractorInputDelegate = DosenPertemuanInteractor(presenter: self)
    
    init(view: DosenPertemuanViewControllerDelegate?) {
        self.view = view
    }
    
}//DosenPertemuan

extension DosenPertemuanPresenter: DosenPertemuanPresenterDelegate {
    
    func viewDidLoad() {
    }
    
    func pushToAllAbsence(_ title: String, _ about: String, _ detailData: [AllAbsenceModel]) {
        router.pushToAllAbsence(title, about, detailData)
    }
    
}//DELEGATE

extension DosenPertemuanPresenter: DosenPertemuanInteractorOutputDelegate {
    
    
}//INTERACTOR
