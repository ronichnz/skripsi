//
//  DosenPertemuanViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay

class DosenPertemuanViewController: UIViewController {
    lazy var root = DosenPertemuanView()
    lazy var presenter: DosenPertemuanPresenterDelegate = DosenPertemuanPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var data = BehaviorRelay<[DosenPertemuanModel]>(value: [])
    
    var loading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
    
    private func setupViewController() {
    
    }
    
    private func binding() {
        bindStudyTbl()
    }
    
    private func bindStudyTbl() {
        data.asObservable().bind(to: root.absenTbl.rx.items(cellIdentifier: "cell", cellType: DosenPertemuanCell.self)) { (index, model, cell) in
            cell.pertemuanLbl.text = "Pertemuan \(model.pertemuanAt)"
            cell.aboutLbl.text = model.about
            cell.dateLbl.text = self.getDate(model.createdAt)
        }.disposed(by: disposable)
        
        root.absenTbl.rx.modelSelected(DosenPertemuanModel.self).subscribe(onNext: { model in
            self.presenter.pushToAllAbsence("Pertemuan \(model.pertemuanAt)", model.about, model.mahasiswa)
        }).disposed(by: disposable)
    }
    
    private func getDate(_ date: String) -> String {
        var fullDate: String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        dateFormatterPrint.locale = Locale(identifier: "ID")
        
        if let date = dateFormatterGet.date(from: "\(date)") {
            fullDate = dateFormatterPrint.string(from: date)
        } else {
           debug("There was an error when formatter date")
        }
        return fullDate
    } 
}

extension DosenPertemuanViewController: DosenPertemuanViewControllerDelegate {
    
    func showEmptyView(_ show: Bool) {
        root.emptyView.showEmptyView(show)
    }
    
    func setData(_ title: String, _ data: [DosenPertemuanModel]) {
        self.root.absenTitleLbl.text = title
        self.data.accept(data) 
        showEmptyView(data.count == 0)
    }
}
