//
//  DosenPertemuanInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class DosenPertemuanInteractor {
    weak var presenter: DosenPertemuanInteractorOutputDelegate?
    
    init(presenter: DosenPertemuanInteractorOutputDelegate?) {
        self.presenter = presenter
    }
    
}//DosenPertemuan

extension DosenPertemuanInteractor: DosenPertemuanInteractorInputDelegate {
    
}//DELEGATE


extension DosenPertemuanInteractor: DosenPertemuanInteractorOutputDelegate {
    
}//DATA_MANAGER
