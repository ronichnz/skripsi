//
//  DosenPertemuanProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 14/05/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol DosenPertemuanViewControllerDelegate: AnyObject {
    
}//VIEW_CONTROLLER

protocol DosenPertemuanPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func pushToAllAbsence(_ title: String, _ about: String, _ detailData: [AllAbsenceModel])
    
}//PRESENTER

protocol DosenPertemuanRouterDelegate: AnyObject {
    
    func pushToAllAbsence(_ title: String, _ about: String, _ detailData: [AllAbsenceModel])
    
}//ROUTER

protocol DosenPertemuanInteractorInputDelegate: AnyObject {
     
    
}//INTERACTOR_INPUT

protocol DosenPertemuanInteractorOutputDelegate: AnyObject {
    
}//INTERACTOR_OUTPUT
