//
//  AbsenReportViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay

class AbsenReportViewController: UIViewController {
    lazy var root = AbsenReportView()
    lazy var presenter: AbsenReportPresenterDelegate = AbsenReportPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var matkulData = BehaviorRelay<[AbsenReportModel]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
    
    private func setupViewController() {
        
    }
    
    private func binding() {
        bindStudyTbl()
    }
    
    private func bindStudyTbl() {
        matkulData.asObservable().bind(to: root.studyTbl.rx.items(cellIdentifier: "cell", cellType: AbsenReportCell.self)) { (index, model, cell) in
            cell.categoryLbl.text = model.namaMatkul
        }.disposed(by: disposable)
        
        root.studyTbl.rx.modelSelected(AbsenReportModel.self).subscribe(onNext: { model in
            self.presenter.pushToAbsenReportDetail(model.namaMatkul, model.absen)
        }).disposed(by: disposable)
    }
}

extension AbsenReportViewController: AbsenReportViewControllerDelegate {
    
    func showLoadView(_ show: Bool) {
        root.loadView.showLoading(show)
    }
    
    func setData(_ data: [AbsenReportModel]) {
        showLoadView(false)
        self.matkulData.accept(data)
        debug(data)
    }
    
}
