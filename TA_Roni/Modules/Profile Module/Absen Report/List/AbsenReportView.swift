//
//  AbsenReportView.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit 

final class AbsenReportView: UIView {
    
    let backBtn = FakeBackBtn(title: "Laporan Kehadiran")
    
    let studyView = UIView()
    let studyTbl = UITableView()
    
    let loadView = LoadingView()
    
    init() {
        super.init(frame: mainScreen)
        backgroundColor = .background
        
        studyView.sv([studyTbl])
        sv([backBtn, studyView, loadView])
          
        setupConstraint()
        setupView()
        setupText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupText() {
        
    }
    
    private func setupView() {
        studyView.style {
            $0.backgroundColor = .white
            $0.setLayer(cornerRadius: 8)
            $0.setShadow(radius: 2, opacity: 0.4)
        }
        
        studyTbl.style {
            $0.backgroundColor = .white
            $0.bounces = false
            $0.flexibleHeight()
            $0.register(AbsenReportCell.self, forCellReuseIdentifier: "cell")
            $0.showsVerticalScrollIndicator = false 
            $0.separatorStyle = .none
            $0.tableFooterView = UIView()
        }
    }
    
    private func setupConstraint() {
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(safeInset().top + Margin.s24)
            make.leading.equalTo(self).offset(Margin.s16)
            make.trailing.equalTo(self).offset(-Margin.s16)
        }
        
        studyView.snp.makeConstraints { (make) in
            make.top.equalTo(backBtn.snp.bottom).offset(Margin.s24)
            make.leading.trailing.equalTo(backBtn)
            make.bottom.equalTo(self).offset(-Margin.s16)
        }
        
        studyTbl.snp.makeConstraints { (make) in
            make.top.leading.equalTo(studyView).offset(Margin.s8)
            make.trailing.bottom.equalTo(studyView).offset(-Margin.s8)
        }
        
        loadView.snp.makeConstraints { (make) in
            make.edges.equalTo(studyTbl)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
