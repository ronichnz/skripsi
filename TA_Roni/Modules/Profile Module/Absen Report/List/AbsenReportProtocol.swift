//
//  AbsenReportProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol AbsenReportViewControllerDelegate: AnyObject {
    
    func showLoadView(_ show: Bool)
    func setData(_ data: [AbsenReportModel])
    
}//VIEW_CONTROLLER

protocol AbsenReportPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
    func pushToAbsenReportDetail(_ title: String, _ detailData: [AbsenReportDetailModel])
    
}//PRESENTER

protocol AbsenReportRouterDelegate: AnyObject {
    
    func pushToAbsenReportDetail(_ title: String, _ detailData: [AbsenReportDetailModel])
    
}//ROUTER

protocol AbsenReportInteractorInputDelegate: AnyObject {
    
    func getAbsenReport(_ mahasiswaId: String, _ semester: String)
    
}//INTERACTOR_INPUT

protocol AbsenReportInteractorOutputDelegate: AnyObject {
    
    func successGetAbsenReport(_ response: ApiResponse<AbsenReportModel>)
    func failedGetAbsenReport(_ error: ApiError)
    
}//INTERACTOR_OUTPUT
