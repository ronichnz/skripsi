//
//  AbsenReportCell.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import UIKit

final class AbsenReportCell: UITableViewCell {
     
    let categoryLbl = UILabel()
    let nextBtn = UIButton()
    let lineView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .background
        
        sv([
            categoryLbl,
            nextBtn,
            lineView
        ])
        
        setupConstraint()
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupCell() {
        categoryLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.numberOfLines = 0
            $0.textColor = .darkGray
        }
        
        lineView.style {
            $0.setAsLine(height: 0.3)
            $0.backgroundColor = .silverGray
        }
        
        nextBtn.style {
            $0.setImage(#imageLiteral(resourceName: "ic_info"), for: .normal)
        }
    }
    
    private func setupConstraint() {
        categoryLbl.snp.makeConstraints { make in
            make.top.leading.equalTo(contentView).offset(Margin.s16)
            make.trailing.equalTo(nextBtn.snp.leading).offset(-Margin.s16)
            make.bottom.equalTo(contentView).offset(-Margin.s16)
        }
        
        nextBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(categoryLbl)
            make.trailing.equalTo(contentView).offset(-Margin.s16)
            make.size.equalTo(Margin.i25.autoSize)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.equalTo(categoryLbl)
            make.trailing.equalTo(nextBtn)
            make.bottom.equalTo(contentView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

