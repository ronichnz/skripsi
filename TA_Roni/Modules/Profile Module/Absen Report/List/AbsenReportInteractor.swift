//
//  AbsenReportInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class AbsenReportInteractor {
    weak var presenter: AbsenReportInteractorOutputDelegate?
    
    private var reportDM: AbsenReportDMRequestProtocol?
    
    init(presenter: AbsenReportInteractorOutputDelegate?) {
        self.presenter = presenter
        reportDM = AbsenReportDataManager(self)
    }
    
}//AbsenReport

extension AbsenReportInteractor: AbsenReportInteractorInputDelegate {
    
    func getAbsenReport(_ mahasiswaId: String, _ semester: String) {
        reportDM?.getAbsenReport(mahasiswaId, semester)
    }
    
}//DELEGATE


extension AbsenReportInteractor: AbsenReportResponseProtocol {
    
    func successGetAbsenReport(_ response: ApiResponse<AbsenReportModel>) {
        presenter?.successGetAbsenReport(response)
    }
    
    func failedGetAbsenReport(_ error: ApiError) {
        presenter?.failedGetAbsenReport(error)
    }
    
}//DATA_MANAGER
