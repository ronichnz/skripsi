//
//  AbsenReportRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class AbsenReportRouter: AbsenReportRouterDelegate {
    weak var source: UIViewController?
    
    init(view: AbsenReportViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToAbsenReportDetail(_ title: String, _ detailData: [AbsenReportDetailModel]) {
        let detailVc = AbsenDetailViewController()
        detailVc.setData(title, detailData)
        source?.navigationController?.pushViewController(detailVc, animated: true)
    }
}
