//
//  AbsenReportPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 24/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class AbsenReportPresenter {
    weak var view: AbsenReportViewControllerDelegate?
    lazy var router: AbsenReportRouterDelegate = AbsenReportRouter(view: view)
    lazy var interactor: AbsenReportInteractorInputDelegate = AbsenReportInteractor(presenter: self)
    
    init(view: AbsenReportViewControllerDelegate?) {
        self.view = view
    }
    
}//AbsenReport

extension AbsenReportPresenter: AbsenReportPresenterDelegate {
    
    func viewDidLoad() {
        view?.showLoadView(true)
        interactor.getAbsenReport(Account.getMahasiswaID, Account.getCurrentSemester)
    }
    
    func pushToAbsenReportDetail(_ title: String, _ detailData: [AbsenReportDetailModel]) {
        router.pushToAbsenReportDetail(title, detailData)
    }
    
}//DELEGATE

extension AbsenReportPresenter: AbsenReportInteractorOutputDelegate {
    
    func successGetAbsenReport(_ response: ApiResponse<AbsenReportModel>) {
        view?.setData(response.data)
    }
    
    func failedGetAbsenReport(_ error: ApiError) {
        view?.showLoadView(false) 
    } 
    
}//INTERACTOR
