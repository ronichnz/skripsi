//
//  AbsenDetailRouter.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

final class AbsenDetailRouter: AbsenDetailRouterDelegate {
    weak var source: UIViewController?
    
    init(view: AbsenDetailViewControllerDelegate?) {
        source = view as? UIViewController
    }
}
