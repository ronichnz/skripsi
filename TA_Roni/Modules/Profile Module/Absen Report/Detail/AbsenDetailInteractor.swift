
//
//  AbsenDetailInteractor.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class AbsenDetailInteractor {
    weak var presenter: AbsenDetailInteractorOutputDelegate? 
    
    init(presenter: AbsenDetailInteractorOutputDelegate?) {
        self.presenter = presenter
    }
    
}//AbsenDetail

extension AbsenDetailInteractor: AbsenDetailInteractorInputDelegate {
    
}//DELEGATE


extension AbsenDetailInteractor: AbsenDetailInteractorOutputDelegate { 
    
}//DATA_MANAGER
