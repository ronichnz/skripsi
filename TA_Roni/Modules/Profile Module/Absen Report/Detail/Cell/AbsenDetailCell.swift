//
//  AbsenDetailCell.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//
 
import UIKit

final class AbsenDetailCell: UITableViewCell {
     
    let absenLbl = UILabel()
    let dateLbl = UILabel()
    let lineView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .background
        
        sv([
            absenLbl,
            dateLbl,
            lineView
        ])
        
        setupConstraint()
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupCell() {
        absenLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f16)
            $0.textColor = .darkGray
        }
        
        dateLbl.style {
            $0.font = UIFont.systemFont(ofSize: Margin.f14)
            $0.textColor = .lightGray
        }
        
        lineView.style {
            $0.setAsLine(height: 0.3)
            $0.backgroundColor = .silverGray
        }
        
    }
    
    private func setupConstraint() {
        absenLbl.snp.makeConstraints { make in
            make.top.leading.equalTo(contentView).offset(Margin.s16)
            make.trailing.equalTo(contentView).offset(-Margin.s16)
        }
        
        dateLbl.snp.makeConstraints { (make) in
            make.top.equalTo(absenLbl.snp.bottom).offset(Margin.s8)
            make.leading.trailing.equalTo(absenLbl)
            make.bottom.equalTo(contentView).offset(-Margin.s16)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(absenLbl) 
            make.bottom.equalTo(contentView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}


