//
//  AbsenDetailViewController.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay

class AbsenDetailViewController: UIViewController {
    lazy var root = AbsenDetailView()
    lazy var presenter: AbsenDetailPresenterDelegate = AbsenDetailPresenter(view: self)
    
    var disposable = DisposeBag()
    
    var data = BehaviorRelay<[AbsenReportDetailModel]>(value: [])
    
    var loading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        presenter.viewDidLoad()
        setupViewController()
        binding()
    }
    
    private func setupViewController() {
    
    }
    
    private func binding() {
        bindStudyTbl()
    }
    
    private func bindStudyTbl() {
        data.asObservable().bind(to: root.absenTbl.rx.items(cellIdentifier: "cell", cellType: AbsenDetailCell.self)) { (index, model, cell) in
            cell.absenLbl.text = "Pertemuan \(model.pertemuanAt)"
            cell.dateLbl.text = self.getDate(model.scannedAt) + ", Pada jam: \(model.atTime)"
        }.disposed(by: disposable)
    }
    
    private func getDate(_ date: String) -> String {
        var fullDate: String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        dateFormatterPrint.locale = Locale(identifier: "ID")
        
        if let date = dateFormatterGet.date(from: "\(date)") {
            fullDate = dateFormatterPrint.string(from: date)
        } else {
           debug("There was an error when formatter date")
        }
        return fullDate
    } 
}

extension AbsenDetailViewController: AbsenDetailViewControllerDelegate { 
    
    func showEmptyView(_ show: Bool) { 
        root.emptyView.showEmptyView(show)
    }
    
    func setData(_ title: String, _ data: [AbsenReportDetailModel]) {
        self.root.absenTitleLbl.text = title
        self.data.accept(data) 
        showEmptyView(data.count == 0)
    }
}
