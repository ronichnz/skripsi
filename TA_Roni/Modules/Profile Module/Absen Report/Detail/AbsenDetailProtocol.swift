//
//  AbsenDetailProtocol.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import UIKit

protocol AbsenDetailViewControllerDelegate: AnyObject { 
    
}//VIEW_CONTROLLER

protocol AbsenDetailPresenterDelegate: AnyObject {
    
    func viewDidLoad()
    
}//PRESENTER

protocol AbsenDetailRouterDelegate: AnyObject {
    
}//ROUTER

protocol AbsenDetailInteractorInputDelegate: AnyObject {
     
    
}//INTERACTOR_INPUT

protocol AbsenDetailInteractorOutputDelegate: AnyObject { 
    
}//INTERACTOR_OUTPUT
