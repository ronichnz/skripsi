//
//  AbsenDetailPresenter.swift
//  TA_Roni
//
//  Created by Cumaroni on 28/04/20.
//  Copyright © 2020 Mojave. All rights reserved.
//

import Foundation

final class AbsenDetailPresenter {
    weak var view: AbsenDetailViewControllerDelegate?
    lazy var router: AbsenDetailRouterDelegate = AbsenDetailRouter(view: view)
    lazy var interactor: AbsenDetailInteractorInputDelegate = AbsenDetailInteractor(presenter: self)
    
    init(view: AbsenDetailViewControllerDelegate?) {
        self.view = view
    }
    
}//AbsenDetail

extension AbsenDetailPresenter: AbsenDetailPresenterDelegate {
    
    func viewDidLoad() { 
    }
    
}//DELEGATE

extension AbsenDetailPresenter: AbsenDetailInteractorOutputDelegate {
    
    
}//INTERACTOR
